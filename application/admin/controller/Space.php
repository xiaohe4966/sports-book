<?php

namespace app\admin\controller;

use app\api\controller\Xiaohe;
use app\common\controller\Backend;
use think\Db;
/**
 * 场地
 *
 * @icon fa fa-circle-o
 */
class Space extends Backend
{
    
    /**
     * Space模型对象
     * @var \app\admin\model\Space
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Space;
        $this->view->assign("playStatusList", $this->model->getPlayStatusList());
        $this->view->assign("onlineStatusList", $this->model->getOnlineStatusList());
        $this->view->assign("applyStatusList", $this->model->getApplyStatusList());
        $this->view->assign("payStatusList", $this->model->getPayStatusList());
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                    ->with(['user','cate'])
                    ->where($where)
                    ->order($sort, $order)
                    ->paginate($limit);

            foreach ($list as $row) {
                $row->visible(['id','name','addr','image','blis_image','play_status','online_status','money','tel','apply_status','s_time','e_time','pay_status','week','baochang_price','price','overtime_price','in_device_id','out_device_id']);
                $row->visible(['user']);
				$row->getRelation('user')->visible(['nickname']);
				$row->visible(['cate']);
				$row->getRelation('cate')->visible(['cate_name']);
            }

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");

            //同意改拒绝
            if($params['apply_status']=='3' && $row['apply_status']=='1'){
                $xiaohe = new \app\api\controller\Xiaohe();
                $xiaohe->update_space_apply_status($ids,'3');
            }elseif($row['apply_status']=='2' && $params['apply_status']=='2'){
                //还需要加入其他的
                $xiaohe = new \app\api\controller\Xiaohe();
                $xiaohe->update_space_apply_status($ids,'3');
            }
            //update_space_apply_status
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
