<?php

return [
    'Uid'        => '用户id',
    'Status'     => '状态',
    'Status 1'   => '商家提现',
    'Status 2'   => '用户退款',
    'Status 3'   => '子订单退款',
    'Status 4'   => '到场订单',
    'Status 5'   => '预约订单',
    'Status 6'   => '子订单',
    'Status 7'   => '交平台费用',
    'Ps'         => '备注',
    'Time'       => '时间',
    'Order'      => '订单号',
    'Money'      => '钱',
    'Admin_id'   => '场馆id',
    'Space.name' => '场馆名称'
];
