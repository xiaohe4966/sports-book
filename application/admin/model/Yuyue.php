<?php

namespace app\admin\model;

use think\Model;


class Yuyue extends Model
{

    

    

    // 表名
    protected $name = 'yuyue';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        's_time_text',
        'e_time_text',
        'yuyue_status_text',
        'baochang_status_text',
        'promoter_status_text',
        'pay_status_text',
        'random_status_text',
        'time_text'
    ];
    

    
    public function getYuyueStatusList()
    {
        return ['1' => __('Yuyue_status 1'), '2' => __('Yuyue_status 2')];
    }

    public function getBaochangStatusList()
    {
        return ['1' => __('Baochang_status 1'), '2' => __('Baochang_status 2'), '3' => __('Baochang_status 3')];
    }

    public function getPromoterStatusList()
    {
        return ['1' => __('Promoter_status 1'), '2' => __('Promoter_status 2')];
    }

    public function getPayStatusList()
    {
        return ['1' => __('Pay_status 1'), '2' => __('Pay_status 2'), '8' => __('Pay_status 8'), '9' => __('Pay_status 9'), '10' => __('Pay_status 10')];
    }

    public function getRandomStatusList()
    {
        return ['1' => __('Random_status 1'), '2' => __('Random_status 2')];
    }


    public function getSTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['s_time']) ? $data['s_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getETimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['e_time']) ? $data['e_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getYuyueStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['yuyue_status']) ? $data['yuyue_status'] : '');
        $list = $this->getYuyueStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getBaochangStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['baochang_status']) ? $data['baochang_status'] : '');
        $list = $this->getBaochangStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPromoterStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['promoter_status']) ? $data['promoter_status'] : '');
        $list = $this->getPromoterStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPayStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['pay_status']) ? $data['pay_status'] : '');
        $list = $this->getPayStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getRandomStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['random_status']) ? $data['random_status'] : '');
        $list = $this->getRandomStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['time']) ? $data['time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setSTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setETimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function order()
    {
        return $this->belongsTo('Order', 'order', 'order', [], 'LEFT')->setEagerlyType(0);
    }
}
