<?php

namespace app\admin\model;

use think\Model;


class Order extends Model
{

    

    

    // 表名
    protected $name = 'order';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'time_text',
        'pay_type_text',
        'paly_status_text',
        'baochang_status_text',
        'e_time_text',
        's_time_text',
        'pay_status_text',
        'money_status_text',
        's_door_time_text',
        'pay_time_text',
        'e_door_time_text',
        'overtime_status_text',
        'deposit_status_text',
        'main_status_text',
        'random_status_text'
    ];
    

    
    public function getPayTypeList()
    {
        return ['1' => __('Pay_type 1'), '2' => __('Pay_type 2')];
    }

    public function getPalyStatusList()
    {
        return ['1' => __('Paly_status 1'), '2' => __('Paly_status 2'), '3' => __('Paly_status 3'), '4' => __('Paly_status 4'), '5' => __('Paly_status 5')];
    }

    public function getBaochangStatusList()
    {
        return ['1' => __('Baochang_status 1'), '2' => __('Baochang_status 2'), '3' => __('Baochang_status 3')];
    }

    public function getPayStatusList()
    {
        return ['1' => __('Pay_status 1'), '2' => __('Pay_status 2'), '8' => __('Pay_status 8'), '9' => __('Pay_status 9'), '10' => __('Pay_status 10')];
    }

    public function getMoneyStatusList()
    {
        return ['1' => __('Money_status 1'), '2' => __('Money_status 2')];
    }

    public function getOvertimeStatusList()
    {
        return ['1' => __('Overtime_status 1'), '2' => __('Overtime_status 2')];
    }

    public function getDepositStatusList()
    {
        return ['0' => __('Deposit_status 0'), '1' => __('Deposit_status 1'), '2' => __('Deposit_status 2')];
    }

    public function getMainStatusList()
    {
        return ['1' => __('Main_status 1'), '2' => __('Main_status 2')];
    }

    public function getRandomStatusList()
    {
        return ['1' => __('Random_status 1'), '2' => __('Random_status 2')];
    }


    public function getTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['time']) ? $data['time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getPayTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['pay_type']) ? $data['pay_type'] : '');
        $list = $this->getPayTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPalyStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['paly_status']) ? $data['paly_status'] : '');
        $list = $this->getPalyStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getBaochangStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['baochang_status']) ? $data['baochang_status'] : '');
        $list = $this->getBaochangStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getETimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['e_time']) ? $data['e_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getSTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['s_time']) ? $data['s_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getPayStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['pay_status']) ? $data['pay_status'] : '');
        $list = $this->getPayStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getMoneyStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['money_status']) ? $data['money_status'] : '');
        $list = $this->getMoneyStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getSDoorTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['s_door_time']) ? $data['s_door_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getPayTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['pay_time']) ? $data['pay_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getEDoorTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['e_door_time']) ? $data['e_door_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getOvertimeStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['overtime_status']) ? $data['overtime_status'] : '');
        $list = $this->getOvertimeStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getDepositStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['deposit_status']) ? $data['deposit_status'] : '');
        $list = $this->getDepositStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getMainStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['main_status']) ? $data['main_status'] : '');
        $list = $this->getMainStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getRandomStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['random_status']) ? $data['random_status'] : '');
        $list = $this->getRandomStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setETimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setSTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setSDoorTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setPayTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setEDoorTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function user()
    {
        return $this->belongsTo('User', 'uid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
