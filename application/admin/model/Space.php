<?php

namespace app\admin\model;

use think\Model;


class Space extends Model
{

    

    

    // 表名
    protected $name = 'space';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'time_text',
        'play_status_text',
        'online_status_text',
        'apply_status_text',
        's_time_text',
        'e_time_text',
        'pay_status_text'
    ];
    

    protected static function init()
    {
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $row->getQuery()->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
        });
    }

    
    public function getPlayStatusList()
    {
        return ['1' => __('Play_status 1'), '2' => __('Play_status 2')];
    }

    public function getOnlineStatusList()
    {
        return ['1' => __('Online_status 1'), '2' => __('Online_status 2'), '3' => __('Online_status 3')];
    }

    public function getApplyStatusList()
    {
        return ['1' => __('Apply_status 1'), '2' => __('Apply_status 2'), '3' => __('Apply_status 3')];
    }

    public function getPayStatusList()
    {
        return ['1' => __('Pay_status 1'), '2' => __('Pay_status 2')];
    }


    public function getTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['time']) ? $data['time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getPlayStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['play_status']) ? $data['play_status'] : '');
        $list = $this->getPlayStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getOnlineStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['online_status']) ? $data['online_status'] : '');
        $list = $this->getOnlineStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getApplyStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['apply_status']) ? $data['apply_status'] : '');
        $list = $this->getApplyStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getSTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['s_time']) ? $data['s_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getETimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['e_time']) ? $data['e_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getPayStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['pay_status']) ? $data['pay_status'] : '');
        $list = $this->getPayStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setSTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setETimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function user()
    {
        return $this->belongsTo('User', 'uid', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function cate()
    {
        return $this->belongsTo('Cate', 'cate_ids', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
