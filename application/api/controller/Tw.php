<?php

namespace app\api\controller;

use app\common\controller\Api;
use fast\Http;  //调用http类
use think\Db;
use think\Config;
use app\api\controller\Myconfig;
use QL\QueryList;

/**
 * 台湾快3
 * @ApiInternal()
 */
class Tw extends Api
{

    //如果$noNeedLogin为空表示所有接口都需要登录才能请求
    //如果$noNeedRight为空表示所有接口都需要验证权限才能请求
    //如果接口已经设置无需登录,那也就无需鉴权了
    //
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];



    public function _initialize()
    {
        // $this->Myconfig = new Myconfig();
        // $this->_addr = $this->request->get('addr');//地区
        // if(empty($this->_addr)){
        //     $this->_addr = 'k3js';
        // }
        // echo $this->_addr;
    }

    /**
     * 推送更新台湾数据
     * @ApiInternal()
     * @param string $data 数据是期号=》开奖号
     * @return void
     */
    public function tui_update_tw($data = null)
    {
        $num = 0;
        if($data){
            $arr = json_decode(base64_decode($data),true);
            // halt($data);
            $num = $this->put_taiwan_Db_Array($arr);
        }
        if($num<1){
            $this->get_tw();
        }else{
            file_get_contents('http://3d.he4966.cn:3333/update?addr=twk3');
        }
        
        
        
        //推送给ws；
    }


    public function get_taiwan2($bug=false)
    {
        $date = date('Y/n');
        $url = 'https://www.taiwanlottery.com.tw/Lotto/BINGOBINGO/drawing.aspx';
        $res = QueryList::get($url,null);

        $params['DropDownList2'] = '1';
        $params['__EVENTVALIDATION'] = $res->find('#__EVENTVALIDATION')->val();
        
        $params['Button1'] = '顯示當日所有期數';
        $params['__VIEWSTATEGENERATOR'] = $res->find('#__VIEWSTATEGENERATOR')->val();


        
        $params['DropDownList1'] = $date;//2021/3

        $params['__VIEWSTATE'] = $res->find('#__VIEWSTATE')->val();//有

        $params['__LASTFOCUS'] = $res->find('#__LASTFOCUS')->val();//无
        $params['__EVENTARGUMENT'] = $res->find('#__EVENTARGUMENT')->val();//无
        $params['__EVENTTARGET'] = $res->find('#__EVENTTARGET')->val();//无



      
        // halt($res);



        $ql = QueryList::post($url,$params);
        $str = $ql->getHtml();

        // if($bug){
        //     var_dump($params);
        //     $arr = $this->re_taiwan($str);
        //     if($arr){
        //         halt($arr);
        //     }else{
        //         halt($ql);
        //     }
            
            
        // }
        if(strpos($str,'class=tableFull')){

            // return $ql->find('.tableFull')->val();
            $this->success('ok',$str);
        }else{
            $this->error('没有',$str);
        }
        
    }



    public function re_taiwan($str)
    {
        // $url = 'https://www.taiwanlottery.com.tw/Lotto/BINGOBINGO/drawing.aspx';
        // $url = 'https://www.taiwanlottery.com.tw/Lotto/BINGOBINGO/drawing.aspx';
        // // $data=$this->http_get($url);
        // // $str = $this->http_post_taiwan($url,1);
        // $str = file_get_contents($url);//获取数据
// halt($str);
        // $str = file_get_contents('str.txt');//获取数据调试
        if(strlen($str)>100){
        	$s = 'class=tableFull';
      		$e = '</table>';


        	$str = $this->get_substr($str,$s,$e);//裁剪字符串

            file_put_contents('str.txt',$str);
            $str = file_get_contents('str.txt');
            halt($str);
            $num_pre ='/\<td class\=tdA\_[0-9]\>([0-9]{9,})\<\/td\>\<td class\=tdA\_[0-9]>([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2})  ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) /';
            
            preg_match_all($num_pre, $str, $newarr);//找期数

            // halt($newarr);
            $new = array();

            foreach ($newarr as $key => $val) {
               
                    // halt($key);
                    if($key===1){//期号

                        $ci = sizeof($val);//96[]

                        for ($i=0; $i < $ci; $i++) { //96
                                $new[$newarr[1][$i]] = [
                                    $newarr[2][$i],
                                    $newarr[3][$i],
                                    $newarr[4][$i],
                                    $newarr[5][$i],
                                    $newarr[6][$i],
                                    $newarr[7][$i],
                                    $newarr[8][$i],
                                    $newarr[9][$i],
                                    $newarr[10][$i],
                                    $newarr[11][$i],
                                    $newarr[12][$i],
                                    $newarr[13][$i],
                                    $newarr[14][$i],
                                    $newarr[15][$i],
                                    $newarr[16][$i],
                                    $newarr[17][$i],
                                    $newarr[18][$i],
                                    $newarr[19][$i],
                                    $newarr[20][$i],
                                    $newarr[21][$i]];
                        }
                        
                    }

                
                
            }
            return $new;


        }else{
        	 return false;
        }

        
    }


    public function get_substr($input, $start, $end) {

        var_dump($input);
        $s = strpos($input, $start);
        $e = (strlen($input) - strripos($input, $end));
        $e = $s+2800;
        // $e = strripos($input, $end);
        $substr = substr($input, $s, $e);
        var_dump($substr);
        return $substr;
      
    }


}