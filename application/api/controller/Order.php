<?php

namespace app\api\controller;

use app\api\controller\Xiaohe;

use app\api\controller\Pay;
// use EasyWeChat\Factory;
// use think\Validate;//验证
// use fast\Random;
// use fast\Http;
use think\Db;
use app\api\controller\Device;

use think\Config;



/**
 * 订单⚽︎
 */
class Order extends Xiaohe
{
    protected $noNeedLogin = ['query_order_platform','query_end_pay_order','query_baochang_pay_order','query_baochang_son_pay_order'];
    protected $noNeedRight = ['*'];


     /**
     * 创建平台订单
     * @ApiInternal()
     * @param int $space_id 场馆id
     * @param int $pay_type 支付类型 支付方式:1=微信,2=钱包
     * @return void
     */
    public function create_order_platform($space_id,$pay_type='1')
    {


        $data['uid'] = $this->auth->id;   //
        $data['space_id'] = $space_id;   //
        $res = Db::name('order_platform')->where($data)->find();
        if($res){
            return $res;
        }
        
        $data['price'] = Config::get('site.platform_price');
        $data['money'] = $data['price']; 
        $data['order'] = $this->rand_order('Plat');
        $data['pay_status'] = '1';//状态:0=待查询,1=待支付,2=已支付,3=已过期,4=已退款
        
        $data['time'] = time();
        $data['id'] = Db::name('order_platform')->insertGetId($data);

        return $data;

    }








    /**
     * 查询支付平台订单
     *
     * @param string $order
     * @return void
     */
    public function query_order_platform($order)
    {   
        $app = $this->get_pay_app();
        $data = $app->order->queryByOutTradeNumber($order);
        // halt($data);
        //查询是否支付成功
        if ($data['return_code'] == 'SUCCESS' && $data['trade_state'] == 'SUCCESS') {
            //查询如果还没修改订状态就修改状态 ，和加用户的累计金额
            
            $orderdata = Db::name('order_platform')->where('order',$order)->find();
            if($orderdata && $orderdata['pay_status']!='2'){   //第一次修改   支付   

                //增加时间
                $updata['pay_time']   = strtotime($data['time_end']);
                // $updata['endtime'] = $updata['paytime'] + $orderdata['month'] * 30 *24*60;//到期时间
                $updata['pay_status'] = '2';//状态:0=待查询,1=待支付,2=已支付,3=已过期,4=已退款
                $updata['pay_time'] = strtotime($data['time_end']);
                $updata['pay_money'] = $data['total_fee'];

                $up_res = Db::name('order_platform')->where('order',$order)->update($updata);
                if($up_res){
                    $space_data['pay_status']='2';

                    if(Config::get('site.apply_space')){
                        $space_data['apply_status'] = '1';//申请状态:1=通过,2=申请中,3=已拒绝
                    }
                    $this->update_space_apply_status($orderdata['space_id'],'1');

                    ////状态:1=商家提现,2=用户退款,3=子订单退款,4=到场订单,5=预约订单,6=子订单,7=交平台费用
                    $this->add_bill_space($orderdata['uid'],$orderdata['price'],'7',$order,$orderdata['space_id'],'平台缴费');

                    // Db::name('space')->where('id',$orderdata['space_id'])->update($space_data);

                    
                }

                //赠送会员卡 2020-12-22 14:42:04修改成一次送完优惠券
                // $this->give_coupons($order);

                
                
            }
        }

        

        $orderdata = Db::name('order')->where('order',$order)->find();
        return json($orderdata);

    }


    /**
     * 查询后支付的订单
     *
     * @param string $order
     * @return void
     */
    public function query_end_pay_order($order)
    {   
        $app = $this->get_pay_app();
        $data = $app->order->queryByOutTradeNumber($order);
        // halt($data);
        //查询是否支付成功
        if ($data['return_code'] == 'SUCCESS' && $data['trade_state'] == 'SUCCESS') {
            //查询如果还没修改订状态就修改状态 ，和加用户的累计金额
            
            $orderdata = Db::name('order')->where('order',$order)->find();
            if($orderdata && $orderdata['pay_status']!='2'){   //第一次修改   支付   

                //修改支付状态
                $updata['pay_status'] = '2';//状态:0=待查询,1=待支付,2=已支付,3=已过期,4=已退款
                $updata['pay_time'] = strtotime($data['time_end']);
                $updata['pay_money'] = $data['total_fee'];
                $up_res = Db::name('order')->where('order',$order)->update($updata);

                if($up_res){

                   
                        $yuyue_data['pay_status']='2';
                        // $yuyue_data['e_time'] = $orderdata['e_time'];
                        Db::name('yuyue')->where('order',$order)->update($yuyue_data);

                  
                    
                    
                }
                
                ////状态:1=商家提现,2=用户退款,3=子订单退款,4=到场订单,5=预约订单,6=子订单,7=交平台费用
                $this->add_bill_space($orderdata['uid'],$orderdata['price'],'4',$order,$orderdata['admin_id'],'到场订单');
                //增加该场馆的人气
                Db::name('space')->where('id',$orderdata['admin_id'])->setInc('hot');

                $orderdata  = Db::name('order')->where('order',$order)->find(); 

                //生成出门二维码
                // $Qrcode = new \app\api\controller\Qrcode();
                
                // $orderdata['out_qr'] = $Qrcode->get_out_door_qrcode($order,true);

                
                           
                
            }
        }else{
            $orderdata = Db::name('order')->where('order',$order)->find();
        }

        

        
        return json($orderdata);

    }








    /**
     * 查询包场支付的订单
     *
     * @param string $order
     * @return void
     */
    public function query_baochang_pay_order($order)
    {   
        $app = $this->get_pay_app();
        $data = $app->order->queryByOutTradeNumber($order);
        // halt($data);
        //查询是否支付成功
        if ($data['return_code'] == 'SUCCESS' && $data['trade_state'] == 'SUCCESS') {
            //查询如果还没修改订状态就修改状态 ，和加用户的累计金额
            
            $orderdata = Db::name('order')->where('order',$order)->find();
            if($orderdata && $orderdata['pay_status']!='2'){   //第一次修改   支付   

                //修改支付状态
                $updata['pay_status'] = '2';//状态:0=待查询,1=待支付,2=已支付,3=已过期,4=已退款
                $updata['pay_time'] = strtotime($data['time_end']);
                $updata['pay_money'] = $data['total_fee'];
                $updata['deposit_status'] = '1';//押金状态:0=未交押金,1=已支付押金,2=已退还押金
                $up_res = Db::name('order')->where('order',$order)->update($updata);

                if($up_res){


                        $yuyue_data['pay_status']='2';
                        // $yuyue_data['e_time'] = $orderdata['e_time'];
                        Db::name('yuyue')->where('order',$order)->update($yuyue_data);

                  
                    
                    
                }
                ////状态:1=商家提现,2=用户退款,3=子订单退款,4=到场订单,5=预约订单,6=子订单,7=交平台费用
                $this->add_bill_space($orderdata['uid'],$orderdata['price'],'5',$order,$orderdata['admin_id'],'包场订单');

                //增加该场馆的人气
                Db::name('space')->where('id',$orderdata['admin_id'])->setInc('hot');

                $orderdata  = Db::name('order')->where('order',$order)->find(); 

                //生成出门二维码
                // $Qrcode = new \app\api\controller\Qrcode();
                
                // $orderdata['out_qr'] = $Qrcode->get_baochang_out_door_qrcode($order,true);

                
                           
                
            }
        }else{
            $orderdata = Db::name('order')->where('order',$order)->find();
        }

        

        
        return json($orderdata);

    }


    /**
     * 查询包场子 支付的订单
     *
     * @param string $order
     * @return void
     */
    public function query_baochang_son_pay_order($order)
    {   
        $app = $this->get_pay_app();
        $data = $app->order->queryByOutTradeNumber($order);
        // halt($data);
        //查询是否支付成功
        if ($data['return_code'] == 'SUCCESS' && $data['trade_state'] == 'SUCCESS') {
            //查询如果还没修改订状态就修改状态 ，和加用户的累计金额
            
            $orderdata = Db::name('order')->where('order',$order)->find();
            if($orderdata && $orderdata['pay_status']!='2'){   //第一次修改   支付   

                //修改支付状态
                $updata['pay_status'] = '2';//状态:0=待查询,1=待支付,2=已支付,3=已过期,4=已退款
                $updata['pay_time'] = strtotime($data['time_end']);
                $updata['pay_money'] = $data['total_fee'];
                $up_res = Db::name('order')->where('order',$order)->update($updata);

                if($up_res){

                    
                        $yuyue_data['pay_status']='2';
                        // $yuyue_data['e_time'] = $orderdata['e_time'];
                        Db::name('yuyue')->where('order',$order)->update($yuyue_data);

                  
                    
                    
                }
                ////状态:1=商家提现,2=用户退款,3=子订单退款,4=到场订单,5=预约订单,6=子订单,7=交平台费用
                $this->add_bill_space($orderdata['uid'],$orderdata['price'],'6',$order,$orderdata['admin_id'],'包场子订单');

                //增加该场馆的人气
                Db::name('space')->where('id',$orderdata['admin_id'])->setInc('hot');

                $orderdata  = Db::name('order')->where('order',$order)->find(); 

                //生成出门二维码
                // $Qrcode = new \app\api\controller\Qrcode();
                
                // $orderdata['out_qr'] = $Qrcode->get_baochang_out_door_qrcode($order,true);

                
                           
                
            }
        }else{
            $orderdata = Db::name('order')->where('order',$order)->find();
        }

        

        
        return json($orderdata);

    }
}