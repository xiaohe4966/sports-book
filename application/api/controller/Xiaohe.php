<?php

namespace app\api\controller;

use app\common\controller\Api;
use EasyWeChat\Factory;
use think\Db;

use think\Config;
use think\Validate;//验证
use fast\Random;
use fast\Http;

use app\admin\model\AuthGroup;
use app\admin\model\AuthGroupAccess;


/**
 * 继承方法⚽︎
 */
class Xiaohe extends Api
{
    
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    protected $stores = null;

    /**微信相关  start
     *  ┏┻━━━━━━━━━┻┓
     *  ┃           ┃
     *  ┃ ┗┳     ┳┛ ┃
     *  ┃     ┻     ┃
     *  ┗━━━┓　┏━━━━┛
     *      ┃　┃
     *      ┃　┃
     *      ┃　┗━━━━━━━━━━┓
     *      ┃     He      ┣┓
     *      ┃　          ┏┛
     *      ┗━┓  ┏━━━┓  ┏┛
     *        ┗━━┛   ┗━━┛
     */



    /**
     * 获取支付app
     * @ApiSummary (内部调用)
     * @ApiWeigh (999)
     * @ApiInternal()
     * @return void
     */
    protected function get_pay_app(){
        $config = [
            'app_id' => Config('site.app_id'),
            'secret' => Config('site.secret'),
            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',

            'log' => [
                'level' => 'info', //info  'driver' => 'daily',
                // 'file' => __DIR__.'/wechat_order.log',
            ],

            'mch_id'     => Config('site.mch_id'),//商户号
            'key'        => Config('site.key'),   // API 密钥

            // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
            'cert_path'  => 'cert/apiclient_cert.pem', // XXX: 绝对路径！！！！
            'key_path'   => 'cert/apiclient_key.pem',      // XXX: 绝对路径！！！！
            // 'notify_url' => '默认的订单回调地址',     // 你也可以在下单时单独设置来想覆盖它

        
        ];

        $app = Factory::payment($config);
        return $app;
    }


    /**
     * 获取小程序
     * @ApiWeigh (998)
     * @ApiInternal()
     * @return void
     */
    protected function get_app(){
        $config = [
            'app_id' => Config('site.app_id'),
            'secret' => Config('site.secret'),
            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',

            'log' => [
                'driver' => 'daily',
               // 'level' => 'debug', //info  'driver' => 'daily', 
                // 'file' => __DIR__.'/wechat.log',
            ],
        ];

        $app = Factory::miniProgram($config);
        return $app;
    }

    /**
     * 获取微信公众号
     * @ApiWeigh (997)
     * @ApiInternal()
     * @return void
     */
    protected function get_wx_gzh_app()
    {
         $config = [
             'app_id' => Config('site.wx_app_id'),//,
             'secret' => Config('site.wx_secret'),//,
         
             // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
             'response_type' => 'array',
         
             //...
         ];
         
         $app = Factory::officialAccount($config);

         return $app;
    }



    /**
     * 发送公众号模板消息测试
     * @ApiInternal()
     * @param string $com_openid
     * @param [type] $keyword1
     * @param [type] $keyword2
     * @param string $path
     * @return void
     */
    public function send_template_message_test($com_openid='ooB8g6Hu3fa5P0oeMSpURFPVZRS4',$keyword1,$keyword2,$path='/pages/u_xjdS/u_xjdS')
    {
        
        $app = $this->get_wx_gzh_app();    
        $data = $app->template_message->send([
            'touser' => $com_openid,//微信openid,
            'template_id' => '_es-MdHZUO-voldjdevPADHj28JQ_t9bPzxowh_M8LM',
            'url' => 'https://wssct.midukj.com/',
            'miniprogram' => [
                    'appid' => Config('site.app_id'),
                    'pagepath' => $path,
            ],
            'data' => [
                'first' =>['您好，您有新的订单信息，请及时查看','#173177'],
                'keyword1' =>[$keyword1,'#173177'],//订单编号
                'keyword2' =>[$keyword2,'#173177'],//订单时间
                // 'keyword3' =>[$service,'#173177'],
                'remark' =>['请尽快登入小程序查看','#173177'],

            ],
        ]);
        return $data;
    }




    /**微信相关  end
     *  ┏┻━━━━━━━━━┻┓
     *  ┃           ┃
     *  ┃ ┗┳     ┳┛ ┃
     *  ┃     ┻     ┃
     *  ┗━━━┓　┏━━━━┛
     *      ┃　┃
     *      ┃　┃
     *      ┃　┗━━━━━━━━━━┓
     *      ┃     He      ┣┓
     *      ┃　          ┏┛
     *      ┗━┓  ┏━━━┓  ┏┛
     *        ┗━━┛   ┗━━┛
     */






    /**公共方法  start
     *  ┏┻━━━━━━━━━┻┓
     *  ┃           ┃
     *  ┃ ┗┳     ┳┛ ┃
     *  ┃     ┻     ┃
     *  ┗━━━┓　┏━━━━┛
     *      ┃　┃
     *      ┃　┃
     *      ┃　┗━━━━━━━━━━┓
     *      ┃     He      ┣┓
     *      ┃　          ┏┛
     *      ┗━┓  ┏━━━┓  ┏┛
     *        ┗━━┛   ┗━━┛
     */


    //生成订单号
    protected function rand_order($qian=null,$uid=null)
    {

        $order = date('YmdHis').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8).rand(100,999).rand(100,999);//.Random::uuid();
        if($uid){
            $order = $uid.$order;
        }else{
            if($this->auth->id){
                $order = $this->auth->id.$order;
            }
        }

        if($qian){$order = $qian.$order;}
        if(strlen($order)>32){
            $order = substr($order,-32);
        }
        return $order;
    }


    /**
     * openid获取用户信息
     * @ApiInternal()
     * @param string $openid
     * @return void
     */
    public function openid_get_user($openid)
    {
       $user =  Db::name('user')->where('openid',$openid)->find();
       return $user;
    }


    /**
     * 获取用户的openid
     * @ApiInternal()
     * 
     * @param array $staffs
     * @return void
     */
    public function get_user_openids($uids = null)
    {
        if(is_array($uids)){
            $list = Db::name('user')->where('id','in',$uids)->column('openid');
        }else{
            $list = Db::name('user')->where('id',$uids)->value('openid');
        }

        return $list;
    }





    /**
     * 
     * 更新用户信息
     * 
     * @ApiInternal()
     */
    public function update_user_data($data,$sharer=null)
    {
        $user = Db::name('user')->where('openid',$data['openid'])->find();
        
        $in_data['logintime'] = time();//登陆时间
        if($user){
            $in_data['sex'] = $data['sex'];
            $in_data['city'] = $data['city'];
            $in_data['headimgurl'] = $data['headimgurl'];
            $in_data['nickname'] = $data['nickname'];
            if($sharer && empty($user['pid'])){
                $in_data['pid'] = base64_decode($sharer);
                //送优惠券
                
            }
            
            Db::name('user')->where('openid',$data['openid'])->update($in_data);
            //修改数据；
        }else{
            
            //分享者
            if($sharer){
                $in_data['pid'] = base64_decode($sharer);
                
                

            }
            $in_data['openid'] = $data['openid'];
            $in_data['sex'] = $data['sex'];
            $in_data['city'] = $data['city'];
            $in_data['headimgurl'] = $data['headimgurl'];
            $in_data['nickname'] = $data['nickname'];
            $in_data['status'] = 'normal';
            $new_uid = Db::name('user')->insertGetId($in_data);
            //插入数据
            //送优惠券
            if($sharer){
                $this->add_coupons($in_data['pid'],'邀请新用户赠送',$new_uid);
            }
            $this->add_coupons($new_uid,'赠送新用户');
        }
    }


    /**
     * 修改申请状态
     * @ApiInternal()
     * @param int $space_id 场地id
     * @param int $status 申请状态:1=通过,2=申请中,3=已拒绝
     * @return void
     */
    public function update_space_apply_status($space_id,$status)
    {
        $space = $this->verify_space_id($space_id);
        $geo = new \addons\redisgeo\controller\Index();
        ////申请状态:1=通过,2=申请中,3=已拒绝
        switch ($status) {
            case '1':
                //加入坐标
                
                $geo->add_geo($space['lng'],$space['lat'],$space['id']);


                $admin = Db::name('admin')->find($space_id);
                if(!$admin){
                     // 加入后台管理员
                    $model = new \app\admin\model\Admin();
                    $params['id'] = $space_id;
                    $params['salt'] = Random::alnum();
                    $params['username'] = empty($space['tel'])?$params['salt']:$space['tel'];
                    $params['nickname'] = empty($space['name'])?'请修改名称':$space['name'];
                    $params['password'] = md5(md5($params['salt']) . $params['salt']);
                    $params['avatar'] = '/assets/img/avatar.png'; //设置新管理员默认头像。
                    // halt($params);
                    $result = $model->save($params);

                    $dataset = [];
                    
                    $dataset[] = ['uid' => $model->id, 'group_id' => '3'];
                    
                    $Group = new \app\admin\model\AuthGroupAccess();
                    $Group->saveAll($dataset);

                }else{
                    //已经有这个id了
                }
               

                break;
            case '2':
                # code...
                break;
            case '3':
                //删除坐标
                $geo->del_geo($space['id']);
                break;
            default:
                
                break;
        }

        Db::name('space')->where('id',$space_id)->update(['apply_status'=>$status,'admin_id'=>$space_id]);
    }    

    /**
     * 更新场地坐标
     *
     * @param int $space_id 场地id
     * @return void
     */
    protected function update_space_geo($space_id)
    {
        $space = $this->verify_space_id($space_id);
        $geo = new \addons\redisgeo\controller\Index();
        $geo->add_geo($space['lng'],$space['lat'],$space['id']);
    }

    

 


    /**
     * 获取场馆的在线人数
     * @ApiInternal()
     * @param int $space_id
     * @param int $time
     * @return void
     */
    public function get_space_online_num($space_id,$time=null)
    {

        //获取支付的人数
        // if(!$time){
        //     $time = time();
        // }
        // $where['admin_id'] = ['=',$space_id];
        // $where['baochang_status'] = ['=','1'];//包场状态:1=个人,2=包场,3=其他
        // $where['s_time'] = ['<',$time];
        // $where['pay_status'] = ['=','1'];//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
        // $where['yuyue_status'] = ['=','2'];//预约状态:1=已预约,2=到场
        // // $list_ge = Db::name('yuyue')->where($where)->select();
        // $list_ge = Db::name('yuyue')->where($where)->count();

        // $where2['admin_id'] = ['=',$space_id];
        // $where2['baochang_status'] = ['=','2'];//包场状态:1=个人,2=包场,3=其他
        // $where2['s_time'] = ['<',$time];
        // $where2['e_time'] = ['>',$time];
        // $where2['pay_status'] = ['=','2'];//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
        // $where2['yuyue_status'] = ['=','1'];//预约状态:1=已预约,2=到场

        // $list_bao = Db::name('yuyue')->where($where2)->count();

        // return ($list_bao+$list_ge);


        //获取在场内的人数

        if(!$time){
            $time = time();
        }
        $where['admin_id'] = ['=',$space_id];
        $where['baochang_status'] = ['=','1'];//包场状态:1=个人,2=包场,3=其他
        $where['e_time'] = ['=',null];
        $where['s_time'] = ['<',$time];
        $where['yuyue_status'] = ['=','2'];//预约状态:1=已预约,2=到场
       
        $list_ge = Db::name('yuyue')->where($where)->count();
        // halt($list_ge);
        $where2['admin_id'] = ['=',$space_id];
        $where2['baochang_status'] = ['=','2'];//包场状态:1=个人,2=包场,3=其他
        $where2['s_door_time'] = ['<',$time];
        $where2['s_door_time'] = ['=','NOT NULL'];
        // $where2['e_time'] = ['>',$time];
        $where2['pay_status'] = ['=','2'];//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
        // $where2['yuyue_status'] = ['=','1'];//预约状态:1=已预约,2=到场

        $list_bao = Db::name('order')->where($where2)->count();
        // halt($list_bao);
        return ($list_bao+$list_ge);

    }
    /**公共方法  end
     *  ┏┻━━━━━━━━━┻┓
     *  ┃           ┃
     *  ┃ ┗┳     ┳┛ ┃
     *  ┃     ┻     ┃
     *  ┗━━━┓　┏━━━━┛
     *      ┃　┃
     *      ┃　┃
     *      ┃　┗━━━━━━━━━━┓
     *      ┃     He      ┣┓
     *      ┃　          ┏┛
     *      ┗━┓  ┏━━━┓  ┏┛
     *        ┗━━┛   ┗━━┛
     */





/** 流水
 * ┌───┐   ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┐
 * │Esc│   │ F1│ F2│ F3│ F4│ │ F5│ F6│ F7│ F8│ │ F9│F10│F11│F12│ │P/S│S L│P/B│  ┌┐    ┌┐    ┌┐
 * └───┘   └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┘  └┘    └┘    └┘
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐ ┌───┬───┬───┬───┐
 * │~ `│! 1│@ 2│# 3│$ 4│% 5│^ 6│& 7│* 8│( 9│) 0│- _│+ =│ BacSp │ │Ins│Hom│PUp│ │N L│ / │ * │ - │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┼───┼───┤ ├───┼───┼───┼───┤
 * │ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │[ {│] }│ | \ │ │Del│End│PDn│ │ 7 │ 8 │ 9 │   │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤ └───┴───┴───┘ ├───┼───┼───┤ + │
 * │ Caps │ A │ S │ D │ F │ G │ H │ J │ K │ L │; :│" '│ Enter  │               │ 4 │ 5 │ 6 │   │
 * ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────────┤     ┌───┐     ├───┼───┼───┼───┤
 * │ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│  Shift   │     │ ↑ │     │ 1 │ 2 │ 3 │   │
 * ├─────┬──┴─┬─┴──┬┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤ ┌───┼───┼───┐ ├───┴───┼───┤ E││
 * │ Ctrl│    │Alt │ xiaohe    Space       │ Alt│    │    │Ctrl│ │ ← │ ↓ │ → │ │   0   │ . │←─┘│
 * └─────┴────┴────┴───────────────────────┴────┴────┴────┴────┘ └───┴───┴───┘ └───────┴───┴───┘
 */

    //增加用户的钱包
    protected function Inc_carat($id){
        $order= Db::name('order_in_money')->find($id);
        if($order && $order['pay_status']=='2'){
            Db::name('user')->where('id',$order['uid'])->setInc('money',$order['carat']);
        }
    }

    //用户  增加 钱包
    protected function user_Inc_money($uid,$money){
        $res = Db::name('user')->where('id',$uid)->setInc('money',$money);
        if(!$res){
            $this->error('增加用户钱包失败');
        }
        Db::name('user')->where('id',$uid)->setInc('total_money',$money);
        $this->update_user_dengji($uid);
    }

    //用户  增加 钱包
    protected function user_Dec_money($uid,$money){
        $res = Db::name('user')->where('id',$uid)->setDec('money',$money);
        if(!$res){
            $this->error('减少用户钱包失败');
        }
    }

    //场馆  增加 钱包
    protected function space_Inc_money($space_id,$money){
        $res = Db::name('space')->where('id',$space_id)->setInc('money',$money);
        if(!$res){
            $this->error('增加场馆钱包失败');
        }
    }

    //场馆  增加 钱包
    protected function space_Dec_money($space_id,$money){
        $res = Db::name('space')->where('id',$space_id)->setDec('money',$money);
        if(!$res){
            $this->error('减少场馆钱包失败');
        }
    }

    

    /**
     * 用户的流水记录
     *
     * @param string $uid 用户id
     * @param string $status 状态:1=充值克拉,2=退款,3=返佣,4=随机匹配,5=打赏,6=聊天
     * @param string $order 订单
     * @param string $ps 备注
     * @return void
     */
    protected function add_bill_user($uid,$money,$status,$order,$ps=null){

        $data['uid'] = $uid;
        $data['order'] = $order;
        $data['money'] = $money;
        $data['status'] = $status;
        $res = Db::name('bill_user')->where($data)->find();
        
        if($res && !empty($order)){
            $this->error('已有该流水'.$order);
        }
        $data['ps'] = $ps;
        $data['time'] = time();
        Db::name('bill_user')->insert($data);
        
        if($status=='1' || $status=='2' || $status=='3' ){            
            $this->user_Inc_money($uid,$money);  
        }elseif($status=='4' || $status=='5' ||$status=='6'){
            $this->user_Dec_money($uid,$money);  
        }else{
            $this->error('status参数错位');
        }
    }


    /**
     * 添加场馆流水
     *
     * @param string $uid 用户id
     * @param string $money 钱
     * @param string $status 状态:1=商家提现,2=用户退款,3=子订单退款,4=到场订单,5=预约订单,6=子订单
     * @param string $order 订单号
     * @param string $space_id 场馆id
     * @param string $ps 备注
     * @return void
     */
    protected function add_bill_space($uid,$money,$status,$order,$space_id,$ps=null){

        $data['uid'] = $uid;
        $data['order'] = $order;
        $data['money'] = $money;
        $data['status'] = $status;//状态:1=商家提现,2=用户退款,3=子订单退款,4=到场订单,5=预约订单,6=子订单
        $res = Db::name('bill_space')->where($data)->find();
        
        if($res && !empty($order)){
            $this->error('已有该流水'.$order);
        }
        $data['ps'] = $ps;
        $data['time'] = time();
        Db::name('bill_space')->insert($data);
        
        if($status=='1' || $status=='2' || $status=='3' ){            
            $this->space_Dec_money($space_id,$money);  
            if($status=='2' || $status=='3' ){
                Db::name('yuyue')->where('order',$order)->update(['pay_status'=>'10']);
            }
        }elseif($status=='4' || $status=='5' ||$status=='6'){
            $this->space_Inc_money($space_id,$money);  
            
        }else{
            $this->error('status参数错位');
        }
    }




   


    /**
     * 获取用户钱够不够
     *
     * @param int $uid
     * @param int $money
     * @return bool
     */
    protected function get_money_content($uid,$money)
    {
        $now = Db::name('user')->where('id',$uid)->value('money');
        if($now>=$money){
            return true;
        }else{
            return false;
        }
    }
/** end
 * ┌───┐   ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┐
 * │Esc│   │ F1│ F2│ F3│ F4│ │ F5│ F6│ F7│ F8│ │ F9│F10│F11│F12│ │P/S│S L│P/B│  ┌┐    ┌┐    ┌┐
 * └───┘   └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┘  └┘    └┘    └┘
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐ ┌───┬───┬───┬───┐
 * │~ `│! 1│@ 2│# 3│$ 4│% 5│^ 6│& 7│* 8│( 9│) 0│- _│+ =│ BacSp │ │Ins│Hom│PUp│ │N L│ / │ * │ - │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┼───┼───┤ ├───┼───┼───┼───┤
 * │ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │[ {│] }│ | \ │ │Del│End│PDn│ │ 7 │ 8 │ 9 │   │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤ └───┴───┴───┘ ├───┼───┼───┤ + │
 * │ Caps │ A │ S │ D │ F │ G │ H │ J │ K │ L │; :│" '│ Enter  │               │ 4 │ 5 │ 6 │   │
 * ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────────┤     ┌───┐     ├───┼───┼───┼───┤
 * │ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│  Shift   │     │ ↑ │     │ 1 │ 2 │ 3 │   │
 * ├─────┬──┴─┬─┴──┬┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤ ┌───┼───┼───┐ ├───┴───┼───┤ E││
 * │ Ctrl│    │Alt │ xiaohe    Space       │ Alt│    │    │Ctrl│ │ ← │ ↓ │ → │ │   0   │ . │←─┘│
 * └─────┴────┴────┴───────────────────────┴────┴────┴────┴────┘ └───┴───┴───┘ └───────┴───┴───┘
 */



/** 验证
 * ┌───┐   ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┐
 * │Esc│   │ F1│ F2│ F3│ F4│ │ F5│ F6│ F7│ F8│ │ F9│F10│F11│F12│ │P/S│S L│P/B│  ┌┐    ┌┐    ┌┐
 * └───┘   └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┘  └┘    └┘    └┘
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐ ┌───┬───┬───┬───┐
 * │~ `│! 1│@ 2│# 3│$ 4│% 5│^ 6│& 7│* 8│( 9│) 0│- _│+ =│ BacSp │ │Ins│Hom│PUp│ │N L│ / │ * │ - │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┼───┼───┤ ├───┼───┼───┼───┤
 * │ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │[ {│] }│ | \ │ │Del│End│PDn│ │ 7 │ 8 │ 9 │   │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤ └───┴───┴───┘ ├───┼───┼───┤ + │
 * │ Caps │ A │ S │ D │ F │ G │ H │ J │ K │ L │; :│" '│ Enter  │               │ 4 │ 5 │ 6 │   │
 * ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────────┤     ┌───┐     ├───┼───┼───┼───┤
 * │ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│  Shift   │     │ ↑ │     │ 1 │ 2 │ 3 │   │
 * ├─────┬──┴─┬─┴──┬┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤ ┌───┼───┼───┐ ├───┴───┼───┤ E││
 * │ Ctrl│    │Alt │ xiaohe    Space       │ Alt│    │    │Ctrl│ │ ← │ ↓ │ → │ │   0   │ . │←─┘│
 * └─────┴────┴────┴───────────────────────┴────┴────┴────┴────┘ └───┴───┴───┘ └───────┴───┴───┘
 */

    /**
     * 验证订单
     *
     * @param string $order
     * @return void
     */
    protected function verify_chat_order($order)
    {
        $order_data = Db::name('order')->where('order',$order)->find();
        if(!$order_data){
            $this->error('没有该订单信息');
        }
        return $order_data;
    }


    /**
     * 验证设备号
     *
     * @param int $id 场馆id
     * @return void
     */
    protected function verify_device_id($id)
    {
        $data = Db::name('device')->where('id',$id)->find();

        if(!$data){
            $this->error('没有该设备信息');
        }
        return $data;
    }


    /**
     * 验证场馆
     *
     * @param int $id 场馆id
     * @return void
     */
    protected function verify_space_id($id)
    {
        $data = Db::name('space')->where('id',$id)->find();
        if(!$data){
            $this->error('没有该场馆信息');
        }
        return $data;
    }


    
    /**
     * 验证场馆的工作时间
     *
     * @param int $space_id 场馆id
     * @param int $time 时间戳
     * @return void
     */
    protected function verify_space_work_time($space_id,$time)
    {

        $data = Db::name('space')->where('id',$space_id)->find();

        $week = date("N",$time);//1-7


        if(strpos($data['week'],$week)){
            $space_time = strtotime(date('Y-m-d ').$data['s_time']);
            // if($space_time<$time){
            //     $this->error('非营业时间请到'.date('Y-m-d H:i',$space_time).'后');
            // }
            
        }else{
            $weekarray=array("无","一","二","三","四","五","六","日");
            $this->error('该场馆星期'.$weekarray[$week].'不营业');
        }
        
    }

    
    /**
     * 验证场馆的某个时间是否有包场
     *
     * @param int $space_id 场馆id
     * @param int $time 时间戳
     * @return void
     */
    protected function verify_space_now_time_baochang($space_id,$time)
    {
        //查询这个时间是否有包场的
        $res = Db::name('yuyue')->where('admin_id',$space_id)
        ->where('pay_status','2')//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
        ->where('s_time','<=',$time)
        ->where('e_time','>',$time)
        ->where('baochang_status','2')//包场状态:1=个人,2=包场,3=其他
        ->select();
        if($res){
            $this->error('该时间已有包场',$res);
        }
    }

    /**
     * 验证场馆的某个时间是否有人
     *
     * @param int $space_id 场馆id
     * @param int $s_time 开始时间戳
     * @param int $e_time 结束时间戳
     * @return void
     */
    protected function verify_space_sometime_nums($space_id,$s_time,$e_time)
    {
       
    }

    /**
     * 用户是否绑定tel
     * @ApiInternal()
     * @param [type] $uid
     * @return void
     */
    public function user_is_bang_tel($uid)
    {
        $user = Db::name('user')->find($uid);
        if(empty($user['mobile']))
        $this->error('未绑定手机号',$user,7);

        // if(!empty($user['mobile'])){
        //     return true;
        // }else{
        //     return false;
        // }
    }


    /**
     * 获取包场的全部信息
     * @ApiInternal()
     * @param string $order 订单
     * @return void
     */
    protected function get_baochang_all_data($order)
    {
        $order_data = Db::name('order')->where('order',$order)->find();
       
        $order_data['yuyue'] = Db::name('yuyue')->where('order',$order)->find();
        $order_data['pay_nums'] = Db::name('order')->where('main_order',$order)->where('pay_status','2')->count()+1;//已报名人数
        $order_data['unpay_nums'] = Db::name('order')->where('main_order',$order)->where('pay_status','1')->count();//未支付的人数
        $order_data['son_all_nums'] = Db::name('order')->where('main_order',$order)->count();//子订单全部的人数
        $order_data['space'] = Db::name('space')->find($order_data['admin_id']);
        return $order_data;
    }



/** 验证
 * ┌───┐   ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┐
 * │Esc│   │ F1│ F2│ F3│ F4│ │ F5│ F6│ F7│ F8│ │ F9│F10│F11│F12│ │P/S│S L│P/B│  ┌┐    ┌┐    ┌┐
 * └───┘   └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┘  └┘    └┘    └┘
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐ ┌───┬───┬───┬───┐
 * │~ `│! 1│@ 2│# 3│$ 4│% 5│^ 6│& 7│* 8│( 9│) 0│- _│+ =│ BacSp │ │Ins│Hom│PUp│ │N L│ / │ * │ - │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┼───┼───┤ ├───┼───┼───┼───┤
 * │ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │[ {│] }│ | \ │ │Del│End│PDn│ │ 7 │ 8 │ 9 │   │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤ └───┴───┴───┘ ├───┼───┼───┤ + │
 * │ Caps │ A │ S │ D │ F │ G │ H │ J │ K │ L │; :│" '│ Enter  │               │ 4 │ 5 │ 6 │   │
 * ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────────┤     ┌───┐     ├───┼───┼───┼───┤
 * │ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│  Shift   │     │ ↑ │     │ 1 │ 2 │ 3 │   │
 * ├─────┬──┴─┬─┴──┬┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤ ┌───┼───┼───┐ ├───┴───┼───┤ E││
 * │ Ctrl│    │Alt │ xiaohe    Space       │ Alt│    │    │Ctrl│ │ ← │ ↓ │ → │ │   0   │ . │←─┘│
 * └─────┴────┴────┴───────────────────────┴────┴────┴────┴────┘ └───┴───┴───┘ └───────┴───┴───┘
 */



    /**
     * 计算场地散客价格
     * @ApiInternal()
     * @param int $space_id
     * @param int $s_time
     * @param int $e_time
     * @return void
     */
    public function get_space_count_price($space_id,$s_time,$e_time)
    {
        $space = $this->verify_space_id($space_id);
        $time = $e_time-$s_time;
        $h = ceil($time/3600);//浮点进1
        $price = $space['price'] * $h;
        return $price;
    }

    /**
     * 计算场包场价格
     * @ApiInternal()
     * @param int $space_id
     * @param int $s_time
     * @param int $e_time
     * @return void
     */
    public function get_space_count_baochang_price($space_id,$s_time,$e_time=null)
    {
        
        $space = $this->verify_space_id($space_id);
        $time = $e_time-$s_time;
        $h = ceil($time/3600);//浮点进1
        $price = $space['baochang_price'] * $h;
        return $price;
    }

    /**
     * 加密字符串
     *
     * @param string $str
     * @return void
     */
    protected function enStr($str){


        return $str;
    }

     /**
     * 解密字符串
     *
     * @param string $str
     * @return void
     */
    protected function deStr($str){

        
        return $str;
    }



    /**
     * 修改包场的进入信息
     *
     * @param string $order 订单号
     * @return void
     */
    protected function update_baochang_in_data($order)
    {
        $data['paly_status'] = '2';//运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
        $data['s_door_time'] = time();


        $res = Db::name('order')->where('order',$order)->update($data);
        
    }
 /**
     * 修改到场的出入信息(出去的时候才调此接口)
     *
     * @param string $order 订单号
     * @return void
     */
    protected function update_daochang_out_data($order)
    {
        $data['paly_status'] = '4';//运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
        $data['e_door_time'] = time();
        $res = Db::name('order')->where('order',$order)->update($data);
    }
    /**
     * 修改包场的出入信息(出去的时候才调此接口)
     *
     * @param string $order 订单号
     * @return void
     */
    protected function update_baochang_out_data($order)
    {
        $order_data = $this->get_baochang_all_data($order);
        
        if($order_data['main_status']=='1'){
            $main_order = $order;
        }else{
            $main_order = $order_data['main_order'];
        }


        //查支付人并没有出去的  
        $list = Db::name('order')
                ->where('paly_status','2')//运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
                ->where('pay_status','2')//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
                // ->where('main_status','2')//主订单:1=主订单,2=子订单
                ->where('admin_id',$order_data['admin_id'])
                ->select();
        //没有出去的人数
        $no_out_num = 0;
        foreach ($list as $key => $val) {
            $temp_order = Null;
            if($val['main_status']=='1'){
                $temp_order = $main_order;
            }else{
                $temp_order = $val['main_order'];
            }
            if($main_order==$temp_order){
                $no_out_num++;
            }
        }

        //随机队员:1=否,2=随机  
        if($order_data['random_status']=='1'){

             
            //最后一个
            if($no_out_num==1){

                // 查询超时没有 
                if($order_data['e_time']<time()){
                    //超时扣除违约金
                    $up_data['overtime_status'] = '2';//超时状态:1=未超时,2=超时
                    //超时的钱  = 超时价格 * 超时小时 取整
                    $up_data['overtime_price'] = $order_data['space']['overtime_price']*ceil((time()-$order_data['yuyue']['e_time'])/3600);
                    $up_data['refund_deposit'] = $order_data['deposit']-$overtime_price;
                    
                }else{
                    $up_data['overtime_status'] = '1';//超时状态:1=未超时,2=超时
                    $up_data['overtime_price'] = 0;
                    $up_data['refund_deposit'] = $order_data['deposit'];
                }
                //退还押金
                Db::name('order')->where('order',$main_order)->update($up_data);
                $Pay = new \app\api\controller\Pay();
                $Pay->refund_deposit($main_order,true);
                
            }

                
            
            

        }else{
           
            //随机进来的

            // 如果超时就按散客价格按出时间算

            //判断是最后一个出去不
        }
        $data['paly_status'] = '4';//运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
        $data['e_door_time'] = time();
        $res = Db::name('order')->where('order',$order)->update($data);

    }


    /**
     * 获取用户的玩的分钟数
     *
     * @param int $uid 用户uid
     * @param int $space_id 场馆id
     * @return void
     */
    protected function get_user_paly_time($uid,$space_id=null)
    {
        if($space_id){
            $where['admin_id'] = ['=' , $space_id];
        }
        $where['uid'] = ['=' , $uid];


        $list = Db::name('order')->where($where)->where('paly_status','4')->where('e_door_time','>',0)->select();
        $sec = 0;
        foreach ($list as $key => $val) {
            $sec = $sec + $val['e_door_time']-$val['s_door_time'];
        }
        return $sec;
    }


    /**
     * 获取包场子订单需要支付的价格
     *
     * @param string $order 主订单号
     * @return void
     */
    protected function get_baochang_son_price($order)
    {
   
        $order_data = $this->get_baochang_all_data($order);

        $price = round(($order_data['price'] - $order_data['deposit'])/$order_data['yuyue']['nums'],2);//实际-押金/人数 //对浮点数进行四舍五入
        return $price;
    }

    //获取包场是否满了
    protected function get_baochang_is_man($order)
    {
        $order_data = $this->get_baochang_all_data($order);

        if($order_data['pay_nums']>=($order_data['yuyue']['nums']-1)){
            return true;
        }else{
            return false;
        }
    
    }

    /**
     * 添加消息
     *
     * @param int $uid 用户uid
     * @param string $msg 消息
     * @return void
     */
    protected function add_msg($uid,$msg){
        $data['time'] = time();
        $data['uid'] = $uid;
        $data['msg'] = $msg;
        $data['status'] = '1';
        Db::name('msg')->insert($data);
    }

}