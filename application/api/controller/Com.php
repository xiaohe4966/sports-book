<?php

namespace app\api\controller;

use app\common\controller\Api;
use EasyWeChat\Factory;
use think\Db;

use think\Config;
use think\Validate;//验证
use fast\Random;
use fast\Http;

use app\api\controller\Xiaohe;


/**
 * 公共⚽︎
 */
class Com extends Xiaohe
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    protected $stores = null;




    // public $uid = '';
    public function _initialize()
    {
        parent::_initialize();
     
        // $uid = $this->request->param('uid');//用户uid 需要解密
        // if(!empty($uid))$this->auth->id_de($uid);
        // $this->request->filter('trim,strip_tags,htmlspecialchars');
    }

    /**
     * 获取微信等配置
     * @ApiWeigh (999)
     * @ApiSummary  (获取微信支付等配置信息)
     * @param string $field 需要获取的某个字段名称
     * @return void
     */
    public function get_config($field=null)
    {
        if($field){
            $field = '.'.$field;
        }
        $data = Config::get('site'.$field);
        return json($data);
    }

    /**
     * 获取场馆分类列表
     * 
     * @return void
     */
    public function get_cate_list()
    {
        $list = Db::name('cate')->order('weigh desc')->select();
        // return $list;
        $this->success('ok',$list);
    }

    /**
     * 获取banner列表
     *
     * @param integer $limit 10
     * @return void
     */
    public function get_banner_list($limit=10)
    {
        $list = Db::name('banner')->order('weigh desc')->limit($limit)->select();
        $this->success('ok',$list);

    }

    /**
     * 获取场馆信息
     *
     * @param int $space_id
     * @return void
     */
    public function get_space_data($space_id)
    {
        $data['space'] = $this->verify_space_id($space_id);
        $data['online'] = $this->get_space_online_num($space_id);

        $this->success('ok',$data);
    }


    /**
     * 获取首页信息
     * @ApiSummary  (获取banner,会员总数，场馆总数)
     * @return void
     */
    public function get_index_data()
    {
        $data['user_nums'] = Db::name('user')->count();
        $data['space_nums'] = Db::name('space')->where('apply_status','1')->count();
        $data['banner'] = Db::name('banner')->order('weigh desc')->select();
        $this->success('ok',$data);
    }

    /**
     * 获取附近场馆列表
     *
     * @param int $cate_id 分类id（可不传
     * @param int $page 1
     * @param int $limit 5
     * @param float $lng 经度
     * @param float $lat 纬度
     * @param integer $m 附近多少米，默认1000m
     * @return void
     */
    public function get_space_list($cate_id=null,$page = 1,$limit = 5,$lng=null,$lat=null,$m=null,$name=null)
    {
        
        $where = null;
        if($cate_id){
            $where['cate_ids'] = ['=',$cate_id];
        }
        if(!$m){
            $m = 10000;
        }
        if(!$limit){
            $limit = 5;
        }
        if(!$page){
            $page = 1;
        }

        $space_ids = Db::name('space')->where($where)->column('id');
     
        
        $list = array();
        if($lng && $lat){
            $geo = new \addons\redisgeo\controller\Index();
            $geo_list = $geo->get_nearby_list($lng,$lat,$m,'m',$limit,'ASC');
            // 
            if($geo_list){
                foreach ($geo_list as $key => $geo) {
                    if(in_array($geo[0],$space_ids)){
                       
                        $temp = Db::name('space')->where('id',$geo[0])->find();
                        $temp['m'] = $geo[1];
                     
                        $temp['online'] = $this->get_space_online_num($geo[0]);//在线数
                        // halt($geo_list);
                        $list[] = $temp;
                        if(sizeof($list)>=$limit)break;
                    }
                }
            }
            
        }  

        $this->success('ok',$list);
    }




    /**
     * 搜索场馆列表
     *
     * @param int $page 1
     * @param int $limit 5
     * @param float $lng 经度（可不传
     * @param float $lat 纬度（可不传
     * @param string $name 搜索名称(可不传
     * @param string $hot 人气（传1
     * @param string $distance 距离（传1
     * @param string $province 省
     * @param string $city 市
     * @param string $area 区
     * @return void
     */
    public function search_space_list($page = 1,$limit = 5,$lng=null,$lat=null,$name=null,$province=null,$city=null,$area=null,$hot = null,$distance=null)
    {
       
        $geo = new \addons\redisgeo\controller\Index();
        $where = null;
        if($name){
            $where['name'] = ['like','%'.$name.'%'];
        }
        
        if($province){//省市

            $where['province'] = ['=',$province];
            if($city){

                $where['city'] = ['=',$city];
                if($area){

                    $where['area'] = ['=',$area];
                }
            }
        }
       
        
        
        if($lat && $lng){

            if($distance){
                
                $geo_list = $geo->get_nearby_list($lng,$lat,99999999,'m',10000,'ASC');
                $list = array();
                $num = 0;
                //有个 bug 这是按先查询后的场馆再获取的附近的  其实最近的不再最前面 
                foreach ($geo_list as $key => $geo) {
                    
                        $num ++;


                        $temp = Db::name('space')->where('id',$geo[0])->find();
                        $temp['m'] = $geo[1];
                        $temp['online'] = $this->get_space_online_num($geo[0]);//在线数
                        if($num<=($page-1)*$limit && $num<=$page*$limit){
                            $list[] = $temp;
                            if(sizeof($list)>=$limit)break;
                        }
                        
                    
                }  
            }
            elseif($hot){
                $space_ids = $this->get_hot_space_list_ids($page,$limit);
                  //用经纬度获取
                
                $geo_list = $geo->get_nearby_list($lng,$lat,99999999,'m',10000,'ASC');
                $list = array();
                foreach ($geo_list as $key => $geo) {
                    if(in_array($geo[0],$space_ids)){
                        $temp = Db::name('space')->where('id',$geo[0])->find();
                        $temp['m'] = $geo[1];
                        $temp['online'] = $this->get_space_online_num($geo[0]);//在线数
                        $list[] = $temp;
                        if(sizeof($list)>=$limit)break;
                    }
                }   
            }else{

                $space_ids = Db::name('space')->where($where)->column('id');
                $geo_list = $geo->get_nearby_list($lng,$lat,99999999,'m',10000,'ASC');
                $list = array();
                //有个 bug 这是按先查询后的场馆再获取的附近的  其实最近的不再最前面 
                foreach ($geo_list as $key => $geo) {
                    if(in_array($geo[0],$space_ids)){
                        $temp = Db::name('space')->where('id',$geo[0])->find();
                        $temp['m'] = $geo[1];
                        $temp['online'] = $this->get_space_online_num($geo[0]);//在线数
                        $list[] = $temp;
                        if(sizeof($list)>=$limit)break;
                    }
                }   
            }

            
            
          
            
            // 获取某个的距离  店家
            //获取翻页里面的


        }else{
            
           $list = Db::name('space')->where($where)->page($page,$limit)->select();
           if($list){
            foreach ($list as $key => $val) {
                $list[$key]['online'] = $this->get_space_online_num($val['id']);//在线数
            }
        }
          
        }

       

        $this->success('ok',$list);
    }




    protected function get_hot_space_list_ids($page=1,$limit=10,$sort='DESC')
    {
        //先获取订单较多的场馆
        $list = Db::name('space')->order('hot '.$sort)->page($page,$limit)->column('id');
        return $list;
    }


    /**
     * 获取全部玩家排行
     * @ApiSummary  (获取全部时间排序 )
     * @param int $page 1
     * @param int $limit 10
     * @return void
     */
    public function get_paly_all_ranking($page=1,$limit=10)
    {
        //获取已完成的 订单
        $list = Db::name('order')
                ->where('paly_status','4')
                ->where('e_door_time','>',0)
                ->where('e_door_time','NOT NULL')
                ->where('s_door_time','NOT NULL')
                ->select();
        // halt($list);
        $new_list = array();
        foreach ($list as $key => $val) {
            if(isset($new_list[$val['uid']])){
                $new_list[$val['uid']]['time'] = $new_list[$val['uid']]['time'] + $val['e_door_time']-$val['s_door_time'];

            }else{
                $new_list[$val['uid']]['time'] = $val['e_door_time']-$val['s_door_time'];
                $new_list[$val['uid']]['user'] = $val['uid'];
            }
        }
        

        //$a是排序数组，$b是要排序的数据集合，$result是最终结果
        $b = $new_list;
        $a = array();
        foreach($b as $key=>$val){
            $a[] = $val['time'];//这里要注意$val['nums']不能为空，不然后面会出问题
        }
        //$a先排序
        rsort($a);
        $a = array_flip($a);
        $list = array();
        foreach($b as $k=>$v){
            $temp1 = $v['time'];
            $temp2 = $a[$temp1];
            $list[$temp2] = $v;
        }
        //这里还要把$list进行排序，健的位置不对
        ksort($list);
        $new_list = array();
        $num = 0;
        foreach ($list as $key => $val) {
            $num++;
            $min = (intval($page)-1) * intval($limit);
            $max = (intval($page) * intval($limit));
            
            if($num<=$max && $num>$min){
                $temp = Db::name('user')->where('id',$val['user'])->find();
                $temp['time'] = ceil($val['time']/60);
                $new_list[] = $temp;
            }
        }
        
        $this->success('ok',$new_list);
    }


     /**
     * 获取场馆的玩家排行
     * @ApiSummary  (获取全部时间排序 )
     * @param int $space_id 场馆id
     * @param int $page 1
     * @param int $limit 10
     * @return void
     */
    public function get_space_paly_ranking($space_id,$page=1,$limit=10)
    {
        //获取已完成的 订单
        $list = Db::name('order')->where('admin_id',$space_id)->where('paly_status','4')->where('e_door_time','>',0)->select();
        $new_list = array();
        foreach ($list as $key => $val) {
            if(isset($new_list[$val['uid']])){
                $new_list[$val['uid']]['time'] = $new_list[$val['uid']]['time'] + $val['e_door_time']-$val['s_door_time'];

            }else{
                $new_list[$val['uid']]['time'] = $val['e_door_time']-$val['s_door_time'];
                $new_list[$val['uid']]['user'] = $val['uid'];
            }
        }


        //$a是排序数组，$b是要排序的数据集合，$result是最终结果
        $b = $new_list;
        $a = array();
        foreach($b as $key=>$val){
            $a[] = $val['time'];//这里要注意$val['nums']不能为空，不然后面会出问题
        }
        //$a先排序
        rsort($a);
        $a = array_flip($a);
        $list = array();
        foreach($b as $k=>$v){
            $temp1 = $v['time'];
            $temp2 = $a[$temp1];
            $list[$temp2] = $v;
        }
        //这里还要把$list进行排序，健的位置不对
        ksort($list);
        $new_list = array();
        $num = 0;
        foreach ($list as $key => $val) {
            $num++;
            $min = (intval($page)-1) * intval($limit);
            $max = (intval($page) * intval($limit));
            
            if($num<=$max && $num>$min){
                $temp = Db::name('user')->where('id',$val['user'])->find();
                $temp['time'] = ceil($val['time']/60);
                $new_list[] = $temp;
            }
        }
        
        $this->success('ok',$new_list);
    }







    

}