<?php

namespace app\api\controller;

use app\common\controller\Api;
use EasyWeChat\Factory;
use think\Db;

use think\Config;
use think\Validate;//验证
use fast\Random;
use fast\Http;

use app\api\controller\Xiaohe;
use app\api\controller\Pay;


/**
 * 预约⚽︎
 */
class Yuyue extends Xiaohe
{

    protected $noNeedLogin = ['*'];//['index','create_end_pay_order'];
    protected $noNeedRight = ['*'];


    public function _initialize()
    {
        parent::_initialize();
    
    }

    /**
     * 创建后付费订单
     *
     * @param int $space_id 场馆id
     * @param int $uid 用户id
     * @param int $min 预计玩多少分钟
     * @return void
     */
    public function create_end_pay_order($space_id,$uid,$min=60)
    {
        //验证场馆
        $space = $this->verify_space_id($space_id);
        // if($space['play_status']=='2'){
        //     //陪玩状态:1=正常营业,2=停止营业
        //     $this->error('此场馆已停止营业');
        // }elseif($space['apply_status']!='1'){
        //     //申请状态:1=通过,2=申请中,3=已拒绝
        //     $this->error('此场馆未通过营业');
        // }
        
        //验证是否在营业时间
        $this->verify_space_work_time($space_id,time());

        //验证当前时间是否有包场
        $this->verify_space_now_time_baochang($space_id,time());

        //验证多少分钟后是否有包场
        $this->verify_space_now_time_baochang($space_id,time()+$min*60);

        
        $data['admin_id'] = $space_id;
        $data['uid'] = $uid;
        $data['baochang_status'] = '1';//包场状态:1=个人,2=包场,3=其他
        $data['yuyue_status'] = '2';//预约状态:1=已预约,2=到场
        $data['nums'] = 1;
        $data['promoter_status'] = '1';//是否是发起人:1=是,2=否
        $data['pay_status'] = '1';//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款

        //判断10分钟内是否有该场的 订单
        $res = Db::name('yuyue')->where($data)->where('s_time','>',time()-600)->find();
        if($res){
            // $res['door'] = 'in';
            return $res;
        }
        $data['order']  = $this->rand_order('Dc');

        $data['s_time'] = time();
        $data['time'] = time();
       
        $data['id'] = Db::name('yuyue')->insertGetId($data);
        $data['door'] = 'in';//进入
        return $data;





    }
    /**
     * 获取后付费订单
     *
     * @param int $space_id 场馆id
     * @param int $uid 用户id
     * @param int $min 预计玩多少分钟
     * @return void
     */
    public function get_yuyue_order($space_id,$uid)
    {

        $data['admin_id'] = $space_id;
        $data['uid'] = $uid;
        $data['baochang_status'] = '1';//包场状态:1=个人,2=包场,3=其他
        $data['yuyue_status'] = '2';//预约状态:1=已预约,2=到场
        $data['nums'] = 1;
        $data['promoter_status'] = '1';//是否是发起人:1=是,2=否
        $data['pay_status'] = '1';//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款

        //判断10分钟内是否有该场的 订单
        $res = Db::name('yuyue')->where($data)->find();
        if($res){
            // $res['door'] = 'in';
            return $res;
        }
        // $data['order']  = $this->rand_order('Dc');

        // $data['s_time'] = time();
        // $data['time'] = time();
       
        // $data['id'] = Db::name('yuyue')->insertGetId($data);
        // $data['door'] = 'in';//进入
        // return $data;

    }

    // /**
    //  * 获取后付费的待支付订单
    //  *
    //  * @param string $space_id 场地id
    //  * @param string $uid 用户id
    //  * @return void
    //  */
    // public function get_end_pay_order($space_id,$uid){

    //     $data['admin_id'] = $space_id;
    //     $data['uid'] = $uid;
    //     $data['baochang_status'] = '1';//包场状态:1=个人,2=包场,3=其他
    //     $data['yuyue_status'] = '2';//预约状态:1=已预约,2=到场
    //     $data['nums'] = 1;
    //     $data['promoter_status'] = '1';//是否是发起人:1=是,2=否
    //     $data['pay_status'] = '1';//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款

    //     $res = Db::name('yuyue')->where($data)->find();

    //     return $res;

    // }

    
    /**
     * 获取后付费的待支付订单返回支付的数据
     *
     * @param string $space_id 场地id
     * @param string $uid 用户id
     * @return void
     */
    public function get_end_pay_order_and_pay($space_id,$uid,$nums=null){

        // $yuyue_order = $this->create_end_pay_order($space_id,$uid,$nums);
        $yuyue_order = $this->get_yuyue_order($space_id,$uid,$nums);
        if($yuyue_order){
            //生成支付
            $data['order'] = $yuyue_order['order'];
            //找支付订单
            $res = Db::name('order')->where($data)->find();
            if($res){
                return $res;
            }else{
                $data['uid'] = $uid;
                $data['admin_id'] = $space_id;
                $data['time'] = time();
                $data['s_door_time'] = $yuyue_order['s_time'];
                $data['s_time'] = $yuyue_order['s_time'];//创建时间 还是开门时间？
                $data['e_time'] = time();

                $data['price'] = $this->get_space_count_price($space_id,$data['s_time'],$data['e_time']);
                $data['pay_money'] = $data['price'];
                $data['paly_status'] = '2';//运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
                $data['baochang_status'] = '1';//包场状态:1=个人,2=包场,3=其他
                $data['pay_status'] = '1';//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款


                $data['id'] = Db::name('order')->insertGetId($data);
                return $data;
            }
        }else{
            return false;
        }
    }

    /**
     * 创建包场订单
     * @ApiInternal()
     * @param string $space_id 场馆id
     * @param string $s_time 开始时间
     * @param string $e_time 结束时间
     * @param string $nums 人数（可不填）
     * @return void
     */
    public function create_baochang_yuyue_order($space_id,$s_time,$e_time,$nums=1,$random_status='1')
    {
        //验证场馆
        $space = $this->verify_space_id($space_id);
        if($space['play_status']=='2'){
            //陪玩状态:1=正常营业,2=停止营业
            $this->error('此场馆已停止营业');
        }elseif($space['apply_status']!='1'){
            //申请状态:1=通过,2=申请中,3=已拒绝
            $this->error('此场馆未通过营业');
        }
        if($s_time<time()){
            $this->error('预约时间需大于当前时间');
        }
        
        // $s_time = strtotime($s_time);
        // $e_time = strtotime($e_time);

        //验证是否在营业时间
        $this->verify_space_work_time($space_id,$s_time);

        //验证场馆的某个时间是否有人
        $this->verify_space_sometime_nums($space_id,$s_time,$e_time);

        // 验证多少分钟后是否有包场
        $this->verify_space_now_time_baochang($space_id,$s_time,$e_time);

        
        $data['admin_id'] = $space_id;
        $data['uid'] = $this->auth->id;
        $data['baochang_status'] = '2';//包场状态:1=个人,2=包场,3=其他
        $data['yuyue_status'] = '1';//预约状态:1=已预约,2=到场
        $data['nums'] = $nums;
        $data['promoter_status'] = '1';//是否是发起人:1=是,2=否
        $data['pay_status'] = '1';//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
        $data['random_status'] = $random_status;//随机队员:1=否,2=随机
        $data['s_time'] = $s_time;
        $data['e_time'] = $e_time;

        //判断是否有该场的预约 订单
        $res = Db::name('yuyue')->where($data)->find();
        if($res){
            return $res;
        }
        $data['order']  = $this->rand_order('Bc');
        $data['time'] = time();
        
        // $data['door'] = 'in';//进入
        $data['id'] = Db::name('yuyue')->insertGetId($data);
        return $data;

    }



    /**
     * 获取某个场地已支付的包场订单
     * @ApiInternal()
     * @param string $space_id 场馆id
     * @return void
     */
    public function get_baochang_pay_order($space_id)
    {
        $data['uid'] = $this->auth->id;
        $data['admin_id'] = $space_id;
    }

    /**
     * 获取我的包场订单
     *
     * @param integer $page 1
     * @param integer $limit 5
     * @param integer $pay_status null(支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
     * @return void
     */
    public function get_my_baochang_order_list($page=1,$limit=5,$pay_status=null)
    {
        $where = null;
        $where['uid'] = $this->auth->id;
        if($pay_status){
            $where['pay_status'] = $pay_status;
        }
        $list = Db::name('order')->where($where)->page($page,$limit)->order('time desc')->select();
        foreach ($list as $key => $val) {
            $list[$key]['space'] = Db::name('space')->where('id',$val['admin_id'])->find();
            $list[$key]['yuyue'] = Db::name('yuyue')->where('order',$val['order'])->find();
        }
        $this->success('ok',$list);
    }


    /**
     * 支付包场订单
     *
     * @param string $order 订单
     * @return void
     */
    public function pay_baochang_order($order)
    {
        $Pay = new Pay();
        
        $where['order'] = ['=',$order];
        $where['uid'] = ['=',$this->auth->id];
        $baochang_order = Db::name('order')->where($where)->find();
        if($baochang_order['s_time']<time()){
            $this->error('包场时间已过');
        }elseif($baochang_order['pay_status']=='8'){
            $this->error('支付超时');
        }
        
        if($baochang_order){
            $pay_data = $Pay->pay_baochang_pay_order($baochang_order);
            $this->success('支付订单',$pay_data,8);
        }
    }





    /**
     * 获取包场的待支付订单返回支付的数据
     *
     * @param string $space_id 场馆id
     * @param string $s_time 开始时间
     * @param string $e_time 结束时间
     * @param string $nums 人数（可不填）
     * @return void
     */
    protected function get_baochang_order_and_pay($space_id,$s_time,$e_time,$nums=10,$random_status='1')
    {
        $s_time = strtotime($s_time);
        $e_time = strtotime($e_time);

        $yuyue_order = $this->create_baochang_yuyue_order($space_id,$s_time,$e_time,$nums,$random_status);
        $space = $this->verify_space_id($space_id);
        if($yuyue_order){
            //生成支付
            $data['order'] = $yuyue_order['order'];
            //找支付订单
            $res = Db::name('order')->where($data)->find();
            if($res){
                // $this->error('订单请稍后再试');
                return $res;
            }else{
                $data['uid'] = $this->auth->id;
                $data['admin_id'] = $space_id;
                $data['time'] = time();
                $data['s_time'] = $s_time;//$yuyue_order['s_time'];//创建时间 还是开门时间？
                $data['e_time'] = $e_time;//time();

                $data['deposit'] = $space['deposit'];//包场押金
                
                $data['random_status'] = $random_status;//随机队员:1=否,2=随机
                $data['price'] = $this->get_space_count_baochang_price($space_id,$data['s_time'],$data['e_time']) + $data['deposit'];
                // $data['pay_money'] = $data['price'];
                $data['paly_status'] = '1';//运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
                $data['baochang_status'] = '2';//包场状态:1=个人,2=包场,3=其他
                $data['pay_status'] = '1';//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
                $data['overtime_status'] = '1';//超时状态:1=未超时,2=超时
                $data['deposit_status'] = '0';//押金状态:0=未交押金,1=已支付押金,2=已退还押金

                $data['id'] = Db::name('order')->insertGetId($data);
                return $data;
            }
        }else{
            return false;
        }
    }


    
    /**
     * 添加包场——返回支付的数据
     *
     * @param string $space_id 场馆id
     * @param string $date 年月日
     * @param string $s_time 开始时间
     * @param string $e_time 结束时间
     * @param string $nums 人数（可不填）
     * @param string $random_status 随机队员:默认1=否（自己二维码邀请）,2=随机进来
     * @return void
     */
    public function add_baochang($space_id,$s_time,$e_time,$random_status='1',$nums=null,$date=null)
    {
        $Pay = new Pay();
        $Yuyue = new Yuyue();

        if($date){
            $s_time = $date.' '.$s_time;
            $e_time = $date.' '.$e_time;
        }
        $baochang_order= $Yuyue->get_baochang_order_and_pay($space_id,$s_time,$e_time,$nums,$random_status);
        if($baochang_order){
            $pay_data = $Pay->pay_baochang_pay_order($baochang_order);
            $this->success('支付订单',$pay_data,8);
        }
    }

     /**
     * 获取包场子订单需要支付的价格
     *
     * @param string $order 主订单号
     * @return void
     */
    public function get_baochang_son_price_api($order)
    {
       $this->success('价格',$this->get_baochang_son_price($order)); 
    }

    /**
     * 添加包场子订单（需要支付
     * 
     * @param string $order 订单
     * @return void
     */
    public function add_baochang_son_order($order)
    {
        $order_data = $this->get_baochang_all_data($order);
        //判断这个人数是否已满
        // var_dump($order_data);
        // die;

        if($order_data['uid']==$this->auth->id){
            $this->error('自己的订单不能报名哦！');
        }
        if($order_data['yuyue']['nums']>1)

        if($order_data['pay_nums']>=($order_data['yuyue']['nums'])){
            $this->error('报名人数已满');
        }

        //场馆信息
        $space = $order_data['space'];

        $yuyue_order = $this->get_baochang_son_yuyue_order($order_data);
        if($yuyue_order){
            //生成支付
            $data['order'] = $yuyue_order['order'];
            //找支付订单
            $res = Db::name('order')->where($data)->find();
            if(!$res){
            
                $data['uid'] = $this->auth->id;
                $data['admin_id'] = $order_data['admin_id'];
                $data['time'] = time();
                $data['s_time'] = $order_data['s_time'];
                $data['e_time'] = $order_data['e_time'];

                $data['deposit'] = 0;//包场押金
                
                $data['random_status'] = '1';//随机队员:1=否,2=随机
                $data['price'] = $this->get_baochang_son_price($order);//round(($order_data['price'] - $order_data['deposit'])/$order_data['yuyue']['nums'],2);//实际-押金/人数 //对浮点数进行四舍五入
                // $data['pay_money'] = $data['price'];
                $data['paly_status'] = '1';//运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
                $data['baochang_status'] = '2';//包场状态:1=个人,2=包场,3=其他
                $data['pay_status'] = '1';//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
                $data['overtime_status'] = '1';//超时状态:1=未超时,2=超时
                $data['deposit_status'] = '0';//押金状态:0=未交押金,1=已支付押金,2=已退还押金
                $data['main_order'] = $order; //主订单
                $data['main_status'] = '2';//主订单:1=主订单,2=子订单

                $data['id'] = Db::name('order')->insertGetId($data);
                $res = $data;
            }
        }else{
            $this->error('生成预约子订单错误');
        }
        
        
        if($res){
            $Pay = new Pay();
            $pay_data = $Pay->pay_baochang_son_pay_order($res);
            $this->success('支付订单',$pay_data,8);
        }
        


        
        // return $data;
    }

    /**
     * 获取包场子订单预约信息
     *
     * @param array $order_data 订单数组
     * @return array
     */
    protected function get_baochang_son_yuyue_order($order_data)
    {
        $len = 29-strlen(strval($this->auth->id));
        $data['order']  = 'Son'.$this->auth->id.substr($order_data['order'],-$len);
        $data['admin_id'] = $order_data['yuyue']['admin_id'];
       

        //判断是否有该场的预约 订单
        $res = Db::name('yuyue')->where($data)->find();
        if(!$res){
            $data['uid'] = $this->auth->id;
            $data['baochang_status'] = '2';//包场状态:1=个人,2=包场,3=其他
            $data['yuyue_status'] = '1';//预约状态:1=已预约,2=到场
            $data['nums'] = 1;
            $data['promoter_status'] = '2';//是否是发起人:1=是,2=否
            $data['pay_status'] = '1';//支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
            $data['random_status'] = '1';//随机队员:1=否,2=随机
            $data['s_time'] = $order_data['s_time'];
            $data['e_time'] = $order_data['e_time'];
            

            $data['time'] = time();
            $data['id'] = Db::name('yuyue')->insertGetId($data);
            
            return $data;
        }else{
            return $res;
        }
    }

    //添加包场的随机订单，不需要支付的，到场地人满就不能进了

}