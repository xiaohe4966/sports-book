<?php

namespace app\api\controller;

use app\api\controller\Xiaohe;
use EasyWeChat\Factory;
use think\Db;

use think\Config;
use think\Validate;//验证
use fast\Random;
use fast\Http;

use app\api\controller\Pay;

use app\admin\model\AuthGroup;
use app\admin\model\AuthGroupAccess;


/**
 * 后台任务⚽︎
 * 
 */
class Task extends Xiaohe
{

    public function overtime()
    {

        $time['zudui_overtime'] = 600;//组队超时，预约时间前多少秒


        //查询已完成的订单 押金未退的


        //查询包场超时提醒
        $list  = Db::name('order')
                    ->where('pay_status','2')
                    ->where('baochang_status','2')  //包场状态:1=个人,2=包场,3=其他
                    ->where('paly_status','2')  //运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
                    ->where('e_time','<',time())
                    ->where('overtime_status','1')
                    ->select();

        foreach ($list as $key => $val) {
            $this->add_msg($val['uid'],'包场超时');
            Db::name('order')->where('id',$val['id'])
                            ->update(['overtime_status'=>'2']); //超时状态:1=未超时,2=超时
        }

        $json['baochang_overtime'] = sizeof($list);
                            
        //组队超时  （自带队员没有拼满人数，退款）
        $list = Db::name('order')
                    ->where('pay_status','2')
                    ->where('baochang_status','2')  //包场状态:1=个人,2=包场,3=其他
                    ->where('paly_status','1')  //运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
                    ->where('main_status','1')   //主订单:1=主订单,2=子订单
                    ->where('random_status','1')        //随机队员:1=否,2=随机
                    ->where('s_time','<',time()+$time['zudui_overtime'])
                    ->select();
        foreach ($list as $key => $val) {
            $order_data = $this->get_baochang_all_data($val['order']);

            if($order_data['pay_nums']<($order_data['yuyue']['nums']-1)){
                $this->add_msg($val['uid'],'包场自带队员超时，未组队成功');
                // $this->//退款给此订单和此订单子队员
                $Pay = new Pay();
                $Pay->refund_baochang_order($val['order'],true);
                // Db::name('order')->where('id',$val['id'])->update('');
            }
            
            
        }




        //组队完成（结束后 退主订单的押金）







        
        //包场时间 结束都没有进场的
        $json['un_in_baochang'] = Db::name('order')
                            ->where('pay_status','2')
                            ->where('baochang_status','2')  //包场状态:1=个人,2=包场,3=其他
                            ->where('paly_status','1')  //运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
                            ->where('e_time','<',time())
                            ->update(['paly_status'=>'5']); //运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进
                  
        



        $this->success('ok',$json);
    }

}