<?php

namespace app\api\controller;

use app\common\controller\Api;
use EasyWeChat\Factory;
use think\Db;

use think\Config;
use think\Validate;//验证
use fast\Random;
use fast\Http;

use app\api\controller\Xiaohe;
use app\api\controller\Qrcode;

/**
 * 场馆⚽︎
 */
class Space extends Xiaohe
{

    protected $noNeedLogin = ['get_user_mobile'];
    protected $noNeedRight = ['*'];

    protected $space_id = null;




    // public $uid = '';
    public function _initialize()
    {
        parent::_initialize();
     
        // $uid = $this->request->param('uid');//用户uid 需要解密
        // if(!empty($uid))$this->auth->id_de($uid);
        // $this->request->filter('trim,strip_tags,htmlspecialchars');
    }


    /**
     * 申请场馆
     * @param string $name 场馆名
     * @param string $addr 详细地址
     * @param string $image 场馆主图
     * @param string $blis_image 营业执照图
     * @param string $photo_images 场馆照片
     * @param string $cate_ids 分类id
     * @param string $service 服务说明
     * @param string $specs 规格说明
     * @param string $tag_str 标签文字（,分割
     * @param string $description 简介
     * @param string $tel 手机号
     * @param string $lng 经度
     * @param string $lat 纬度
     * @param string $s_time 营业开始时间
     * @param string $e_time 营业结束时间
     * @param string $week 星期几营业数字（,分割
     * @param string $price 多少元每小时
     * @return void
     */
    public function apply_space(){
        $params = $this->request->param();

        $data['uid'] = $this->auth->id;
        $space = Db::name('space')->where($data)->find();

        if($space && $space['apply_status']!=3){
            $msg = ['1'=>'已注册过','2'=>'申请中','3'=>'已拒绝'];
            $this->error($msg[$space['apply_status']]);
        }

        //必传字段
        $field = ['name','addr',
        'blis_image',
        'lng',
        'lat',
        'tel'];
        foreach($field as $key=>$val){
            if(empty($params[$val])){
                $this->error($val.'字段不能为空');
            }else{
                $data[$val] = $params[$val];
            }
        }

        //有就填上
        $field2 = ['name','addr',
        'image',
        'blis_image',
        'photo_images',
        'cate_ids',
        'service',
        'specs',
        'tag_str',
        'description',
        'tel',
        'lng',
        'lat',
        's_time',
        'e_time',
        'week',
        // '',        
        'price'];
        foreach($field2 as $key=>$val){
            if(!empty($params[$val])){
                $data[$val] = $params[$val];
            }
        }



        $data['pay_status'] = '1';//支付状态:1=待支付,2=已支付
        $data['play_status'] = '1';//陪玩状态:1=正常营业,2=停止营业
        $data['time'] = time();
        $data['apply_status'] = '2';//申请状态:1=通过,2=申请中,3=已拒绝

        
        $space_id = Db::name('space')->insertGetId($data);
        $res = Db::name('device')->where('space_id',$space_id)->select();
        if(!$res){
            //插入设备id  设备:1=进,2=出
            $device['status'] = '1';//
            $device['space_id'] = $space_id;
            $device['time'] = time();
            $devices[] = $device; 
            $device['status'] = '2';//
            $devices[] = $device; 
            Db::name('device')->insertAll($devices);
            
            //修改设备场地设备id
            $up_data['in_device_id']= Db::name('device')->where('space_id',$space_id)->where('status','1')->value('id');
            $up_data['out_device_id']= Db::name('device')->where('space_id',$space_id)->where('status','2')->value('id');
            Db::name('space')->where('id',$space_id)->update($up_data);

        }else{
            $this->error('已有该设备');
        }

        $this->success('已申请');
       
    }

    /**
     * 修改场馆信息
     * @ApiWeigh (999)
     * @param string $name 场馆名
     * @param string $addr 详细地址
     * @param string $image 场馆主图
     * @param string $blis_image 营业执照图
     * @param string $photo_images 场馆照片
     * @param string $cate_ids 分类id
     * @param string $service 服务说明
     * @param string $specs 规格说明
     * @param string $tag_str 标签文字（,分割
     * @param string $description 简介
     * @param string $tel 手机号
     * @param string $lng 经度
     * @param string $lat 纬度
     * @param string $s_time 营业开始时间
     * @param string $e_time 营业结束时间
     * @param string $week 星期几营业数字（,分割
     * @param string $overtime_price 超时每小时价格
     * @param string $deposit 包场押金
     * @param string $price 散客每小时价格
     * @param string $baochang_price 包场每小时价格
     * @param string $province 省
     * @param string $city 市
     * @param string $area 区
     * 
     * 
     * 
     * @return void
     */
    public function edit_space()
    {
        $params = $this->request->param();

        
        $data['uid'] = $this->auth->id;
        $space = Db::name('space')->where($data)->find();

        if($space['apply_status']==3){
            $msg = ['1'=>'已注册过','2'=>'申请中','3'=>'已拒绝'];
            $this->error($msg[$space['apply_status']]);
        }

        $field = ['name','addr',
        'image',
        'blis_image',
        'photo_images',
        'cate_ids',
        'service',
        'specs',
        'tag_str',
        'description',
        'tel',
        'lng',
        'lat',
        's_time',
        'e_time',
        'week',
        'province',
        'city',
        'area',
        // '',
        'overtime_price',
        'deposit',
        'baochang_price',
       
        
        'price'];
        foreach($field as $key=>$val){
            if(!empty($params[$val])){
                // $this->error($val.'字段不能为空');
                $data[$val] = $params[$val];
            }
        }

        if($space['apply_status']=='1'){
            if(!empty($data['lng'])){
                $this->update_space_geo($space['admin_id']);//更新地理状态
            }
        }


    
        Db::name('space')->where('id',$space['id'])->update($data);
        $this->success('修改成功');

    }

    /**
     * 支付场馆费用
     *
     * @return void
     */
    public function pay_space()
    {
        $data['uid'] = $this->auth->id;
        $space = Db::name('space')->where($data)->find();
        if(!$space){
            $this->error('未注册场馆');
        }

        $order_data = Db::name('order_platform')->where('space_id',$space['id'])->find();
        $Pay = new Pay();
        //支付状态:1=待支付,2=已支付
        if($space['pay_status']=='1'){
            if(!$order_data){
                //创建平台支付订单
                $Order = new Order();
                $order_data = $Order->create_order_platform($space['id']);
            }elseif($order_data['pay_status']!='2'){
                
            }
            

            $order_data['title'] = '平台费用';
            $pay_data = $Pay->pay_order_platform($order_data);
            $this->success('ok',$pay_data);

        }else{
            $this->error('你已支付',$order_data,3);
        }
    }

    /**
     * 获取我的场馆数据
     * @ApiWeigh (998)
     * @return void
     */
    public function get_space_data()
    {
        $data['uid'] = $this->auth->id;
        $space = Db::name('space')->where($data)->find();
        $this->success('ok',$space);
    }


    /**
     * 判断是否是场馆的管理人员
     * @ApiInternal()
     * @return void
     */
    public function is_space()
    {
        $space = Db::name('space')->where('uid',$this->auth->id)->find();
        // halt($space);
        if(!$space){
            $this->error('非场馆人员');
        }
        if($space['apply_status']!='1'){
            $msg = ['1'=>'已注册过','2'=>'申请中','3'=>'已拒绝'];
            $this->error($msg[$space['apply_status']]);
        }
        return $space;
    }

    /**
     * 获取场馆二维码场馆❌
     *
     * @return void
     */
    public function get_space_qrcode()
    {
        $space = $this->is_space();
        $Qrcode  = new Qrcode();
        $data = $Qrcode->get_space_qrcode($space['admin_id'],true);
        $this->success('qrcode',$data);
    }


    /**
     * 获取流水记录
     *
     * @param integer $page 1
     * @param integer $limit 10
     * @return void
     */
    public function get_bill_space_list($page = 1,$limit =10)
    {
        $list = Db::name('bill_space')
                ->where('uid',$this->auth->id)
                ->order('time desc')
                ->page($page,$limit)
                ->select();
        $this->success('ok',$list);
    }


    /**
     * 获取场馆订单
     *
     * @param string $pay_status 支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
     * @param int $baochang_status 包场状态:1=个人,2=包场,3=其他
     * @param integer $page 1
     * @param integer $limit 10
     * @return void
     */
    public function get_space_order($pay_status=null,$baochang_status=null,$page=1,$limit=5)
    {
        $space = $this->is_space();

        $where = null;
        if($pay_status){
            $where['pay_status'] = ['=',$pay_status];
        }
        if($baochang_status){
            $where['baochang_status'] = ['=',$baochang_status];
        }
        
        $where['admin_id'] = ['=',$space['admin_id']];
        // halt($where);
        $list = Db::name('order')->where($where)->page($page,$limit)->order('time desc')->select();
        // halt($list);
        foreach($list as $key=>$val){
            $list[$key]['user'] = Db::name('user')->find($val['uid']);
            $list[$key]['yuyue'] = Db::name('yuyue')->where('order',$val['order'])->find();
        }
        $this->success('ok',$list);

    }






    /**
     * 获取预约列表
     *
     * @param integer $page 1
     * @param integer $limit 10
     * @param string $yuyue_status null预约状态:1=已预约,2=到场
     * @param string $baochang_status null包场状态:1=个人,2=包场,3=其他
     *  @param string $pay_status 支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
     * @return void
     */
    public function get_yuyue_list($yuyue_status = 'null',$page=1,$limit=5,$baochang_status = null,$pay_status=null)
    {
        $space = $this->is_space();

        $where = null;
        if($pay_status){
            $where['pay_status'] = ['=',$pay_status];
        }
        if($baochang_status){
            $where['baochang_status'] = ['=',$baochang_status];
        }
        if($yuyue_status){
            $where['yuyue_status'] = ['=',$yuyue_status];
        }
        $where['admin_id'] = ['=',$space['admin_id']];
        
        $list = Db::name('yuyue')->where($where)->page($page,$limit)->order('time desc')->select();
        foreach($list as $key=>$val){
            $list[$key]['user'] = Db::name('user')->find($val['uid']);
            $list[$key]['order_data'] = Db::name('order')->where('order',$val['order'])->find();
        }
        $this->success('ok',$list);
    }

    /**
     * 获取我的后台账号密码
     *
     * @return void
     */
    public function get_admin_passwd()
    {
        $space = $this->is_space();
        $data['uid'] = $this->auth->id;
        $data = Db::name('admin')->where('id',$space['id'])->find();
        $this->success('ok',$data);
    }




    /**
     * 添加提现
     *
     * @param integer $money 钱
     * @return void
     */
    public function add_tixian($money=1)
    {
        //创建订单，
        $space = Db::name('space')->where('uid',$this->auth->id)->find();
        
        if($space){
            if($money>$space['money']){
                $this->error('余额不足！');
            }
            $data['space_id'] = $space['id'];
            $data['pay_status'] = '1';//到账状态:1=待到账,2=已提现
            $data['time'] = time();
            $data['order'] = $this->rand_order('Tx');
            $data['uid'] = $this->auth->id;
            $data['money'] = $money;
            $data['status'] = '2';//状态:1=待审核,2=已通过,3=已拒绝


            $res = Db::name('bill_tixian')->insert($data);
            if($res){
                $Pay = new \app\api\controller\Pay();
                $Pay->space_tixian($data['order']);
                $this->success('成功提现');
            }
        }
        
    }

}