<?php

namespace app\api\controller;

use app\common\controller\Api;
use EasyWeChat\Factory;
use think\Db;

use think\Config;
use think\Validate;//验证
use fast\Random;
use QL\QueryList;

use app\api\controller\Xiaohe;


/**
 * 灯💡
 */
class Lamp extends Xiaohe
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    protected $stores = null;
    protected $accessToken = null;

    protected $username = 'benniao';
    protected $password = '123456';
    protected $domain = 'http://xmnengjia.com';


    // public $uid = '';
    public function _initialize()
    {
        parent::_initialize();
        $this->accessToken = Config::get('site.accessToken');
        if(!$this->accessToken){
            $this->accessToken = $this->get_accessToken();
            Config::set('site.accessToken',$this->accessToken);
        }
        // $uid = $this->request->param('uid');//用户uid 需要解密
        // if(!empty($uid))$this->auth->id_de($uid);
        // $this->request->filter('trim,strip_tags,htmlspecialchars');
    }

    // /**
    //  * 获取微信等配置
    //  * @ApiWeigh (999)
    //  * @ApiSummary  (获取微信支付等配置信息)
    //  * @param string $field 需要获取的某个字段名称
    //  * @return void
    //  */
    // public function get_config($field=null)
    // {
    //     if($field){
    //         $field = '.'.$field;
    //     }
    //     $data = Config::get('site'.$field);
    //     return json($data);
    // }

    /**
     * 获取assesstoken
     * 
     * @return void
     */
    public function get_accessToken($url = '/sdLamp/api/external/accessToken')
    {
       
       $url = $this->domain.$url;
       
       

       $params = [
        'username' => $this->username,
        'password' => $this->password
       ];
       $data = $this->http_post($url, $params);
       
        
         
        if(!empty($data['data'])){
            return $data['data'];
        }
      
    }

    /**
     * 添加设备
     *
     * @param string $serial 二维码内容
     * @return void
     */
    public function add_lamp($url = '/sdLamp/api/external/addDevice',$serial=null)
    {

        $url = $this->domain.$url;
        $params = [
            'accessToken' => $this->accessToken,
            'serial' => $serial
           ];
        $data = $this->http_post($url, $params);
        if($data['code']==1000){
            $this->success('添加成功',$data['data']);
            //{"totalRow":0,"pageNumber":1,"lastPage":true,"firstPage":true,"totalPage":0,"pageSize":10,"list":[]}
        }

    }
    
    /**
     * 查看某个设备状态
     *
     * @param string $serial 二维码内容
     * @return void
     */
    public function lamp_status($url = '/sdLamp/api/external/deviceStatus',$serial=null)
    {

        $url = $this->domain.$url;
        $params = [
            'accessToken' => $this->accessToken,
            'serial' => $serial
           ];
        $data = $this->http_post($url, $params);
        if($data['code']==1000){
            $this->success('ok',$data['data']);
            
        }

    }

    

    /**
     * 灯开
     *
     * @param string $time 开灯时间 
     * @param string $serial 二维码内容
     * @return void
     */
    public function lamp_open($url = '/sdLamp/api/external/distributeParam',$serial=null,$time=null)
    {

        $url = $this->domain.$url;
        if(!$time){
            $timeBrightness = date('h:i').'_01';
        }else{
            $timeBrightness = $time.'_01';
        }
        
        $params = [
            'accessToken' => $this->accessToken,
            'serial' => $serial,
            'timeBrightness' => $timeBrightness

           ];
        $data = $this->http_post($url, $params);
        
        if($data['code']==1001){
            $this->success('ok',$data['data']);
            
        }else{
            return json($data);
        }

    }


    /**
     * 灯关
     *
     * @param string $time 关灯时间 
     * @param string $serial 二维码内容
     * @return void
     */
    public function lamp_close($url = '/sdLamp/api/external/distributeParam',$serial=null,$time=null)
    {

        $url = $this->domain.$url;
        if(!$time){
            $timeBrightness = date('h:i').'_00';
        }else{
            $timeBrightness = $time.'_00';
        }
        
        $params = [
            'accessToken' => $this->accessToken,
            'serial' => $serial,
            'timeBrightness' => $timeBrightness

           ];
        $data = $this->http_post($url, $params);
        // halt($data);
        if($data['code']==1001){
            $this->success('ok',$data['data']);
           
        }else{
            return json($data);
        }

    }



    /**
     * 灯亮度调节
     *
     * @param string $pwm 脉冲等级
     * @param string $time 开灯时间 
     * @param string $serial 二维码内容
     * @return void
     */
    public function lamp_pwm($url = '/sdLamp/api/external/distributeParam',$serial=null,$time=null,$pwm='50')
    {

        $url = $this->domain.$url;
        if(!$time){
            $timeBrightness = date('h:i').'_02_'.$pwm;
        }else{
            $timeBrightness = $time.'_02_'.$pwm;
        }
        
        $params = [
            'accessToken' => $this->accessToken,
            'serial' => $serial,
            'timeBrightness' => $timeBrightness

           ];
        $data = $this->http_post($url, $params);
        
        if($data['code']==1001){
            $this->success('ok',$data['data']);
            
        }else{
            return json($data);
        }

    }

    /**
     * 获取所有灯列表
     *
     * @param string $url
     * @param integer $page
     * @param integer $limit
     * @return void
     */
    public function get_lamp_list($url = '/sdLamp/api/external/deviceList', $page=1,$limit=10)
    {
        $url = $this->domain.$url;
        $params = [
            'accessToken' => $this->accessToken,
            'pageNumber' => $page,
            'pageSize'  =>$limit
           ];
        $res = $this->http_post($url, $params);
        halt($res);
        // "totalRow" => 1
        // "pageNumber" => 1
        // "lastPage" => true
        // "firstPage" => true
        // "totalPage" => 1
        // "pageSize" => 10
        // "list" => array:1 [▼
        // 0 => array:11 [▼
        //     "is_lighting" => 0
        //     "current" => 0.0
        //     "brightness" => 14
        //     "serial" => "n56a1eaee5a343c5a420210106685145"
        //     "total_active_power" => 0.0
        //     "reactive_power" => 0.89
        //     "active_power" => 22.08
        //     "is_online" => 0
        //     "signal_strength" => 0
        //     "timestamp" => 1619684478444
        //     "voltage" => 0.0
        // ]
        // ]
    }

    /**
     * 获取场馆信息
     *
     * @param int $space_id
     * @return void
     */
    public function get_space_data($space_id)
    {
        $data['space'] = $this->verify_space_id($space_id);
        $data['online'] = $this->get_space_online_num($space_id);

        $this->success('ok',$data);
    }


    /**
     * 获取首页信息
     * @ApiSummary  (获取banner,会员总数，场馆总数)
     * @return void
     */
    public function get_index_data()
    {
        $data['user_nums'] = Db::name('user')->count();
        $data['space_nums'] = Db::name('space')->where('apply_status','1')->count();
        $data['banner'] = Db::name('banner')->order('weigh desc')->select();
        $this->success('ok',$data);
    }



    public function http_post($url,$params){


        $ql = QueryList::post($url,$params);
        $data = json_decode($ql->getHtml(),true);
        return $data;
    }
    

}