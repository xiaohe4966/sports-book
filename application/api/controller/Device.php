<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Db;

use think\Config;
use think\Validate;//验证
use fast\Random;
use fast\Http;

use app\api\controller\Xiaohe;
use QL\QueryList;
use GuzzleHttp\Cookie\CookieJar;


/**
 * 设备⚽︎
 */
class Device extends Xiaohe
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];


    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 开门(调试用)
     *
     * @param string $device_id 设备id
     * @param string $status 状态/in/out
     * @return void
     */
    public function door_open($device_id=null,$status=null,$code=null)
    {
        



        $ql = QueryList::post('http://he4966.cn/xiaohe/Login/checkLogin.html', [
            'vgdecoderesult' => $code,
            'devicenumber' => $device_id
            ], [
            'headers' => [
                // 'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
                // 'Accept-Encoding' => 'gzip, deflate, br',
                'Accept-encoding' => 'deflate', 
                'Accept' => 'text/html; q=0.01', 
                'Connection' => 'close, TE', 
                'Host' => 'he4966.cn:8090', 
                'User-agent' => 'LuaSocket 3.0-rc1', 
                'Content-type' => 'text/html; charset=UTF-8', 
                'Accept-language' => 'zh-CN,zh;q=0.8', 
                'Te' => 'trailers' 

            ]
        ]);
        // $ql = QueryList::post('http://he4966.cn/xiaohe/Login/checkLogin.html', [
        //         'username' => 'xiaohe',
        //         'password' => 'xiaohe'
        //         ], [
        //     'headers' => [
        //         'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
        //         'Accept-Encoding' => 'gzip, deflate, br',
        //     ]
        // ]);

        $data = $ql->getHtml();
        return($data);
    }

    /**
     * 添加日志
     * @ApiInternal()
     * @param string $id 设备id
     * @param string $status 设备:1=进,2=出
     * @param string $order 订单号
     * @param string $ps 备注
     * @return void
     */
    public function add_device_log($id,$order,$ps=null,$uid=null)
    {

  
      
      
        $device = Db::name('device')->where('id',$id)->find();
        // $data['admin_id'] = Db::name('device')->where('id',$id)->value('space_id');
        $data['admin_id'] = $device['space_id'];

        $data['status'] = $device['status'];//设备:1=进,2=出
        $data['device_id'] = $id;
        
        $data['order'] = $order;
        switch ($device['status']) {
            case '1':
                $data['ps'] = '进';
                break;
            case '2':
                $data['ps'] = '出';
                break;
            default:
                $data['ps'] = '未知';
                break;
        }
        $data['uid'] = $uid;
        $data['time'] = time();
        Db::name('device_log')->insert($data);

        Db::name('device')->where('id',$id)->update(['updatetime'=>time()]);
    }
}