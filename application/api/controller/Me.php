<?php

namespace app\api\controller;

use app\common\controller\Api;
use EasyWeChat\Factory;
use think\Db;

use think\Config;
use think\Validate;//验证
use fast\Random;
use fast\Http;

use app\api\controller\Xiaohe;


/**
 * 我的⚽︎
 */
class Me extends Xiaohe
{

    protected $noNeedLogin = ['get_openid','get_user_mobile'];
    protected $noNeedRight = ['*'];

    protected $stores = null;




    // public $uid = '';
    public function _initialize()
    {
        parent::_initialize();
     
        // $uid = $this->request->param('uid');//用户uid 需要解密
        // if(!empty($uid))$this->auth->id_de($uid);
        // $this->request->filter('trim,strip_tags,htmlspecialchars');
    }

    /**
     * 获取openid
     * @ApiSummary (里面有token，提交接口都要带上)
     * @param [type] $code
     * @return void
     */
    public function get_openid($code=null){
    
        $app = $this->get_app();
        $user = $app->auth->session($code);
        if(!empty($user['errcode'])){
            halt($user);
        }
        $openid = $user['openid'];
     
        $data = array();
        $data['openid'] = $openid;
        $user_data = Db::name('user')->where('openid',$openid)->find();
        if($user_data){
            $ret = $this->auth->direct($user_data['id']);
            $data = $this->auth->getUserinfo();
            // $data['uid'] = $user_data['id'];


            // $data['logintime'] = time();
            //开放平台
            if(empty($user_data['unionid']) && (!empty($user['unionid']))){
                Db::name('user')->where('openid',$openid)->update(['unionid'=>$user['unionid']]);
            }
        }else{

            $indata['openid'] = $openid;
            $indata['createtime'] = time();
            $indata['jointime'] = $indata['createtime'];
            $indata['joinip'] = $this->request->ip();
            $indata['logintime'] = time();
            $indata['status'] = 'normal';

            // $indata['stateswitch'] = 1;

            //开放平台
            if(!empty($user['unionid'])){
                $indata['unionid'] = $user['unionid'];
            }
            
     
            $id = Db::name('user')->insertGetId($indata);
            $ret = $this->auth->direct($id);

            $data = $this->auth->getUserinfo();
        }
        
        // $data['is']     = $data['uid'];//未知
        return json($data);
    }



    /**
     * 修改小程序用户的昵称和头像
     *
     * @param string $nickname 昵称
     * @param string $img 头像
     * @return void
     */ 
    public function che_user($nickname,$img)
    {   
        
        $u['id'] = $this->auth->id;
        $json['code'] = -1;
        $json['msg'] = '修改失败';

        $data['nickname'] = $nickname;
        $data['headimage'] = $img;
        $res = Db::name('user')->where($u)->update($data);
        if($res){
            $json['code'] = 1;
            $json['msg'] = '修改成功';
        }
        return json($json);
    }


    /**
     * 获取用户信息
     *
     * @return void
     */
    public function get_user()
    {
        

        $list['user'] = Db::name('user')->find($this->auth->id);
        $list['user']['play_min'] = ceil($this->get_user_paly_time($this->auth->id)/60);
        $list['space'] = Db::name('space')->where('uid',$this->auth->id)->find();
        // $list['user']['vip_stime'] = date('Y-m-d',$list['user']['vip_stime']);
        // $list['user']['vip_etime'] = date('Y-m-d',$list['user']['vip_etime']);


        return json($list);
    }



    /**
     * 修改上级
     *
     * @param string $pid 用户id
     * @return void
     */
    public function update_pid($pid)
    {
        
        $user = Db::name('user')->find($this->auth->id);
        if($user){
            if(!empty($user['pid'])){
                $this->error('已经有上级'.$user['pid']);
            }
        }

        
        $res_up = Db::name('user')->where('id',$this->auth->id)->update(['pid'=>$pid]);
        if($res_up){
            $this->success('绑定成功');
        }
    } 
    


    /**
     * 小程序授权获取手机号
     *
     * @param string $code
     * @return void
     */    
    public function get_user_mobile($code=null)
    {   
        $app = $this->get_app();
        $data = $this->request->param();
        $user = $app->auth->session($code);
        $iv = $data['iv'];
        $encryptedData = $data['encryptedData'];

        $session = $user['session_key'];//$app->sns->getSessionKey($code);
        $decryptedData = $app->encryptor->decryptData($session, $iv, $encryptedData);

        //修改手机号
        Db::name('user')->where('openid',$user['openid'])->update(['mobile'=>$decryptedData['phoneNumber']]);
        
        return $decryptedData['phoneNumber'];
        // halt($decryptedData);
        
    }




    /**
     * 获取我的邀请人数
     *
     * @param boolean $self 掉接口不需要传
     * @return void
     */
    public function user_get_my_share_num($self=false)
    {
        
        $num = Db::name('user')
                    ->where('pid',$this->auth->id)
                    ->count();
        if($self){
            return $num; 
        }
        $this->success('👌',$num);
        

    }

    /**
     * 获取我的邀请列表
     *
     * @param integer $page 1
     * @param integer $limit 200
     * @return void
     */
    public function get_share_list($page = 1,$limit = 200)
    {

        $list = Db::name('user')
                    ->where('pid',$this->auth->id)
                    ->order('createtime desc')
                    ->field([ '*,FROM_UNIXTIME(createtime,"%Y-%m-%d %H:%m:%s") as date'])
                    ->select();
        $this->success(sizeof($list), $list);
    }


    /**
     * 获取我的计时进门二维码💮
     *
     * @return void
     */
    public function get_mydoor_qrcode()
    {
        //
        $Qrcode  = new Qrcode();
        $data = $Qrcode->get_in_door_qrcode($this->auth->id,true);
        $this->success('open_door_qrcode',$data);
  
    }


    /**
     * 获取包场订单
     *
     * @param string $pay_status 支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
     * @param int $baochang_status 包场状态:1=个人,2=包场,3=其他
     * @param integer $page 1
     * @param integer $limit 10
     * @return void
     */
    public function get_order($pay_status=null,$baochang_status=null,$page=1,$limit=5)
    {
        // $space = $this->is_space();

        $where = null;
        if($pay_status){
            $where['pay_status'] = ['=',$pay_status];
        }
        if($baochang_status){
            $where['baochang_status'] = ['=',$baochang_status];
        }
        
        $where['uid'] = ['=',$this->auth->id];

        $list = Db::name('order')->where($where)->page($page,$limit)->order('time desc')->select();
        foreach($list as $key=>$val){
            $list[$key]['space'] = Db::name('space')->find($val['admin_id']);
        }
        $this->success('ok',$list);

    }



    /**
     * 获取计时订单列表
     *
     * @param integer $page 1
     * @param integer $limit 10
     * @param string $yuyue_status null预约状态:1=已预约,2=到场
     * @param string $baochang_status null包场状态:1=个人,2=包场,3=其他
     *  @param string $pay_status 支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款
     * @return void
     */
    public function get_yuyue_list($yuyue_status = 'null',$page=1,$limit=5,$baochang_status = null,$pay_status=null)
    {
        

        $where = null;
        if($pay_status){
            $where['pay_status'] = ['=',$pay_status];
        }
        if($baochang_status){
            $where['baochang_status'] = ['=',$baochang_status];
        }
        if($yuyue_status){
            $where['yuyue_status'] = ['=',$yuyue_status];
        }
        $where['uid'] = ['=',$this->auth->id];
        
        $list = Db::name('yuyue')->where($where)->page($page,$limit)->order('time desc')->select();
        foreach($list as $key=>$val){
            $list[$key]['space'] = Db::name('space')->find($val['admin_id']);
            $list[$key]['order_data'] = Db::name('order')->where('order',$val['order'])->find();
        }
        $this->success('ok',$list);
    }

    /**
     * 获取我的包场分享二维码💮
     *
     * @param string $order 订单
     * @return void
     */
    public function get_baochang_qr($order)
    {
       $Qrcode = new \app\api\controller\Qrcode();
       $url = $Qrcode->get_share_baochang_qrcode($order);
       if($url){
           $this->success('ok','/'.$url);
       }
    }


    /**
     * 获取分享的订单数据
     *
     * @param string $order 订单
     * @return void
     */
    public function get_share_order_data($order)
    {
        $order_data = Db::name('order')->where('order',$order)->find();
        //暂时未验证
        // if($order_data['random_status']=='1' && $order_data['baochang_status'] && $order_data['pay_status']=='2'){
            $order_data['yuyue'] = Db::name('yuyue')->where('order',$order)->find();
            $order_data['pay_nums'] = Db::name('order')->where('main_order',$order)->where('pay_status','2')->count()+1;//已报名人数
            $order_data['space'] = Db::name('space')->find($order_data['admin_id']);
            $order_data['one_price'] = $this->get_baochang_son_price($order);
            $this->success('ok',$order_data);
        // }else{
        //     $this->error('订单未支付或非包场');
        // }
        
    }



    /**
     * 获取包场进门二维码💮
     *
     * @param string $order 订单
     * @return void
     */
    public function get_baochang_in_door_qr($order)
    {
        $order_data = Db::name('order')->where('order',$order)->find();
        if($order_data['uid']!=$this->auth->id){
            $this->error('非本人订单');
        }
        if($order_data['pay_status']!='2'){
            $this->error('非支付状态');
        }

        $Qrcode = new \app\api\controller\Qrcode();
        $qr = $Qrcode->get_baochang_in_door_qrcode($order,true);
        $this->success('包场进门',$qr);
    }


    /**
     * 获取包场出门二维码💮
     *
     * @param string $order 订单
     * @return void
     */
    public function get_baochang_out_door_qr($order)
    {
        $order_data = Db::name('order')->where('order',$order)->find();
        if($order_data['uid']!=$this->auth->id){
            $this->error('非本人订单');
        }
        if($order_data['pay_status']!='2'){
            $this->error('非支付状态');
        }

        $Qrcode = new \app\api\controller\Qrcode();
        $qr = $Qrcode->get_baochang_out_door_qrcode($order,true);
        $this->success('包场出门',$qr);
    }



  

    /**
     * 支付后支付订单-个人到场
     *
     * @param string $order 订单号
     * @return void
     */
    public function pay_daochang_order($order)
    {
        // $space = $this->verify_space_id($id);
        $yuyue_order = Db::name('yuyue')->where('order',$order)->find();
        if(!$yuyue_order)$this->error('没有此订单');
        // if($yuyue_order['baochang_status']!='1'){
        //     $this->error('此订单非个人订单');
        // }


        $uid = $this->auth->id;

        $Yuyue = new Yuyue();
        $Pay   = new   Pay();

        //判断 是否有后付费的进场订单 并支付
        $end_pay_order = $Yuyue->get_end_pay_order_and_pay($yuyue_order['admin_id'],$uid);
        if($end_pay_order){
            $pay_data = $Pay->pay_end_pay_order($end_pay_order);
            $this->success('支付订单',$pay_data,8);
        }
    }




    // /**
    //  * 获取到场出门二维码💮
    //  *
    //  * @param string $order 订单
    //  * @return void
    //  */
    // public function get_out_door_qrcode($order)
    // {
    //     $order_data = Db::name('order')->where('order',$order)->find();
    //     if($order_data['uid']!=$this->auth->id){
    //         $this->error('非本人订单');
    //     }
    //     if($order_data['pay_status']!='2'){
    //         $this->error('未支付状态');
    //     }

    //     $Qrcode = new \app\api\controller\Qrcode();
    //     $qr = $Qrcode->get_out_door_qrcode($order,true);
    //     $this->success('到场出门',$qr);
    // }

    /**
     * 获取到场出门二维码💮
     *
     * @param string $order 订单
     * @return void
     */
    public function get_daochang_out_door_qr($order)
    {
        $order_data = Db::name('order')->where('order',$order)->find();
        if($order_data['uid']!=$this->auth->id){
            $this->error('非本人订单');
        }
        if($order_data['pay_status']!='2'){
            $this->error('非支付状态');
        }

        $Qrcode = new \app\api\controller\Qrcode();
        $qr = $Qrcode->get_out_door_qrcode($order,true);
        $this->success('到场出门',$qr);
    }


    /**
     * 获取最新的消息 
     *
     * @param integer $limit 默认2
     * @return void
     */
    public function get_new_msg($limit=2)
    {
        $list = Db::name('msg')
                ->where('uid',$this->auth->id)
                ->where('status','1')
                ->order('time desc')
                ->limit($limit)
                ->select();

        $this->success(sizeof($list),$list);
    }

    /**
     * 获取消息列表
     *
     * @param integer $page 默认1
     * @param integer $limit 默认10
     * @return void
     */
    public function get_msg_list($page = 1,$limit=10)
    {
        $list = Db::name('msg')
                ->where('uid',$this->auth->id)
                ->order('time desc')
                ->page($page,$limit)
                ->select();

        $this->success('ok',$list);
    }


}