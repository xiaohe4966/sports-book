define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'order/index' + location.search,
                    add_url: 'order/add',
                    edit_url: 'order/edit',
                    del_url: 'order/del',
                    multi_url: 'order/multi',
                    import_url: 'order/import',
                    table: 'order',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'order', title: __('Order'), operate: 'LIKE'},
                        {field: 'uid', title: __('Uid')},
                        {field: 'admin_id', title: __('Admin_id')},
                        {field: 'time', title: __('Time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'coupons_id', title: __('Coupons_id')},
                        {field: 'price', title: __('Price'), operate:'BETWEEN'},
                        {field: 'money', title: __('Money'), operate:'BETWEEN'},
                        {field: 'cut_price', title: __('Cut_price'), operate:'BETWEEN'},
                        {field: 'pay_money', title: __('Pay_money')},
                        {field: 'pay_type', title: __('Pay_type'), searchList: {"1":__('Pay_type 1'),"2":__('Pay_type 2')}, formatter: Table.api.formatter.normal},
                        {field: 'ps', title: __('Ps'), operate: 'LIKE'},
                        {field: 'paly_status', title: __('Paly_status'), searchList: {"1":__('Paly_status 1'),"2":__('Paly_status 2'),"3":__('Paly_status 3'),"4":__('Paly_status 4'),"5":__('Paly_status 5')}, formatter: Table.api.formatter.status},
                        {field: 'baochang_status', title: __('Baochang_status'), searchList: {"1":__('Baochang_status 1'),"2":__('Baochang_status 2'),"3":__('Baochang_status 3')}, formatter: Table.api.formatter.status},
                        {field: 'e_time', title: __('E_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 's_time', title: __('S_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'pay_status', title: __('Pay_status'), searchList: {"1":__('Pay_status 1'),"2":__('Pay_status 2'),"8":__('Pay_status 8'),"9":__('Pay_status 9'),"10":__('Pay_status 10')}, formatter: Table.api.formatter.status},
                        {field: 'user_ps', title: __('User_ps'), operate: 'LIKE'},
                        {field: 'money_status', title: __('Money_status'), searchList: {"1":__('Money_status 1'),"2":__('Money_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'send_msg', title: __('Send_msg'), operate: 'LIKE'},
                        {field: 's_door_time', title: __('S_door_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'pay_time', title: __('Pay_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'e_door_time', title: __('E_door_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'deposit', title: __('Deposit'), operate:'BETWEEN'},
                        {field: 'overtime_price', title: __('Overtime_price'), operate:'BETWEEN'},
                        {field: 'overtime_status', title: __('Overtime_status'), searchList: {"1":__('Overtime_status 1'),"2":__('Overtime_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'refund_deposit', title: __('Refund_deposit'), operate:'BETWEEN'},
                        {field: 'deposit_status', title: __('Deposit_status'), searchList: {"0":__('Deposit_status 0'),"1":__('Deposit_status 1'),"2":__('Deposit_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'main_order', title: __('Main_order'), operate: 'LIKE'},
                        {field: 'main_status', title: __('Main_status'), searchList: {"1":__('Main_status 1'),"2":__('Main_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'random_status', title: __('Random_status'), searchList: {"1":__('Random_status 1'),"2":__('Random_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'refund_order', title: __('Refund_order'), operate: 'LIKE'},
                        {field: 'refund_ps', title: __('Refund_ps'), operate: 'LIKE'},
                        {field: 'user.nickname', title: __('User.nickname'), operate: 'LIKE'},
                        {field: 'user.headimage', title: __('User.headimage'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'user.openid', title: __('User.openid'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});