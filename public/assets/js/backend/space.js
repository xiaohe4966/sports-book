define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'space/index' + location.search,
                    add_url: 'space/add',
                    edit_url: 'space/edit',
                    del_url: 'space/del',
                    multi_url: 'space/multi',
                    import_url: 'space/import',
                    table: 'space',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'addr', title: __('Addr'), operate: 'LIKE'},
                        {field: 'image', title: __('Image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'blis_image', title: __('Blis_image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'play_status', title: __('Play_status'), searchList: {"1":__('Play_status 1'),"2":__('Play_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'online_status', title: __('Online_status'), searchList: {"1":__('Online_status 1'),"2":__('Online_status 2'),"3":__('Online_status 3')}, formatter: Table.api.formatter.status},
                        {field: 'money', title: __('Money'), operate:'BETWEEN'},
                        {field: 'tel', title: __('Tel'), operate: 'LIKE'},
                        {field: 'apply_status', title: __('Apply_status'), searchList: {"1":__('Apply_status 1'),"2":__('Apply_status 2'),"3":__('Apply_status 3')}, formatter: Table.api.formatter.status},
                        {field: 's_time', title: __('S_time'), operate: 'LIKE'},
                        {field: 'e_time', title: __('E_time'), operate: 'LIKE'},
                        {field: 'pay_status', title: __('Pay_status'), searchList: {"1":__('Pay_status 1'),"2":__('Pay_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'week', title: __('Week'), operate: 'LIKE'},
                        {field: 'baochang_price', title: __('Baochang_price'), operate:'BETWEEN'},
                        {field: 'price', title: __('Price'), operate:'BETWEEN'},
                        {field: 'overtime_price', title: __('Overtime_price'), operate:'BETWEEN'},
                        {field: 'in_device_id', title: __('In_device_id')},
                        {field: 'out_device_id', title: __('Out_device_id')},
                        {field: 'user.nickname', title: __('User.nickname'), operate: 'LIKE'},
                        {field: 'cate.cate_name', title: __('Cate.cate_name'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});