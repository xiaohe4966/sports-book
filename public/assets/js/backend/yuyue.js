define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'yuyue/index' + location.search,
                    add_url: 'yuyue/add',
                    edit_url: 'yuyue/edit',
                    del_url: 'yuyue/del',
                    multi_url: 'yuyue/multi',
                    import_url: 'yuyue/import',
                    table: 'yuyue',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'admin_id', title: __('Admin_id')},
                        {field: 'uid', title: __('Uid')},
                        {field: 's_time', title: __('S_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'e_time', title: __('E_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'yuyue_status', title: __('Yuyue_status'), searchList: {"1":__('Yuyue_status 1'),"2":__('Yuyue_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'baochang_status', title: __('Baochang_status'), searchList: {"1":__('Baochang_status 1'),"2":__('Baochang_status 2'),"3":__('Baochang_status 3')}, formatter: Table.api.formatter.status},
                        {field: 'order', title: __('Order'), operate: 'LIKE'},
                        {field: 'nums', title: __('Nums')},
                        {field: 'ps', title: __('Ps'), operate: 'LIKE'},
                        {field: 'promoter_status', title: __('Promoter_status'), searchList: {"1":__('Promoter_status 1'),"2":__('Promoter_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'pay_status', title: __('Pay_status'), searchList: {"1":__('Pay_status 1'),"2":__('Pay_status 2'),"8":__('Pay_status 8'),"9":__('Pay_status 9'),"10":__('Pay_status 10')}, formatter: Table.api.formatter.status},
                        {field: 'random_status', title: __('Random_status'), searchList: {"1":__('Random_status 1'),"2":__('Random_status 2')}, formatter: Table.api.formatter.status},
                        {field: 'time', title: __('Time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'order.price', title: __('Order.price'), operate:'BETWEEN'},
                        {field: 'order.money', title: __('Order.money'), operate:'BETWEEN'},
                        {field: 'order.cut_price', title: __('Order.cut_price'), operate:'BETWEEN'},
                        {field: 'order.pay_money', title: __('Order.pay_money')},
                        {field: 'order.pay_type', title: __('Order.pay_type')},
                        {field: 'order.paly_status', title: __('Order.paly_status'), formatter: Table.api.formatter.status},
                        {field: 'order.baochang_status', title: __('Order.baochang_status'), formatter: Table.api.formatter.status},
                        {field: 'order.pay_status', title: __('Order.pay_status'), formatter: Table.api.formatter.status},
                        {field: 'order.main_order', title: __('Order.main_order'), operate: 'LIKE'},
                        {field: 'order.main_status', title: __('Order.main_status'), formatter: Table.api.formatter.status},
                        {field: 'order.random_status', title: __('Order.random_status'), formatter: Table.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});