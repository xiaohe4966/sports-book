<?php

return [
    [
        //配置唯一标识
        'name'    => 'usernmae',
        //显示的标题
        'title'   => '用户名',
        //类型
        'type'    => 'string',
        //数据字典
        'content' => [
        ],
        //值
        'value'   => '',
        //验证规则 
        'rule'    => 'required',
        //错误消息
        'msg'     => '',
        //提示消息
        'tip'     => '',
        //成功消息
        'ok'      => '',
        //扩展信息
        'extend'  => ''
    ],
    [
        'name'    => 'password',
        'title'   => '密码',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name' => '__tips__',
        'title' => '温馨提示',
        'type' => '',
        'content' => [],
        'value' => '1、请在开启redis插件，并在config.php里面配置redis的正确配置，使用说明:<a href="https://he4966.cn/index" target="_blank">https://he4966.cn/index</a><br/>啊啊啊<a href="https://www.fastadmin.net/go/aliyun" target="_blank">点击领取</a><br>'."\n"
            .'2、直接可以调用<code>data-chunking="true"</code>',
        'rule' => '',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
];
