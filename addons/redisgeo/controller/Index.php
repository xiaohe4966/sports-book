<?php

namespace addons\redisgeo\controller;

use think\addons\Controller;
use addons\redisgeo\library\Redis;

class Index extends Controller
{
    

    public function _initialize()
    {
        parent::_initialize();
        $this->redis = $this->getRedis();
    }

    public function index()
    {   
        // var_dump($this->del_geo_all());//删除全部坐标信息 非必要慎用

        
        
        //生成10个坐标添加
        for ($i=1; $i <=10; $i++) { 
            $val['lng'] = 120.320011+$i*0.0005;
            $val['lat'] = 31.68000+$i*0.0005;
            $val['id']  = $i;
            $this->add_geo($val['lng'],$val['lat'],$val['id']);
        }

        //删除id为1的坐标
        // $this->del_geo(1);

        //找这个坐标的附近400米的信息数量8个
        $val['lng'] = 120.3200011;
        $val['lat'] = 31.68540;

        $list = $this->get_nearby_list($val['lng'],$val['lat'],400,'m',8,'ASC');
        halt($list);
    }


    /**
     * 获取redis对象
     *
     * @return void
     */
    public function getRedis() {

        if (!isset($GLOBALS['REDIS'])) {
            $GLOBALS['REDIS'] = (new Redis())->getRedis();
        }
        return $GLOBALS['REDIS'];
    }

    /**
     * 获取附近的列表
     *
     * @param float $lng 经度
     * @param float $lat 纬度
     * @param int $m 距离多远
     * @param string $unit 单位:m米,km千米,mi英里,ft英尺
     * @param int $limit 需要的数量
     * @param string $sort 排序ASC,DESC
     * @param string $name 用于分类名
     * @return void
     */
    public function get_nearby_list($lng,$lat,$m=1000,$unit='m',$limit=5,$sort='ASC',$name='geo:space')
    {   

        if($limit<1){
            $limit = 10;
        }else{
            $limit = intval($limit);
        }
       

        $list = $this->redis->georadius($name,floatval($lng),floatval($lat),floatval($m),$unit,['WITHDIST', 'COUNT' => $limit, $sort]);
        return $list;
       
    }


    /**
     * 添加坐标(编辑这个id坐标也用这个)
     *
     * @param string $lng 经度
     * @param string $lat 纬度
     * @param int $m 距离多远
     * @param int $id 数据id
     * @param string $name 用于分类名
     * @return void
     */
    public function add_geo($lng,$lat,$id,$name='geo:space')
    {   
        //提示：如果有一样的$id，新坐标的会覆盖掉原来的坐标

        $this->redis->geoadd($name,$lng,$lat,$id);
    }

    /**
     * 删除某个坐标
     *
     * @param int $id 数据id
     * @param string $name 用于分类名
     * @return void
     */
    public function del_geo($id,$name='geo:space')
    {   
        
        $this->redis->zrem($name,$id);
    }   

    /**
     * 删除全部地理坐标
     * 
     * @param string $name 用于分类名
     * @return void
     */
    public function del_geo_all($name='geo:space')
    {
        
        $nums = $this->redis->zcard($name);
        for ($i=0; $i <= $nums; $i++) { 
            $this->del_geo($i+1);
        }
        return $nums;
    }



  

}
