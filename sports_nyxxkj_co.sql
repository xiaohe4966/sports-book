/*
 Navicat Premium Data Transfer

 Source Server         : 笨鸟运动
 Source Server Type    : MySQL
 Source Server Version : 50650
 Source Host           : court.nyxxkj.com:3306
 Source Schema         : sports_nyxxkj_co

 Target Server Type    : MySQL
 Target Server Version : 50650
 File Encoding         : 65001

 Date: 19/05/2021 13:42:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sp_admin
-- ----------------------------
DROP TABLE IF EXISTS `sp_admin`;
CREATE TABLE `sp_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '昵称',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码',
  `salt` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '头像',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录IP',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(59) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Session标识',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员表';

-- ----------------------------
-- Records of sp_admin
-- ----------------------------
BEGIN;
INSERT INTO `sp_admin` VALUES (1, 'superadmin', 'Admin', '8c876fa6e2db980b5e1a3895860cab7b', 'fca989', '/assets/img/avatar.png', 'superadmin@admin.com', 0, 1620722836, '114.223.143.173', 1491635035, 1620722836, '21d4dcee-f0b2-4b1a-98f5-7e66e5aa8a63', 'normal');
INSERT INTO `sp_admin` VALUES (5, '18333333333', '', 'a72f9dfe8226af566b9de8e388e9a8fd', 'wAD1j8', '/assets/img/avatar.png', '', 0, NULL, NULL, 1618900933, 1618900933, '', 'normal');
INSERT INTO `sp_admin` VALUES (7, '18205034747', '诺远球馆', 'c0c006be0cf90a3fe1141564196e0d9a', '8sY6EK', '/assets/img/avatar.png', '', 0, NULL, NULL, 1618906435, 1618906435, '', 'normal');
COMMIT;

-- ----------------------------
-- Table structure for sp_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `sp_admin_log`;
CREATE TABLE `sp_admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '日志标题',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `name` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员日志表';

-- ----------------------------
-- Records of sp_admin_log
-- ----------------------------
BEGIN;
INSERT INTO `sp_admin_log` VALUES (31, 1, 'superadmin', '/sports.php/index/login?url=%2Fsports.php', '登录', '{\"url\":\"\\/sports.php\",\"__token__\":\"***\",\"username\":\"superadmin\",\"password\":\"***\",\"keeplogin\":\"1\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617773676);
INSERT INTO `sp_admin_log` VALUES (32, 1, 'superadmin', '/sports.php/command/execute/ids/9', '在线命令管理 / 运行', '{\"ids\":\"9\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617779229);
INSERT INTO `sp_admin_log` VALUES (33, 1, 'superadmin', '/sports.php/command/execute/ids/10', '在线命令管理 / 运行', '{\"ids\":\"10\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617779615);
INSERT INTO `sp_admin_log` VALUES (34, 1, 'superadmin', '/sports.php/general/config/check', '常规管理 / 系统配置', '{\"row\":{\"name\":\"platform_price\"}}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617782394);
INSERT INTO `sp_admin_log` VALUES (35, 1, 'superadmin', '/sports.php/general.config/add', '常规管理 / 系统配置 / 添加', '{\"__token__\":\"***\",\"row\":{\"group\":\"basic\",\"type\":\"number\",\"name\":\"platform_price\",\"title\":\"加入平台费用\",\"setting\":{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"},\"value\":\"0.01\",\"content\":\"value1|title1\\r\\nvalue2|title2\",\"tip\":\"平台费用\",\"rule\":\"\",\"extend\":\"\"}}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617782424);
INSERT INTO `sp_admin_log` VALUES (36, 1, 'superadmin', '/sports.php/command/execute/ids/11', '在线命令管理 / 运行', '{\"ids\":\"11\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617785786);
INSERT INTO `sp_admin_log` VALUES (37, 1, 'superadmin', '/sports.php/general/config/check', '常规管理 / 系统配置', '{\"row\":{\"name\":\"apply_\"}}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617786288);
INSERT INTO `sp_admin_log` VALUES (38, 1, 'superadmin', '/sports.php/general/config/check', '常规管理 / 系统配置', '{\"row\":{\"name\":\"apply_space\"}}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617786298);
INSERT INTO `sp_admin_log` VALUES (39, 1, 'superadmin', '/sports.php/general.config/add', '常规管理 / 系统配置 / 添加', '{\"__token__\":\"***\",\"row\":{\"group\":\"basic\",\"type\":\"switch\",\"name\":\"apply_space\",\"title\":\"场地申请不需要审核\",\"setting\":{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"},\"value\":\"1\",\"content\":\"value1|title1\\r\\nvalue2|title2\",\"tip\":\"当开关打开，申请场地只需要支付后即可通过，否则都需要审核\",\"rule\":\"\",\"extend\":\"\"}}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617786380);
INSERT INTO `sp_admin_log` VALUES (40, 1, 'superadmin', '/sports.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"name\":\"运动场地预约\",\"beian\":\"\",\"cdnurl\":\"\",\"version\":\"1.0.1\",\"timezone\":\"Asia\\/Shanghai\",\"forbiddenip\":\"\",\"languages\":\"{&quot;backend&quot;:&quot;zh-cn&quot;,&quot;frontend&quot;:&quot;zh-cn&quot;}\",\"fixedpage\":\"dashboard\",\"platform_price\":\"0.01\",\"apply_space\":\"1\"}}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617786388);
INSERT INTO `sp_admin_log` VALUES (41, 1, 'superadmin', '/sports.php/command/execute/ids/12', '在线命令管理 / 运行', '{\"ids\":\"12\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617786867);
INSERT INTO `sp_admin_log` VALUES (42, 1, 'superadmin', '/sports.php/command/execute/ids/13', '在线命令管理 / 运行', '{\"ids\":\"13\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617788338);
INSERT INTO `sp_admin_log` VALUES (43, 1, 'superadmin', '/sports.php/command/execute/ids/14', '在线命令管理 / 运行', '{\"ids\":\"14\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617859601);
INSERT INTO `sp_admin_log` VALUES (44, 1, 'superadmin', '/sports.php/command/execute/ids/15', '在线命令管理 / 运行', '{\"ids\":\"15\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617867243);
INSERT INTO `sp_admin_log` VALUES (45, 1, 'superadmin', '/sports.php/command/execute/ids/16', '在线命令管理 / 运行', '{\"ids\":\"16\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617869482);
INSERT INTO `sp_admin_log` VALUES (46, 1, 'superadmin', '/sports.php/command/execute/ids/17', '在线命令管理 / 运行', '{\"ids\":\"17\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617874907);
INSERT INTO `sp_admin_log` VALUES (47, 1, 'superadmin', '/sports.php/command/execute/ids/24', '在线命令管理 / 运行', '{\"ids\":\"24\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617874981);
INSERT INTO `sp_admin_log` VALUES (48, 1, 'superadmin', '/sports.php/command/execute/ids/25', '在线命令管理 / 运行', '{\"ids\":\"25\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617875020);
INSERT INTO `sp_admin_log` VALUES (49, 1, 'superadmin', '/sports.php/command/execute/ids/26', '在线命令管理 / 运行', '{\"ids\":\"26\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617932538);
INSERT INTO `sp_admin_log` VALUES (50, 1, 'superadmin', '/sports.php/command/execute/ids/27', '在线命令管理 / 运行', '{\"ids\":\"27\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617932610);
INSERT INTO `sp_admin_log` VALUES (51, 1, 'superadmin', '/sports.php/command/execute/ids/28', '在线命令管理 / 运行', '{\"ids\":\"28\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617934681);
INSERT INTO `sp_admin_log` VALUES (52, 1, 'superadmin', '/sports.php/command/execute/ids/29', '在线命令管理 / 运行', '{\"ids\":\"29\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617934911);
INSERT INTO `sp_admin_log` VALUES (53, 1, 'superadmin', '/sports.php/command/execute/ids/30', '在线命令管理 / 运行', '{\"ids\":\"30\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617955978);
INSERT INTO `sp_admin_log` VALUES (54, 1, 'superadmin', '/sports.php/command/execute/ids/31', '在线命令管理 / 运行', '{\"ids\":\"31\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617959418);
INSERT INTO `sp_admin_log` VALUES (55, 1, 'superadmin', '/sports.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"app_id\":\"wx0cab47881bf85892\",\"secret\":\"a8ec09d0ca1d4cc021c8c137ef44dbc7\",\"mch_id\":\"1607387054\",\"key\":\"80c72e48b093e803a89fb6377974eb26\",\"wx_app_id\":\"qq496631085\",\"wx_secret\":\"qq496631085qq496631085qq496631085\"}}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617959474);
INSERT INTO `sp_admin_log` VALUES (56, 1, 'superadmin', '/sports.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"app_id\":\"wx0cab47881bf85892\",\"secret\":\"a8ec09d0ca1d4cc021c8c137ef44dbc7\",\"mch_id\":\"1504804021\",\"key\":\"24dd9088d4d9905b9d97ba61b202b1e5\",\"wx_app_id\":\"qq496631085\",\"wx_secret\":\"qq496631085qq496631085qq496631085\"}}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1617961138);
INSERT INTO `sp_admin_log` VALUES (57, 1, 'superadmin', '/sports.php/addon/local', '插件管理', '{\"uid\":\"15937\",\"token\":\"***\",\"version\":\"1.2.0.20210401_beta\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1618190020);
INSERT INTO `sp_admin_log` VALUES (58, 1, 'superadmin', '/sports.php/addon/local', '插件管理', '{\"uid\":\"15937\",\"token\":\"***\",\"version\":\"1.2.0.20210401_beta\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1618190096);
INSERT INTO `sp_admin_log` VALUES (59, 1, 'superadmin', '/sports.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"redisgeo\",\"action\":\"enable\",\"force\":\"0\"}', '114.224.131.31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 1618190096);
INSERT INTO `sp_admin_log` VALUES (60, 1, 'superadmin', '/sports.php/index/login?url=%2Fsports.php%2Fgeneral%2Fconfig%3Fref%3Daddtabs', '登录', '{\"url\":\"\\/sports.php\\/general\\/config?ref=addtabs\",\"__token__\":\"***\",\"username\":\"superadmin\",\"password\":\"***\",\"keeplogin\":\"1\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618794833);
INSERT INTO `sp_admin_log` VALUES (61, 1, 'superadmin', '/sports.php/command/execute/ids/34', '在线命令管理 / 运行', '{\"ids\":\"34\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618896107);
INSERT INTO `sp_admin_log` VALUES (62, 1, 'superadmin', '/sports.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"app_id\":\"wx38f6f26dc6705c73\",\"secret\":\"80c72e48b093e803a89fb6377974eb26\",\"mch_id\":\"1504804021\",\"key\":\"24dd9088d4d9905b9d97ba61b202b1e5\",\"wx_app_id\":\"qq496631085\",\"wx_secret\":\"qq496631085qq496631085qq496631085\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618896547);
INSERT INTO `sp_admin_log` VALUES (63, 1, 'superadmin', '/sports.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"app_id\":\"wx38f6f26dc6705c73\",\"secret\":\"80c72e48b093e803a89fb6377974eb26\",\"mch_id\":\"1504804021\",\"key\":\"24dd9088d4d9905b9d97ba61b202b1e5\",\"wx_app_id\":\"qq496631085\",\"wx_secret\":\"qq496631085qq496631085qq496631085\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618896548);
INSERT INTO `sp_admin_log` VALUES (64, 1, 'superadmin', '/sports.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"app_id\":\"wx38f6f26dc6705c73\",\"secret\":\"80c72e48b093e803a89fb6377974eb26\",\"mch_id\":\"1607387054\",\"key\":\"80c72e48b093e803a89fb6377974eb26\",\"wx_app_id\":\"qq496631085\",\"wx_secret\":\"qq496631085qq496631085qq496631085\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618896584);
INSERT INTO `sp_admin_log` VALUES (65, 1, 'superadmin', '/sports.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"app_id\":\"wx38f6f26dc6705c73\",\"secret\":\"80c72e48b093e803a89fb6377974eb26\",\"mch_id\":\"1607387054\",\"key\":\"80c72e48b093e803a89fb6377974eb26\",\"wx_app_id\":\"qq496631085\",\"wx_secret\":\"qq496631085qq496631085qq496631085\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618896585);
INSERT INTO `sp_admin_log` VALUES (66, 1, 'superadmin', '/sports.php/command/execute/ids/38', '在线命令管理 / 运行', '{\"ids\":\"38\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618897063);
INSERT INTO `sp_admin_log` VALUES (67, 1, 'superadmin', '/sports.php/command/execute/ids/39', '在线命令管理 / 运行', '{\"ids\":\"39\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618897132);
INSERT INTO `sp_admin_log` VALUES (68, 1, 'superadmin', '/sports.php/index/login?url=%2Fsports.php', '登录', '{\"url\":\"\\/sports.php\",\"__token__\":\"***\",\"username\":\"superadmin\",\"password\":\"***\"}', '114.224.144.226', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900320);
INSERT INTO `sp_admin_log` VALUES (69, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900328);
INSERT INTO `sp_admin_log` VALUES (70, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_banner\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900332);
INSERT INTO `sp_admin_log` VALUES (71, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"0\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"0\",\"table\":\"sp_banner\",\"controller\":\"\",\"model\":\"\",\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900341);
INSERT INTO `sp_admin_log` VALUES (72, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"0\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"0\",\"table\":\"sp_banner\",\"controller\":\"\",\"model\":\"\",\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900342);
INSERT INTO `sp_admin_log` VALUES (73, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900344);
INSERT INTO `sp_admin_log` VALUES (74, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900347);
INSERT INTO `sp_admin_log` VALUES (75, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900349);
INSERT INTO `sp_admin_log` VALUES (76, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Banner.php\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900350);
INSERT INTO `sp_admin_log` VALUES (77, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Banner.php\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900350);
INSERT INTO `sp_admin_log` VALUES (78, 1, 'superadmin', '/sports.php/ajax/upload', '', '[]', '114.224.144.226', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900376);
INSERT INTO `sp_admin_log` VALUES (79, 1, 'superadmin', '/sports.php/banner/edit/ids/2?dialog=1', 'Banner / 编辑', '{\"dialog\":\"1\",\"row\":{\"banner_image\":\"\\/uploads\\/20210420\\/073ff384d3d0c51acbdc9ce16ba0e316.png\",\"title\":\"ccc\",\"url\":\"111\",\"weigh\":\"1\",\"crteatime\":\"1970-01-01 08:00:01\"},\"ids\":\"2\"}', '114.224.144.226', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900377);
INSERT INTO `sp_admin_log` VALUES (80, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_space\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900402);
INSERT INTO `sp_admin_log` VALUES (81, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900489);
INSERT INTO `sp_admin_log` VALUES (82, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900489);
INSERT INTO `sp_admin_log` VALUES (83, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900489);
INSERT INTO `sp_admin_log` VALUES (84, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900489);
INSERT INTO `sp_admin_log` VALUES (85, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_user\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900496);
INSERT INTO `sp_admin_log` VALUES (86, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_user\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900496);
INSERT INTO `sp_admin_log` VALUES (87, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900518);
INSERT INTO `sp_admin_log` VALUES (88, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900518);
INSERT INTO `sp_admin_log` VALUES (89, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900518);
INSERT INTO `sp_admin_log` VALUES (90, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900518);
INSERT INTO `sp_admin_log` VALUES (91, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_cate\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900527);
INSERT INTO `sp_admin_log` VALUES (92, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_cate\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900527);
INSERT INTO `sp_admin_log` VALUES (93, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900537);
INSERT INTO `sp_admin_log` VALUES (94, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900537);
INSERT INTO `sp_admin_log` VALUES (95, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900537);
INSERT INTO `sp_admin_log` VALUES (96, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900537);
INSERT INTO `sp_admin_log` VALUES (97, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"1\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"1\",\"table\":\"sp_space\",\"controller\":\"\",\"model\":\"\",\"fields\":[\"id\",\"name\",\"addr\",\"image\",\"blis_image\",\"play_status\",\"online_status\",\"money\",\"tel\",\"apply_status\",\"s_time\",\"e_time\",\"pay_status\",\"week\",\"baochang_price\",\"price\",\"overtime_price\",\"in_device_id\",\"out_device_id\"],\"relation\":{\"2\":{\"relation\":\"sp_user\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"uid\",\"relationprimarykey\":\"id\",\"relationfields\":[\"nickname\"]},\"3\":{\"relation\":\"sp_cate\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"cate_ids\",\"relationprimarykey\":\"id\",\"relationfields\":[\"cate_name\"]}},\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900556);
INSERT INTO `sp_admin_log` VALUES (98, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"1\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"1\",\"table\":\"sp_space\",\"controller\":\"\",\"model\":\"\",\"fields\":[\"id\",\"name\",\"addr\",\"image\",\"blis_image\",\"play_status\",\"online_status\",\"money\",\"tel\",\"apply_status\",\"s_time\",\"e_time\",\"pay_status\",\"week\",\"baochang_price\",\"price\",\"overtime_price\",\"in_device_id\",\"out_device_id\"],\"relation\":{\"2\":{\"relation\":\"sp_user\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"uid\",\"relationprimarykey\":\"id\",\"relationfields\":[\"nickname\"]},\"3\":{\"relation\":\"sp_cate\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"cate_ids\",\"relationprimarykey\":\"id\",\"relationfields\":[\"cate_name\"]}},\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618900557);
INSERT INTO `sp_admin_log` VALUES (99, 1, 'superadmin', '/sports.php/general/config/check', '常规管理 / 系统配置', '{\"row\":{\"name\":\"disclaimer\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618903246);
INSERT INTO `sp_admin_log` VALUES (100, 1, 'superadmin', '/sports.php/general.config/add', '常规管理 / 系统配置 / 添加', '{\"__token__\":\"***\",\"row\":{\"group\":\"basic\",\"type\":\"editor\",\"name\":\"disclaimer\",\"title\":\"免责声明\",\"setting\":{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"},\"value\":\"\",\"content\":\"value1|title1\\r\\nvalue2|title2\",\"tip\":\"免责声明\",\"rule\":\"\",\"extend\":\"\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618903269);
INSERT INTO `sp_admin_log` VALUES (101, 1, 'superadmin', '/sports.php/general.config/add', '常规管理 / 系统配置 / 添加', '{\"__token__\":\"***\",\"row\":{\"group\":\"basic\",\"type\":\"editor\",\"name\":\"disclaimer\",\"title\":\"免责声明\",\"setting\":{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"},\"value\":\"\",\"content\":\"value1|title1\\r\\nvalue2|title2\",\"tip\":\"免责声明\",\"rule\":\"\",\"extend\":\"\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618903271);
INSERT INTO `sp_admin_log` VALUES (102, 1, 'superadmin', '/sports.php/addon/install', '插件管理', '{\"name\":\"summernote\",\"force\":\"0\",\"uid\":\"15937\",\"token\":\"***\",\"version\":\"1.0.4\",\"faversion\":\"1.2.0.20210401_beta\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618903293);
INSERT INTO `sp_admin_log` VALUES (103, 1, 'superadmin', '/sports.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{\"__token__\":\"***\",\"row\":{\"name\":\"运动场地预约\",\"beian\":\"\",\"cdnurl\":\"\",\"version\":\"1.0.1\",\"timezone\":\"Asia\\/Shanghai\",\"forbiddenip\":\"\",\"languages\":\"{&quot;backend&quot;:&quot;zh-cn&quot;,&quot;frontend&quot;:&quot;zh-cn&quot;}\",\"fixedpage\":\"dashboard\",\"platform_price\":\"0.01\",\"apply_space\":\"1\",\"disclaimer\":\"测试﻿\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618903323);
INSERT INTO `sp_admin_log` VALUES (104, 1, 'superadmin', '/sports.php/command/execute/ids/43', '在线命令管理 / 运行', '{\"ids\":\"43\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618903931);
INSERT INTO `sp_admin_log` VALUES (105, 1, 'superadmin', '/sports.php/command/execute/ids/44', '在线命令管理 / 运行', '{\"ids\":\"44\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618907824);
INSERT INTO `sp_admin_log` VALUES (106, 1, 'superadmin', '/sports.php/command/execute/ids/45', '在线命令管理 / 运行', '{\"ids\":\"45\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910392);
INSERT INTO `sp_admin_log` VALUES (107, 1, 'superadmin', '/sports.php/command/execute/ids/46', '在线命令管理 / 运行', '{\"ids\":\"46\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910498);
INSERT INTO `sp_admin_log` VALUES (108, 1, 'superadmin', '/sports.php/command/execute/ids/47', '在线命令管理 / 运行', '{\"ids\":\"47\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910528);
INSERT INTO `sp_admin_log` VALUES (109, 1, 'superadmin', '/sports.php/command/execute/ids/48', '在线命令管理 / 运行', '{\"ids\":\"48\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910568);
INSERT INTO `sp_admin_log` VALUES (110, 1, 'superadmin', '/sports.php/command/execute/ids/49', '在线命令管理 / 运行', '{\"ids\":\"49\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910639);
INSERT INTO `sp_admin_log` VALUES (111, 1, 'superadmin', '/sports.php/command/execute/ids/50', '在线命令管理 / 运行', '{\"ids\":\"50\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910701);
INSERT INTO `sp_admin_log` VALUES (112, 1, 'superadmin', '/sports.php/command/execute/ids/51', '在线命令管理 / 运行', '{\"ids\":\"51\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910728);
INSERT INTO `sp_admin_log` VALUES (113, 1, 'superadmin', '/sports.php/command/execute/ids/42', '在线命令管理 / 运行', '{\"ids\":\"42\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910776);
INSERT INTO `sp_admin_log` VALUES (114, 1, 'superadmin', '/sports.php/command/execute/ids/53', '在线命令管理 / 运行', '{\"ids\":\"53\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910862);
INSERT INTO `sp_admin_log` VALUES (115, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910925);
INSERT INTO `sp_admin_log` VALUES (116, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"api\",\"force\":\"0\",\"url\":\"\",\"output\":\"\",\"template\":\"\",\"title\":\"\",\"author\":\"\",\"language\":\"\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910927);
INSERT INTO `sp_admin_log` VALUES (117, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"api\",\"force\":\"0\",\"url\":\"\",\"output\":\"\",\"template\":\"\",\"title\":\"\",\"author\":\"\",\"language\":\"\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618910928);
INSERT INTO `sp_admin_log` VALUES (118, 1, 'superadmin', '/sports.php/command/execute/ids/57', '在线命令管理 / 运行', '{\"ids\":\"57\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618970126);
INSERT INTO `sp_admin_log` VALUES (119, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618970136);
INSERT INTO `sp_admin_log` VALUES (120, 1, 'superadmin', '/sports.php/command/execute/ids/57', '在线命令管理 / 运行', '{\"ids\":\"57\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618970139);
INSERT INTO `sp_admin_log` VALUES (121, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618970144);
INSERT INTO `sp_admin_log` VALUES (122, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"api\",\"force\":\"1\",\"url\":\"\",\"output\":\"\",\"template\":\"\",\"title\":\"\",\"author\":\"\",\"language\":\"\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618970150);
INSERT INTO `sp_admin_log` VALUES (123, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"api\",\"force\":\"1\",\"url\":\"\",\"output\":\"\",\"template\":\"\",\"title\":\"\",\"author\":\"\",\"language\":\"\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618970150);
INSERT INTO `sp_admin_log` VALUES (124, 1, 'superadmin', '/sports.php/command/execute/ids/60', '在线命令管理 / 运行', '{\"ids\":\"60\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618975781);
INSERT INTO `sp_admin_log` VALUES (125, 1, 'superadmin', '/sports.php/command/execute/ids/61', '在线命令管理 / 运行', '{\"ids\":\"61\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618975900);
INSERT INTO `sp_admin_log` VALUES (126, 1, 'superadmin', '/sports.php/command/execute/ids/62', '在线命令管理 / 运行', '{\"ids\":\"62\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618975926);
INSERT INTO `sp_admin_log` VALUES (127, 1, 'superadmin', '/sports.php/command/execute/ids/63', '在线命令管理 / 运行', '{\"ids\":\"63\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618976061);
INSERT INTO `sp_admin_log` VALUES (128, 1, 'superadmin', '/sports.php/command/execute/ids/64', '在线命令管理 / 运行', '{\"ids\":\"64\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618976097);
INSERT INTO `sp_admin_log` VALUES (129, 1, 'superadmin', '/sports.php/command/execute/ids/65', '在线命令管理 / 运行', '{\"ids\":\"65\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618976580);
INSERT INTO `sp_admin_log` VALUES (130, 1, 'superadmin', '/sports.php/command/execute/ids/60', '在线命令管理 / 运行', '{\"ids\":\"60\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618977031);
INSERT INTO `sp_admin_log` VALUES (131, 1, 'superadmin', '/sports.php/command/execute/ids/67', '在线命令管理 / 运行', '{\"ids\":\"67\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618984837);
INSERT INTO `sp_admin_log` VALUES (132, 1, 'superadmin', '/sports.php/general/config/check', '常规管理 / 系统配置', '{\"row\":{\"name\":\"agreement\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618986157);
INSERT INTO `sp_admin_log` VALUES (133, 1, 'superadmin', '/sports.php/general.config/add', '常规管理 / 系统配置 / 添加', '{\"__token__\":\"***\",\"row\":{\"group\":\"basic\",\"type\":\"editor\",\"name\":\"agreement\",\"title\":\"包场协议\",\"setting\":{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"},\"value\":\"包场协议包场协议包场协议包场协议包场协议\",\"content\":\"value1|title1\\r\\nvalue2|title2\",\"tip\":\"包场协议\",\"rule\":\"\",\"extend\":\"\"}}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618986175);
INSERT INTO `sp_admin_log` VALUES (134, 1, 'superadmin', '/sports.php/command/execute/ids/66', '在线命令管理 / 运行', '{\"ids\":\"66\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618986769);
INSERT INTO `sp_admin_log` VALUES (135, 1, 'superadmin', '/sports.php/command/execute/ids/69', '在线命令管理 / 运行', '{\"ids\":\"69\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618988599);
INSERT INTO `sp_admin_log` VALUES (136, 1, 'superadmin', '/sports.php/command/execute/ids/69', '在线命令管理 / 运行', '{\"ids\":\"69\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618988630);
INSERT INTO `sp_admin_log` VALUES (137, 1, 'superadmin', '/sports.php/command/execute/ids/70', '在线命令管理 / 运行', '{\"ids\":\"70\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618989761);
INSERT INTO `sp_admin_log` VALUES (138, 1, 'superadmin', '/sports.php/command/execute/ids/71', '在线命令管理 / 运行', '{\"ids\":\"71\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618991395);
INSERT INTO `sp_admin_log` VALUES (139, 1, 'superadmin', '/sports.php/command/execute/ids/73', '在线命令管理 / 运行', '{\"ids\":\"73\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618995898);
INSERT INTO `sp_admin_log` VALUES (140, 1, 'superadmin', '/sports.php/command/execute/ids/74', '在线命令管理 / 运行', '{\"ids\":\"74\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618996748);
INSERT INTO `sp_admin_log` VALUES (141, 1, 'superadmin', '/sports.php/command/execute/ids/75', '在线命令管理 / 运行', '{\"ids\":\"75\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1618996835);
INSERT INTO `sp_admin_log` VALUES (142, 1, 'superadmin', '/sports.php/command/execute/ids/76', '在线命令管理 / 运行', '{\"ids\":\"76\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1619057307);
INSERT INTO `sp_admin_log` VALUES (143, 1, 'superadmin', '/sports.php/command/execute/ids/77', '在线命令管理 / 运行', '{\"ids\":\"77\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1619077924);
INSERT INTO `sp_admin_log` VALUES (144, 1, 'superadmin', '/sports.php/command/execute/ids/80', '在线命令管理 / 运行', '{\"ids\":\"80\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1619080765);
INSERT INTO `sp_admin_log` VALUES (145, 1, 'superadmin', '/sports.php/command/execute/ids/81', '在线命令管理 / 运行', '{\"ids\":\"81\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1619080825);
INSERT INTO `sp_admin_log` VALUES (146, 1, 'superadmin', '/sports.php/command/execute/ids/82', '在线命令管理 / 运行', '{\"ids\":\"82\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1619082571);
INSERT INTO `sp_admin_log` VALUES (147, 1, 'superadmin', '/sports.php/command/execute/ids/83', '在线命令管理 / 运行', '{\"ids\":\"83\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1619082631);
INSERT INTO `sp_admin_log` VALUES (148, 1, 'superadmin', '/sports.php/command/execute/ids/84', '在线命令管理 / 运行', '{\"ids\":\"84\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1619083920);
INSERT INTO `sp_admin_log` VALUES (149, 1, 'superadmin', '/sports.php/command/execute/ids/85', '在线命令管理 / 运行', '{\"ids\":\"85\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1619084174);
INSERT INTO `sp_admin_log` VALUES (150, 1, 'superadmin', '/sports.php/command/execute/ids/86', '在线命令管理 / 运行', '{\"ids\":\"86\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36', 1619084267);
INSERT INTO `sp_admin_log` VALUES (151, 1, 'superadmin', '/sports.php/command/execute/ids/87', '在线命令管理 / 运行', '{\"ids\":\"87\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619144208);
INSERT INTO `sp_admin_log` VALUES (152, 1, 'superadmin', '/sports.php/command/execute/ids/88', '在线命令管理 / 运行', '{\"ids\":\"88\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619147451);
INSERT INTO `sp_admin_log` VALUES (153, 1, 'superadmin', '/sports.php/command/del', '在线命令管理 / 删除', '{\"action\":\"del\",\"ids\":\"88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40\",\"params\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154276);
INSERT INTO `sp_admin_log` VALUES (154, 1, 'superadmin', '/sports.php/command/del', '在线命令管理 / 删除', '{\"action\":\"del\",\"ids\":\"39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1\",\"params\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154282);
INSERT INTO `sp_admin_log` VALUES (155, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154387);
INSERT INTO `sp_admin_log` VALUES (156, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154395);
INSERT INTO `sp_admin_log` VALUES (157, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_yuyue\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154399);
INSERT INTO `sp_admin_log` VALUES (158, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154425);
INSERT INTO `sp_admin_log` VALUES (159, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154425);
INSERT INTO `sp_admin_log` VALUES (160, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154425);
INSERT INTO `sp_admin_log` VALUES (161, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154425);
INSERT INTO `sp_admin_log` VALUES (162, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_order\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154447);
INSERT INTO `sp_admin_log` VALUES (163, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_order\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154448);
INSERT INTO `sp_admin_log` VALUES (164, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"1\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"1\",\"table\":\"sp_yuyue\",\"controller\":\"\",\"model\":\"\",\"fields\":[\"id\",\"admin_id\",\"uid\",\"s_time\",\"e_time\",\"yuyue_status\",\"baochang_status\",\"order\",\"nums\",\"ps\",\"promoter_status\",\"pay_status\",\"random_status\",\"time\"],\"relation\":{\"2\":{\"relation\":\"sp_order\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"order\",\"relationprimarykey\":\"order\",\"relationfields\":[\"price\",\"money\",\"cut_price\",\"pay_money\",\"pay_type\",\"paly_status\",\"baochang_status\",\"pay_status\",\"main_order\",\"main_status\",\"random_status\"]}},\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154518);
INSERT INTO `sp_admin_log` VALUES (165, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"1\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"1\",\"table\":\"sp_yuyue\",\"controller\":\"\",\"model\":\"\",\"fields\":[\"id\",\"admin_id\",\"uid\",\"s_time\",\"e_time\",\"yuyue_status\",\"baochang_status\",\"order\",\"nums\",\"ps\",\"promoter_status\",\"pay_status\",\"random_status\",\"time\"],\"relation\":{\"2\":{\"relation\":\"sp_order\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"order\",\"relationprimarykey\":\"order\",\"relationfields\":[\"price\",\"money\",\"cut_price\",\"pay_money\",\"pay_type\",\"paly_status\",\"baochang_status\",\"pay_status\",\"main_order\",\"main_status\",\"random_status\"]}},\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154519);
INSERT INTO `sp_admin_log` VALUES (166, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154522);
INSERT INTO `sp_admin_log` VALUES (167, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154523);
INSERT INTO `sp_admin_log` VALUES (168, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Yuyue.php\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154525);
INSERT INTO `sp_admin_log` VALUES (169, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Yuyue.php\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154526);
INSERT INTO `sp_admin_log` VALUES (170, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_order\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154535);
INSERT INTO `sp_admin_log` VALUES (171, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154544);
INSERT INTO `sp_admin_log` VALUES (172, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154544);
INSERT INTO `sp_admin_log` VALUES (173, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154544);
INSERT INTO `sp_admin_log` VALUES (174, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154544);
INSERT INTO `sp_admin_log` VALUES (175, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_user\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154547);
INSERT INTO `sp_admin_log` VALUES (176, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_user\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154547);
INSERT INTO `sp_admin_log` VALUES (177, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"1\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"1\",\"table\":\"sp_order\",\"controller\":\"\",\"model\":\"\",\"relation\":{\"3\":{\"relation\":\"sp_user\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"uid\",\"relationprimarykey\":\"id\",\"relationfields\":[\"nickname\",\"headimage\",\"openid\"]}},\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154591);
INSERT INTO `sp_admin_log` VALUES (178, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"1\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"1\",\"table\":\"sp_order\",\"controller\":\"\",\"model\":\"\",\"relation\":{\"3\":{\"relation\":\"sp_user\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"uid\",\"relationprimarykey\":\"id\",\"relationfields\":[\"nickname\",\"headimage\",\"openid\"]}},\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154592);
INSERT INTO `sp_admin_log` VALUES (179, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154595);
INSERT INTO `sp_admin_log` VALUES (180, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154598);
INSERT INTO `sp_admin_log` VALUES (181, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154599);
INSERT INTO `sp_admin_log` VALUES (182, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Order.php\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154600);
INSERT INTO `sp_admin_log` VALUES (183, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Order.php\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154601);
INSERT INTO `sp_admin_log` VALUES (184, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154943);
INSERT INTO `sp_admin_log` VALUES (185, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_bill_space\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154949);
INSERT INTO `sp_admin_log` VALUES (186, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"0\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"0\",\"table\":\"sp_bill_space\",\"controller\":\"\",\"model\":\"\",\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154967);
INSERT INTO `sp_admin_log` VALUES (187, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154973);
INSERT INTO `sp_admin_log` VALUES (188, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154973);
INSERT INTO `sp_admin_log` VALUES (189, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154973);
INSERT INTO `sp_admin_log` VALUES (190, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154973);
INSERT INTO `sp_admin_log` VALUES (191, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_space\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154981);
INSERT INTO `sp_admin_log` VALUES (192, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_space\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619154981);
INSERT INTO `sp_admin_log` VALUES (193, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"1\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"0\",\"table\":\"sp_bill_space\",\"controller\":\"\",\"model\":\"\",\"relation\":{\"2\":{\"relation\":\"sp_space\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"admin_id\",\"relationprimarykey\":\"id\",\"relationfields\":[\"name\"]}},\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155021);
INSERT INTO `sp_admin_log` VALUES (194, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"crud\",\"isrelation\":\"1\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"0\",\"table\":\"sp_bill_space\",\"controller\":\"\",\"model\":\"\",\"relation\":{\"2\":{\"relation\":\"sp_space\",\"relationmode\":\"belongsto\",\"relationforeignkey\":\"admin_id\",\"relationprimarykey\":\"id\",\"relationfields\":[\"name\"]}},\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155021);
INSERT INTO `sp_admin_log` VALUES (195, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155046);
INSERT INTO `sp_admin_log` VALUES (196, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155047);
INSERT INTO `sp_admin_log` VALUES (197, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Space.php\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155052);
INSERT INTO `sp_admin_log` VALUES (198, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Space.php\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155069);
INSERT INTO `sp_admin_log` VALUES (199, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Space.php\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155072);
INSERT INTO `sp_admin_log` VALUES (200, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_admin\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155134);
INSERT INTO `sp_admin_log` VALUES (201, 1, 'superadmin', '/sports.php/command/get_field_list', '在线命令管理', '{\"table\":\"sp_bill_space\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155141);
INSERT INTO `sp_admin_log` VALUES (202, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155148);
INSERT INTO `sp_admin_log` VALUES (203, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155152);
INSERT INTO `sp_admin_log` VALUES (204, 1, 'superadmin', '/sports.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155157);
INSERT INTO `sp_admin_log` VALUES (205, 1, 'superadmin', '/sports.php/command/command/action/command', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"BillSpace.php\",\"action\":\"command\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155158);
INSERT INTO `sp_admin_log` VALUES (206, 1, 'superadmin', '/sports.php/command/command/action/execute', '在线命令管理', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"BillSpace.php\",\"action\":\"execute\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619155159);
INSERT INTO `sp_admin_log` VALUES (207, 1, 'superadmin', '/sports.php/command/execute/ids/96', '在线命令管理 / 运行', '{\"ids\":\"96\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619162487);
INSERT INTO `sp_admin_log` VALUES (208, 1, 'superadmin', '/sports.php/command/execute/ids/89', '在线命令管理 / 运行', '{\"ids\":\"89\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619162491);
INSERT INTO `sp_admin_log` VALUES (209, 1, 'superadmin', '/sports.php/command/execute/ids/98', '在线命令管理 / 运行', '{\"ids\":\"98\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619162623);
INSERT INTO `sp_admin_log` VALUES (210, 1, 'superadmin', '/sports.php/command/execute/ids/99', '在线命令管理 / 运行', '{\"ids\":\"99\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619164053);
INSERT INTO `sp_admin_log` VALUES (211, 1, 'superadmin', '/sports.php/space/edit/ids/5?dialog=1', '场地 / 编辑', '{\"dialog\":\"1\",\"row\":{\"uid\":\"2\",\"name\":\"足球馆\",\"time\":\"2021-04-20 10:44:29\",\"addr\":\"无锡\",\"image\":\"\\/\",\"blis_image\":\"\\/\",\"photo_images\":\"\\/\",\"cate_ids\":\"1\",\"tag_str\":\"1\",\"description\":\"简介\",\"play_status\":\"1\",\"service\":\"服务说明\",\"specs\":\"规格说明\",\"online_status\":\"3\",\"money\":\"0.00\",\"tel\":\"18333333333\",\"wechat\":\"\",\"apply_status\":\"3\",\"lng\":\"12.111\",\"lat\":\"20.000\",\"weigh\":\"\",\"s_time\":\"2021-04-23 09:30:00\",\"e_time\":\"2021-04-23 23:00:00\",\"order\":\"\",\"pay_status\":\"1\",\"week\":\"1234567\",\"baochang_price\":\"0.01\",\"price\":\"1.00\",\"deposit\":\"\",\"overtime_price\":\"\",\"in_device_id\":\"3\",\"out_device_id\":\"4\"},\"ids\":\"5\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619168318);
INSERT INTO `sp_admin_log` VALUES (212, 1, 'superadmin', '/sports.php/space/edit/ids/1?dialog=1', '场地 / 编辑', '{\"dialog\":\"1\",\"row\":{\"uid\":\"1\",\"name\":\"场地测试1\",\"time\":\"1970-01-02 14:51:51\",\"addr\":\"地址111\",\"image\":\"1\",\"blis_image\":\"1\",\"photo_images\":\"1\",\"cate_ids\":\"1\",\"tag_str\":\"1\",\"description\":\"1\",\"play_status\":\"1\",\"service\":\"1\",\"specs\":\"1\",\"online_status\":\"3\",\"money\":\"0.00\",\"tel\":\"199999999\",\"wechat\":\"12\",\"apply_status\":\"3\",\"lng\":\"120.29839\",\"lat\":\"31.68099\",\"weigh\":\"1\",\"s_time\":\"2021-04-23 08:00:00\",\"e_time\":\"2021-04-23 23:00:00\",\"order\":\"\",\"pay_status\":\"1\",\"week\":\"1,2,3,4,5,6,7\",\"baochang_price\":\"10.00\",\"price\":\"1.00\",\"deposit\":\"30.00\",\"overtime_price\":\"5.00\",\"in_device_id\":\"1\",\"out_device_id\":\"2\"},\"ids\":\"1\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619168343);
INSERT INTO `sp_admin_log` VALUES (213, 1, 'superadmin', '/sports.php/space/del', '场地 / 删除', '{\"action\":\"del\",\"ids\":\"1,5\",\"params\":\"\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619168369);
INSERT INTO `sp_admin_log` VALUES (214, 1, 'superadmin', '/sports.php/command/execute/ids/100', '在线命令管理 / 运行', '{\"ids\":\"100\"}', '114.224.144.226', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619231674);
INSERT INTO `sp_admin_log` VALUES (215, 1, 'superadmin', '/sports.php/index/login?url=%2Fsports.php%2Fgeneral%2Fconfig%3Fref%3Daddtabs', '登录', '{\"url\":\"\\/sports.php\\/general\\/config?ref=addtabs\",\"__token__\":\"***\",\"username\":\"superadmin\",\"password\":\"***\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 1619594249);
INSERT INTO `sp_admin_log` VALUES (216, 1, 'superadmin', '/sports.php/index/login?url=%2Fsports.php%2Fgeneral%2Fconfig%3Fref%3Daddtabs', '登录', '{\"url\":\"\\/sports.php\\/general\\/config?ref=addtabs\",\"__token__\":\"***\",\"username\":\"superadmin\",\"password\":\"***\",\"keeplogin\":\"1\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620262106);
INSERT INTO `sp_admin_log` VALUES (217, 1, 'superadmin', '/sports.php/order/edit/ids/28?dialog=1', '用户订单 / 编辑', '{\"dialog\":\"1\",\"row\":{\"order\":\"Bc2021050410414899501005493562\",\"uid\":\"6\",\"time\":\"2021-05-04 10:41:48\",\"price\":\"0.06\",\"money\":\"\",\"cut_price\":\"\",\"pay_money\":\"6\",\"pay_type\":\"1\",\"ps\":\"\",\"paly_status\":\"1\",\"baochang_status\":\"2\",\"e_time\":\"2021-05-04 12:45:00\",\"s_time\":\"2021-05-04 10:45:00\",\"pay_status\":\"2\",\"user_ps\":\"\",\"money_status\":\"1\",\"send_msg\":\"\",\"s_door_time\":\"\",\"pay_time\":\"2021-05-04 10:41:58\",\"e_door_time\":\"\",\"deposit\":\"0.02\",\"overtime_price\":\"\",\"overtime_status\":\"1\",\"refund_deposit\":\"\",\"deposit_status\":\"1\",\"main_order\":\"\",\"main_status\":\"1\",\"random_status\":\"1\",\"refund_order\":\"\",\"refund_ps\":\"\"},\"ids\":\"28\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620263967);
INSERT INTO `sp_admin_log` VALUES (218, 1, 'superadmin', '/sports.php/order/edit/ids/28?dialog=1', '用户订单 / 编辑', '{\"dialog\":\"1\",\"row\":{\"order\":\"Bc2021050410414899501005493562\",\"uid\":\"6\",\"time\":\"2021-05-04 10:41:48\",\"price\":\"0.06\",\"money\":\"0.00\",\"cut_price\":\"0.00\",\"pay_money\":\"6\",\"pay_type\":\"1\",\"ps\":\"\",\"paly_status\":\"1\",\"baochang_status\":\"2\",\"e_time\":\"2021-05-04 12:45:00\",\"s_time\":\"2021-05-04 10:45:00\",\"pay_status\":\"2\",\"user_ps\":\"\",\"money_status\":\"1\",\"send_msg\":\"\",\"s_door_time\":\"\",\"pay_time\":\"2021-05-04 10:41:58\",\"e_door_time\":\"\",\"deposit\":\"0.02\",\"overtime_price\":\"0.00\",\"overtime_status\":\"1\",\"refund_deposit\":\"0.00\",\"deposit_status\":\"1\",\"main_order\":\"\",\"main_status\":\"1\",\"random_status\":\"1\",\"refund_order\":\"\",\"refund_ps\":\"\"},\"ids\":\"28\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620263992);
INSERT INTO `sp_admin_log` VALUES (219, 1, 'superadmin', '/sports.php/order/edit/ids/28?dialog=1', '用户订单 / 编辑', '{\"dialog\":\"1\",\"row\":{\"order\":\"Bc2021050410414899501005493562\",\"uid\":\"6\",\"time\":\"2021-05-04 10:41:48\",\"price\":\"0.06\",\"money\":\"0.00\",\"cut_price\":\"0.00\",\"pay_money\":\"6\",\"pay_type\":\"1\",\"ps\":\"\",\"paly_status\":\"1\",\"baochang_status\":\"2\",\"e_time\":\"2021-05-04 12:45:00\",\"s_time\":\"2021-05-04 10:45:00\",\"pay_status\":\"2\",\"user_ps\":\"\",\"money_status\":\"1\",\"send_msg\":\"\",\"s_door_time\":\"\",\"pay_time\":\"2021-05-04 10:41:58\",\"e_door_time\":\"\",\"deposit\":\"0.02\",\"overtime_price\":\"0.00\",\"overtime_status\":\"1\",\"refund_deposit\":\"0.00\",\"deposit_status\":\"1\",\"main_order\":\"\",\"main_status\":\"1\",\"random_status\":\"1\",\"refund_order\":\"\",\"refund_ps\":\"\"},\"ids\":\"28\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620264003);
INSERT INTO `sp_admin_log` VALUES (220, 1, 'superadmin', '/sports.php/order/edit/ids/28?dialog=1', '用户订单 / 编辑', '{\"dialog\":\"1\",\"row\":{\"order\":\"Bc2021050410414899501005493562\",\"uid\":\"6\",\"time\":\"2021-05-04 10:41:48\",\"price\":\"0.06\",\"money\":\"0.00\",\"cut_price\":\"0.00\",\"pay_money\":\"6\",\"pay_type\":\"1\",\"ps\":\"\",\"paly_status\":\"1\",\"baochang_status\":\"2\",\"e_time\":\"2021-05-04 12:45:00\",\"s_time\":\"2021-05-04 10:45:00\",\"pay_status\":\"2\",\"user_ps\":\"\",\"money_status\":\"1\",\"send_msg\":\"\",\"s_door_time\":\"\",\"pay_time\":\"2021-05-04 10:41:58\",\"e_door_time\":\"\",\"deposit\":\"0.02\",\"overtime_price\":\"0.00\",\"overtime_status\":\"1\",\"refund_deposit\":\"0.00\",\"deposit_status\":\"1\",\"main_order\":\"\",\"main_status\":\"1\",\"random_status\":\"1\",\"refund_order\":\"\",\"refund_ps\":\"\"},\"ids\":\"28\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620264072);
INSERT INTO `sp_admin_log` VALUES (221, 1, 'superadmin', '/sports.php/order/edit/ids/28?dialog=1', '用户订单 / 编辑', '{\"dialog\":\"1\",\"row\":{\"order\":\"Bc2021050410414899501005493562\",\"uid\":\"6\",\"time\":\"2021-05-04 10:41:48\",\"price\":\"0.06\",\"money\":\"0.00\",\"cut_price\":\"0.00\",\"pay_money\":\"6\",\"pay_type\":\"1\",\"ps\":\"\",\"paly_status\":\"5\",\"baochang_status\":\"2\",\"e_time\":\"2021-05-04 12:45:00\",\"s_time\":\"2021-05-04 10:45:00\",\"pay_status\":\"2\",\"user_ps\":\"\",\"money_status\":\"1\",\"send_msg\":\"\",\"s_door_time\":\"\",\"pay_time\":\"2021-05-04 10:41:58\",\"e_door_time\":\"\",\"deposit\":\"0.02\",\"overtime_price\":\"0.00\",\"overtime_status\":\"1\",\"refund_deposit\":\"0.00\",\"deposit_status\":\"1\",\"main_order\":\"\",\"main_status\":\"1\",\"random_status\":\"1\",\"refund_order\":\"Tkb2021050609211357985054382359\",\"refund_ps\":\"基本账户余额不足，请充值后重新发起\"},\"ids\":\"28\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620264209);
INSERT INTO `sp_admin_log` VALUES (222, 1, 'superadmin', '/sports.php/auth/group/roletree', '权限管理 / 角色组', '{\"id\":\"2\",\"pid\":\"1\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620265102);
INSERT INTO `sp_admin_log` VALUES (223, 1, 'superadmin', '/sports.php/auth/group/roletree', '权限管理 / 角色组', '{\"id\":\"3\",\"pid\":\"2\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620265110);
INSERT INTO `sp_admin_log` VALUES (224, 1, 'superadmin', '/sports.php/auth/group/roletree', '权限管理 / 角色组', '{\"id\":\"2\",\"pid\":\"1\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620265123);
INSERT INTO `sp_admin_log` VALUES (225, 1, 'superadmin', '/sports.php/auth/group/edit/ids/2?dialog=1', '权限管理 / 角色组 / 编辑', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"rules\":\"1,2,4,6,7,8,9,10,11,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,63,64,65,99,100,101,102,103,104,105,106,107,108,109,110,111,112,120,121,122,123,124,125,126,5\",\"pid\":\"1\",\"name\":\"二级管理组\",\"status\":\"normal\"},\"ids\":\"2\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620265456);
INSERT INTO `sp_admin_log` VALUES (226, 1, 'superadmin', '/sports.php/auth/group/roletree', '权限管理 / 角色组', '{\"id\":\"3\",\"pid\":\"2\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620265461);
INSERT INTO `sp_admin_log` VALUES (227, 1, 'superadmin', '/sports.php/auth/group/edit/ids/3?dialog=1', '权限管理 / 角色组 / 编辑', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"rules\":\"1,4,5,9,10,11,13,14,15,16,17,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,63,64,65,101,108,122,99,106,120\",\"pid\":\"2\",\"name\":\"三级管理组\",\"status\":\"normal\"},\"ids\":\"3\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620265490);
INSERT INTO `sp_admin_log` VALUES (228, 1, 'superadmin', '/sports.php/bill_space/edit/ids/55?dialog=1', '场馆的流水 / 编辑', '{\"dialog\":\"1\",\"row\":{\"uid\":\"3\",\"status\":\"3\",\"ps\":\"包场子订单未成功退款\",\"time\":\"2021-05-06 10:16:25\",\"order\":\"Son32021050609433555100555228920\",\"money\":\"0.01\"},\"ids\":\"55\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620267574);
INSERT INTO `sp_admin_log` VALUES (229, 1, 'superadmin', '/sports.php/bill_space/del', '场馆的流水 / 删除', '{\"action\":\"del\",\"ids\":\"55\",\"params\":\"\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620267589);
INSERT INTO `sp_admin_log` VALUES (230, 1, 'superadmin', '/sports.php/index/login?url=%2Fsports.php%2Fgeneral%2Fconfig%3Fref%3Daddtabs', '登录', '{\"url\":\"\\/sports.php\\/general\\/config?ref=addtabs\",\"__token__\":\"***\",\"username\":\"superadmin\",\"password\":\"***\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620371893);
INSERT INTO `sp_admin_log` VALUES (231, 1, 'superadmin', '/sports.php/order/edit/ids/43?dialog=1', '用户订单 / 编辑', '{\"dialog\":\"1\",\"row\":{\"order\":\"Bc42021050716464452971005597814\",\"uid\":\"4\",\"time\":\"2021-05-07 16:46:44\",\"price\":\"0.06\",\"money\":\"\",\"cut_price\":\"\",\"pay_money\":\"6\",\"pay_type\":\"1\",\"ps\":\"\",\"paly_status\":\"1\",\"baochang_status\":\"2\",\"e_time\":\"2021-05-07 10:00:00\",\"s_time\":\"2021-05-08 08:00:00\",\"pay_status\":\"2\",\"user_ps\":\"\",\"money_status\":\"1\",\"send_msg\":\"\",\"s_door_time\":\"\",\"pay_time\":\"2021-05-07 16:46:49\",\"e_door_time\":\"\",\"deposit\":\"0.02\",\"overtime_price\":\"\",\"overtime_status\":\"1\",\"refund_deposit\":\"\",\"deposit_status\":\"1\",\"main_order\":\"\",\"main_status\":\"1\",\"random_status\":\"1\",\"refund_order\":\"\",\"refund_ps\":\"\"},\"ids\":\"43\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620377585);
INSERT INTO `sp_admin_log` VALUES (232, 1, 'superadmin', '/sports.php/order/edit/ids/43?dialog=1', '用户订单 / 编辑', '{\"dialog\":\"1\",\"row\":{\"order\":\"Bc42021050716464452971005597814\",\"uid\":\"4\",\"time\":\"2021-05-07 16:46:44\",\"price\":\"0.06\",\"money\":\"0.00\",\"cut_price\":\"0.00\",\"pay_money\":\"6\",\"pay_type\":\"1\",\"ps\":\"\",\"paly_status\":\"1\",\"baochang_status\":\"2\",\"e_time\":\"2021-05-07 10:00:00\",\"s_time\":\"2021-05-07 12:00:00\",\"pay_status\":\"2\",\"user_ps\":\"\",\"money_status\":\"1\",\"send_msg\":\"\",\"s_door_time\":\"\",\"pay_time\":\"2021-05-07 16:46:49\",\"e_door_time\":\"\",\"deposit\":\"0.02\",\"overtime_price\":\"0.00\",\"overtime_status\":\"1\",\"refund_deposit\":\"0.00\",\"deposit_status\":\"1\",\"main_order\":\"\",\"main_status\":\"1\",\"random_status\":\"1\",\"refund_order\":\"\",\"refund_ps\":\"\"},\"ids\":\"43\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620377629);
INSERT INTO `sp_admin_log` VALUES (233, 1, 'superadmin', '/sports.php/space/edit/ids/7?dialog=1', '场地 / 编辑', '{\"dialog\":\"1\",\"row\":{\"uid\":\"3\",\"name\":\"诺远球馆\",\"time\":\"2021-04-20 15:44:47\",\"addr\":\"江苏省无锡市惠山区锦绣路\",\"image\":\"\\/uploads\\/20210420\\/2d1dde1511dad2c2fc52ad1552ab01f8.jpg\",\"blis_image\":\"\\/uploads\\/20210420\\/22b1db482c0eb9d77588d51a5c51d761.jpg\",\"photo_images\":\"\\/uploads\\/20210420\\/a1083a1fe9136dbdc5edb0689dedfb59.jpeg,\\/uploads\\/20210420\\/65be614f5b5a115ecc486eec1b1e0b69.jpg,\\/uploads\\/20210420\\/40a4f02c7d1fcb2cfb49eff0889b2be2.jpeg\",\"cate_ids\":\"3\",\"tag_str\":\"室内,可包场,羽毛球\",\"description\":\"测试一下\",\"play_status\":\"1\",\"service\":\"设施很好\",\"specs\":\"场地规格很棒\",\"online_status\":\"1\",\"money\":\"0.68\",\"tel\":\"18205034747\",\"wechat\":\"\",\"apply_status\":\"1\",\"lng\":\"120.29834\",\"lat\":\"31.68096\",\"weigh\":\"\",\"s_time\":\"2021-05-07 09:00:00\",\"e_time\":\"2021-05-07 20:00:00\",\"order\":\"\",\"pay_status\":\"1\",\"week\":\"1,2,3,4,5,6,7\",\"baochang_price\":\"0.02\",\"price\":\"0.01\",\"deposit\":\"0.02\",\"overtime_price\":\"0.02\",\"in_device_id\":\"7\",\"out_device_id\":\"8\"},\"ids\":\"7\"}', '114.224.147.96', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620378426);
INSERT INTO `sp_admin_log` VALUES (234, 1, 'superadmin', '/sports.php/index/login?url=%2Fsports.php%2Fgeneral%2Fconfig%3Fref%3Daddtabs', '登录', '{\"url\":\"\\/sports.php\\/general\\/config?ref=addtabs\",\"__token__\":\"***\",\"username\":\"superadmin\",\"password\":\"***\"}', '114.224.144.101', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620625409);
INSERT INTO `sp_admin_log` VALUES (235, 1, 'superadmin', '/sports.php/general/config/check', '常规管理 / 系统配置', '{\"row\":{\"name\":\"accessToken\"}}', '114.224.144.101', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620625440);
INSERT INTO `sp_admin_log` VALUES (236, 1, 'superadmin', '/sports.php/general.config/add', '常规管理 / 系统配置 / 添加', '{\"__token__\":\"***\",\"row\":{\"group\":\"basic\",\"type\":\"string\",\"name\":\"accessToken\",\"title\":\"灯的accessToken\",\"setting\":{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"},\"value\":\"\",\"content\":\"value1|title1\\r\\nvalue2|title2\",\"tip\":\"请不要修改此项\",\"rule\":\"\",\"extend\":\"\"}}', '114.224.144.101', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620625480);
INSERT INTO `sp_admin_log` VALUES (237, 1, 'superadmin', '/sports.php/command/execute/ids/104', '在线命令管理 / 运行', '{\"ids\":\"104\"}', '114.224.144.101', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620629172);
INSERT INTO `sp_admin_log` VALUES (238, 1, 'superadmin', '/sports.php/command/execute/ids/105', '在线命令管理 / 运行', '{\"ids\":\"105\"}', '114.224.144.101', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620629253);
INSERT INTO `sp_admin_log` VALUES (239, 1, 'superadmin', '/sports.php/index/login?url=%2Fsports.php%2Fgeneral%2Fconfig%3Fref%3Daddtabs', '登录', '{\"url\":\"\\/sports.php\\/general\\/config?ref=addtabs\",\"__token__\":\"***\",\"username\":\"superadmin\",\"password\":\"***\",\"keeplogin\":\"1\"}', '114.223.143.173', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620722836);
INSERT INTO `sp_admin_log` VALUES (240, 1, 'superadmin', '/sports.php/general/config/check', '常规管理 / 系统配置', '{\"row\":{\"name\":\"free_time\"}}', '114.223.143.173', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620722997);
INSERT INTO `sp_admin_log` VALUES (241, 1, 'superadmin', '/sports.php/general.config/add', '常规管理 / 系统配置 / 添加', '{\"__token__\":\"***\",\"row\":{\"group\":\"basic\",\"type\":\"number\",\"name\":\"free_time\",\"title\":\"免费时间\\/分钟\",\"setting\":{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"},\"value\":\"10\",\"content\":\"value1|title1\\r\\nvalue2|title2\",\"tip\":\"进场后多少分钟内出场免费\",\"rule\":\"\",\"extend\":\"\"}}', '114.223.143.173', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 1620723064);
INSERT INTO `sp_admin_log` VALUES (242, 1, 'superadmin', '/sports.php/command/execute/ids/106', '在线命令管理 / 运行', '{\"ids\":\"106\"}', '114.223.143.173', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 1621040985);
INSERT INTO `sp_admin_log` VALUES (243, 1, 'superadmin', '/sports.php/command/execute/ids/108', '在线命令管理 / 运行', '{\"ids\":\"108\"}', '114.223.143.173', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 1621041070);
COMMIT;

-- ----------------------------
-- Table structure for sp_area
-- ----------------------------
DROP TABLE IF EXISTS `sp_area`;
CREATE TABLE `sp_area` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(10) DEFAULT NULL COMMENT '父id',
  `shortname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '简称',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `mergename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '全称',
  `level` tinyint(4) DEFAULT NULL COMMENT '层级 0 1 2 省市区县',
  `pinyin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '拼音',
  `code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '长途区号',
  `zip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '邮编',
  `first` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '首字母',
  `lng` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='地区表';

-- ----------------------------
-- Records of sp_area
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_attachment
-- ----------------------------
DROP TABLE IF EXISTS `sp_attachment`;
CREATE TABLE `sp_attachment` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '高度',
  `imagetype` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `filename` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '文件名称',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `mimetype` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '透传数据',
  `createtime` int(10) DEFAULT NULL COMMENT '创建日期',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `uploadtime` int(10) DEFAULT NULL COMMENT '上传时间',
  `storage` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `sha1` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '文件 sha1编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='附件表';

-- ----------------------------
-- Records of sp_attachment
-- ----------------------------
BEGIN;
INSERT INTO `sp_attachment` VALUES (1, 1, 0, '/assets/img/qrcode.png', '150', '150', 'png', 0, 'qrcode.png', 21859, 'image/png', '', 1491635035, 1491635035, 1491635035, 'local', '17163603d0263e4838b9387ff2cd4877e8b018f6');
INSERT INTO `sp_attachment` VALUES (2, 1, 0, '/uploads/20210420/073ff384d3d0c51acbdc9ce16ba0e316.png', '828', '323', 'png', 0, 'banner.png', 188618, 'image/png', '', 1618900376, 1618900376, 1618900376, 'local', '9c60938daf3ff98b2e139febb99da830b7e4b198');
INSERT INTO `sp_attachment` VALUES (3, 0, 0, '/uploads/20210420/22b1db482c0eb9d77588d51a5c51d761.jpg', '433', '433', 'jpg', 0, 'BXayecK5e73P22b1db482c0eb9d77588d51a5c51d761.jpg', 59677, 'image/jpeg', '', 1618902768, 1618902768, 1618902768, 'local', 'af7243e86466e15e0693d07e7f121e9268f26be4');
INSERT INTO `sp_attachment` VALUES (4, 0, 0, '/uploads/20210420/2d1dde1511dad2c2fc52ad1552ab01f8.jpg', '516', '356', 'jpg', 0, 'dgpYrMubufCE2d1dde1511dad2c2fc52ad1552ab01f8.jpg', 63957, 'image/jpeg', '', 1618911857, 1618911857, 1618911857, 'local', '3ee2f8d5f15808be4f25a1aa814cfc313567d2dc');
INSERT INTO `sp_attachment` VALUES (5, 0, 0, '/uploads/20210420/a1083a1fe9136dbdc5edb0689dedfb59.jpeg', '700', '700', 'jpeg', 0, 'wyVGtdZGjnFPa1083a1fe9136dbdc5edb0689dedfb59.jpeg', 60040, 'image/jpeg', '', 1618911870, 1618911870, 1618911870, 'local', '62e02f0be5496b45b57932ebeb4dd7c3a48ac476');
INSERT INTO `sp_attachment` VALUES (6, 0, 0, '/uploads/20210420/65be614f5b5a115ecc486eec1b1e0b69.jpg', '658', '658', 'jpg', 0, 'zKjmggd25ec565be614f5b5a115ecc486eec1b1e0b69.jpg', 67504, 'image/jpeg', '', 1618911875, 1618911875, 1618911875, 'local', '281b57d029f660492d95a09105d3f067c5f17c89');
INSERT INTO `sp_attachment` VALUES (7, 0, 0, '/uploads/20210420/40a4f02c7d1fcb2cfb49eff0889b2be2.jpeg', '700', '700', 'jpeg', 0, 'xLUb5qkwd1wM40a4f02c7d1fcb2cfb49eff0889b2be2.jpeg', 107357, 'image/jpeg', '', 1618911881, 1618911881, 1618911881, 'local', '1bc088f28288931f27d52aa2b3e14fe167858100');
INSERT INTO `sp_attachment` VALUES (8, 0, 0, '/uploads/20210507/3abe14d6cbb33e2557fbaca552766b7b.jpg', '640', '1136', 'jpg', 0, 'tmp_vSfsDJ3ahx7G5SABeLQthf8bPixhe1RX.jpg', 132050, 'text/plain', '', 1620372485, 1620372485, 1620372485, 'local', '42e37faf378602b7211440fe4e610e9d41d34ad7');
COMMIT;

-- ----------------------------
-- Table structure for sp_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `sp_auth_group`;
CREATE TABLE `sp_auth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父组别',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '组名',
  `rules` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '规则ID',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='分组表';

-- ----------------------------
-- Records of sp_auth_group
-- ----------------------------
BEGIN;
INSERT INTO `sp_auth_group` VALUES (1, 0, 'Admin group', '*', 1491635035, 1491635035, 'normal');
INSERT INTO `sp_auth_group` VALUES (2, 1, '二级管理组', '1,2,4,6,7,8,9,10,11,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,63,64,65,99,100,101,102,103,104,105,106,107,108,109,110,111,112,120,121,122,123,124,125,126,5', 1491635035, 1620265456, 'normal');
INSERT INTO `sp_auth_group` VALUES (3, 2, '三级管理组', '1,4,9,10,11,13,14,15,16,17,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,63,64,65,99,101,106,108,120,122,5', 1491635035, 1620265490, 'normal');
INSERT INTO `sp_auth_group` VALUES (4, 1, 'Second group 2', '1,4,13,14,15,16,17,55,56,57,58,59,60,61,62,63,64,65', 1491635035, 1491635035, 'normal');
INSERT INTO `sp_auth_group` VALUES (5, 2, 'Third group 2', '1,2,6,7,8,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34', 1491635035, 1620265456, 'normal');
COMMIT;

-- ----------------------------
-- Table structure for sp_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `sp_auth_group_access`;
CREATE TABLE `sp_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '会员ID',
  `group_id` int(10) unsigned NOT NULL COMMENT '级别ID',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='权限分组表';

-- ----------------------------
-- Records of sp_auth_group_access
-- ----------------------------
BEGIN;
INSERT INTO `sp_auth_group_access` VALUES (1, 1);
INSERT INTO `sp_auth_group_access` VALUES (5, 3);
INSERT INTO `sp_auth_group_access` VALUES (7, 3);
COMMIT;

-- ----------------------------
-- Table structure for sp_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `sp_auth_rule`;
CREATE TABLE `sp_auth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('menu','file') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图标',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '规则URL',
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '条件',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为菜单',
  `menutype` enum('addtabs','blank','dialog','ajax') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '菜单类型',
  `extend` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '扩展属性',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `pid` (`pid`),
  KEY `weigh` (`weigh`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='节点表';

-- ----------------------------
-- Records of sp_auth_rule
-- ----------------------------
BEGIN;
INSERT INTO `sp_auth_rule` VALUES (1, 'file', 0, 'dashboard', 'Dashboard', 'fa fa-dashboard', '', '', 'Dashboard tips', 1, NULL, '', 1491635035, 1491635035, 143, 'normal');
INSERT INTO `sp_auth_rule` VALUES (2, 'file', 0, 'general', 'General', 'fa fa-cogs', '', '', '', 1, NULL, '', 1491635035, 1491635035, 137, 'normal');
INSERT INTO `sp_auth_rule` VALUES (3, 'file', 0, 'category', 'Category', 'fa fa-leaf', '', '', 'Category tips', 1, NULL, '', 1491635035, 1491635035, 119, 'normal');
INSERT INTO `sp_auth_rule` VALUES (4, 'file', 0, 'addon', 'Addon', 'fa fa-rocket', '', '', 'Addon tips', 1, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (5, 'file', 0, 'auth', 'Auth', 'fa fa-group', '', '', '', 1, NULL, '', 1491635035, 1491635035, 99, 'normal');
INSERT INTO `sp_auth_rule` VALUES (6, 'file', 2, 'general/config', 'Config', 'fa fa-cog', '', '', 'Config tips', 1, NULL, '', 1491635035, 1491635035, 60, 'normal');
INSERT INTO `sp_auth_rule` VALUES (7, 'file', 2, 'general/attachment', 'Attachment', 'fa fa-file-image-o', '', '', 'Attachment tips', 1, NULL, '', 1491635035, 1491635035, 53, 'normal');
INSERT INTO `sp_auth_rule` VALUES (8, 'file', 2, 'general/profile', 'Profile', 'fa fa-user', '', '', '', 1, NULL, '', 1491635035, 1491635035, 34, 'normal');
INSERT INTO `sp_auth_rule` VALUES (9, 'file', 5, 'auth/admin', 'Admin', 'fa fa-user', '', '', 'Admin tips', 1, NULL, '', 1491635035, 1491635035, 118, 'normal');
INSERT INTO `sp_auth_rule` VALUES (10, 'file', 5, 'auth/adminlog', 'Admin log', 'fa fa-list-alt', '', '', 'Admin log tips', 1, NULL, '', 1491635035, 1491635035, 113, 'normal');
INSERT INTO `sp_auth_rule` VALUES (11, 'file', 5, 'auth/group', 'Group', 'fa fa-group', '', '', 'Group tips', 1, NULL, '', 1491635035, 1491635035, 109, 'normal');
INSERT INTO `sp_auth_rule` VALUES (12, 'file', 5, 'auth/rule', 'Rule', 'fa fa-bars', '', '', 'Rule tips', 1, NULL, '', 1491635035, 1491635035, 104, 'normal');
INSERT INTO `sp_auth_rule` VALUES (13, 'file', 1, 'dashboard/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 136, 'normal');
INSERT INTO `sp_auth_rule` VALUES (14, 'file', 1, 'dashboard/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 135, 'normal');
INSERT INTO `sp_auth_rule` VALUES (15, 'file', 1, 'dashboard/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 133, 'normal');
INSERT INTO `sp_auth_rule` VALUES (16, 'file', 1, 'dashboard/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 134, 'normal');
INSERT INTO `sp_auth_rule` VALUES (17, 'file', 1, 'dashboard/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 132, 'normal');
INSERT INTO `sp_auth_rule` VALUES (18, 'file', 6, 'general/config/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 52, 'normal');
INSERT INTO `sp_auth_rule` VALUES (19, 'file', 6, 'general/config/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 51, 'normal');
INSERT INTO `sp_auth_rule` VALUES (20, 'file', 6, 'general/config/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 50, 'normal');
INSERT INTO `sp_auth_rule` VALUES (21, 'file', 6, 'general/config/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 49, 'normal');
INSERT INTO `sp_auth_rule` VALUES (22, 'file', 6, 'general/config/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 48, 'normal');
INSERT INTO `sp_auth_rule` VALUES (23, 'file', 7, 'general/attachment/index', 'View', 'fa fa-circle-o', '', '', 'Attachment tips', 0, NULL, '', 1491635035, 1491635035, 59, 'normal');
INSERT INTO `sp_auth_rule` VALUES (24, 'file', 7, 'general/attachment/select', 'Select attachment', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 58, 'normal');
INSERT INTO `sp_auth_rule` VALUES (25, 'file', 7, 'general/attachment/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 57, 'normal');
INSERT INTO `sp_auth_rule` VALUES (26, 'file', 7, 'general/attachment/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 56, 'normal');
INSERT INTO `sp_auth_rule` VALUES (27, 'file', 7, 'general/attachment/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 55, 'normal');
INSERT INTO `sp_auth_rule` VALUES (28, 'file', 7, 'general/attachment/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 54, 'normal');
INSERT INTO `sp_auth_rule` VALUES (29, 'file', 8, 'general/profile/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 33, 'normal');
INSERT INTO `sp_auth_rule` VALUES (30, 'file', 8, 'general/profile/update', 'Update profile', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 32, 'normal');
INSERT INTO `sp_auth_rule` VALUES (31, 'file', 8, 'general/profile/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 31, 'normal');
INSERT INTO `sp_auth_rule` VALUES (32, 'file', 8, 'general/profile/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 30, 'normal');
INSERT INTO `sp_auth_rule` VALUES (33, 'file', 8, 'general/profile/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 29, 'normal');
INSERT INTO `sp_auth_rule` VALUES (34, 'file', 8, 'general/profile/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 28, 'normal');
INSERT INTO `sp_auth_rule` VALUES (35, 'file', 3, 'category/index', 'View', 'fa fa-circle-o', '', '', 'Category tips', 0, NULL, '', 1491635035, 1491635035, 142, 'normal');
INSERT INTO `sp_auth_rule` VALUES (36, 'file', 3, 'category/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 141, 'normal');
INSERT INTO `sp_auth_rule` VALUES (37, 'file', 3, 'category/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 140, 'normal');
INSERT INTO `sp_auth_rule` VALUES (38, 'file', 3, 'category/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 139, 'normal');
INSERT INTO `sp_auth_rule` VALUES (39, 'file', 3, 'category/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 138, 'normal');
INSERT INTO `sp_auth_rule` VALUES (40, 'file', 9, 'auth/admin/index', 'View', 'fa fa-circle-o', '', '', 'Admin tips', 0, NULL, '', 1491635035, 1491635035, 117, 'normal');
INSERT INTO `sp_auth_rule` VALUES (41, 'file', 9, 'auth/admin/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 116, 'normal');
INSERT INTO `sp_auth_rule` VALUES (42, 'file', 9, 'auth/admin/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 115, 'normal');
INSERT INTO `sp_auth_rule` VALUES (43, 'file', 9, 'auth/admin/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 114, 'normal');
INSERT INTO `sp_auth_rule` VALUES (44, 'file', 10, 'auth/adminlog/index', 'View', 'fa fa-circle-o', '', '', 'Admin log tips', 0, NULL, '', 1491635035, 1491635035, 112, 'normal');
INSERT INTO `sp_auth_rule` VALUES (45, 'file', 10, 'auth/adminlog/detail', 'Detail', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 111, 'normal');
INSERT INTO `sp_auth_rule` VALUES (46, 'file', 10, 'auth/adminlog/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 110, 'normal');
INSERT INTO `sp_auth_rule` VALUES (47, 'file', 11, 'auth/group/index', 'View', 'fa fa-circle-o', '', '', 'Group tips', 0, NULL, '', 1491635035, 1491635035, 108, 'normal');
INSERT INTO `sp_auth_rule` VALUES (48, 'file', 11, 'auth/group/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 107, 'normal');
INSERT INTO `sp_auth_rule` VALUES (49, 'file', 11, 'auth/group/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 106, 'normal');
INSERT INTO `sp_auth_rule` VALUES (50, 'file', 11, 'auth/group/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 105, 'normal');
INSERT INTO `sp_auth_rule` VALUES (51, 'file', 12, 'auth/rule/index', 'View', 'fa fa-circle-o', '', '', 'Rule tips', 0, NULL, '', 1491635035, 1491635035, 103, 'normal');
INSERT INTO `sp_auth_rule` VALUES (52, 'file', 12, 'auth/rule/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 102, 'normal');
INSERT INTO `sp_auth_rule` VALUES (53, 'file', 12, 'auth/rule/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 101, 'normal');
INSERT INTO `sp_auth_rule` VALUES (54, 'file', 12, 'auth/rule/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 100, 'normal');
INSERT INTO `sp_auth_rule` VALUES (55, 'file', 4, 'addon/index', 'View', 'fa fa-circle-o', '', '', 'Addon tips', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (56, 'file', 4, 'addon/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (57, 'file', 4, 'addon/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (58, 'file', 4, 'addon/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (59, 'file', 4, 'addon/downloaded', 'Local addon', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (60, 'file', 4, 'addon/state', 'Update state', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (63, 'file', 4, 'addon/config', 'Setting', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (64, 'file', 4, 'addon/refresh', 'Refresh', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (65, 'file', 4, 'addon/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (66, 'file', 0, 'user', 'User', 'fa fa-list', '', '', '', 1, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (67, 'file', 66, 'user/user', 'User', 'fa fa-user', '', '', '', 1, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (68, 'file', 67, 'user/user/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (69, 'file', 67, 'user/user/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (70, 'file', 67, 'user/user/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (71, 'file', 67, 'user/user/del', 'Del', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (72, 'file', 67, 'user/user/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (73, 'file', 66, 'user/group', 'User group', 'fa fa-users', '', '', '', 1, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (74, 'file', 73, 'user/group/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (75, 'file', 73, 'user/group/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (76, 'file', 73, 'user/group/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (77, 'file', 73, 'user/group/del', 'Del', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (78, 'file', 73, 'user/group/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (79, 'file', 66, 'user/rule', 'User rule', 'fa fa-circle-o', '', '', '', 1, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (80, 'file', 79, 'user/rule/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (81, 'file', 79, 'user/rule/del', 'Del', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (82, 'file', 79, 'user/rule/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (83, 'file', 79, 'user/rule/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (84, 'file', 79, 'user/rule/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (85, 'file', 0, 'command', '在线命令管理', 'fa fa-terminal', '', '', '', 1, NULL, '', 1617697086, 1617697086, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (86, 'file', 85, 'command/index', '查看', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1617697086, 1617697086, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (87, 'file', 85, 'command/add', '添加', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1617697086, 1617697086, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (88, 'file', 85, 'command/detail', '详情', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1617697086, 1617697086, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (89, 'file', 85, 'command/execute', '运行', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1617697086, 1617697086, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (90, 'file', 85, 'command/del', '删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1617697086, 1617697086, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (91, 'file', 85, 'command/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1617697086, 1617697086, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (92, 'file', 0, 'banner', 'Banner', 'fa fa-circle-o', '', '', '', 1, NULL, '', 1618900350, 1618900350, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (93, 'file', 92, 'banner/import', 'Import', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1618900350, 1618910862, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (94, 'file', 92, 'banner/index', '查看', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1618900350, 1618910862, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (95, 'file', 92, 'banner/add', '添加', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1618900350, 1618910862, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (96, 'file', 92, 'banner/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1618900350, 1618910862, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (97, 'file', 92, 'banner/del', '删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1618900350, 1618910862, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (98, 'file', 92, 'banner/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1618900350, 1618910862, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (99, 'file', 0, 'yuyue', '预约场馆', 'fa fa-circle-o', '', '', '', 1, NULL, '', 1619154526, 1619154526, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (100, 'file', 99, 'yuyue/import', 'Import', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154526, 1619154526, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (101, 'file', 99, 'yuyue/index', '查看', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154526, 1619154526, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (102, 'file', 99, 'yuyue/add', '添加', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154526, 1619154526, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (103, 'file', 99, 'yuyue/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154526, 1619154526, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (104, 'file', 99, 'yuyue/del', '删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154526, 1619154526, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (105, 'file', 99, 'yuyue/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154526, 1619154526, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (106, 'file', 0, 'order', '用户订单', 'fa fa-circle-o', '', '', '', 1, NULL, '', 1619154601, 1619154601, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (107, 'file', 106, 'order/import', 'Import', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154601, 1619154601, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (108, 'file', 106, 'order/index', '查看', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154601, 1619154601, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (109, 'file', 106, 'order/add', '添加', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154601, 1619154601, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (110, 'file', 106, 'order/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154601, 1619154601, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (111, 'file', 106, 'order/del', '删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154601, 1619154601, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (112, 'file', 106, 'order/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619154601, 1619154601, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (113, 'file', 0, 'space', '场地', 'fa fa-circle-o', '', '', '', 1, NULL, '', 1619155072, 1619155072, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (114, 'file', 113, 'space/import', 'Import', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155072, 1619155072, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (115, 'file', 113, 'space/index', '查看', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155072, 1619155072, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (116, 'file', 113, 'space/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155072, 1619155072, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (117, 'file', 113, 'space/add', '添加', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155072, 1619155072, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (118, 'file', 113, 'space/del', '删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155072, 1619155072, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (119, 'file', 113, 'space/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155072, 1619155072, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (120, 'file', 0, 'bill_space', '场馆的流水', 'fa fa-circle-o', '', '', '', 1, NULL, '', 1619155159, 1619155159, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (121, 'file', 120, 'bill_space/import', 'Import', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155159, 1619162487, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (122, 'file', 120, 'bill_space/index', '查看', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155159, 1619162487, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (123, 'file', 120, 'bill_space/add', '添加', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155159, 1619162487, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (124, 'file', 120, 'bill_space/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155159, 1619162487, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (125, 'file', 120, 'bill_space/del', '删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155159, 1619162487, 0, 'normal');
INSERT INTO `sp_auth_rule` VALUES (126, 'file', 120, 'bill_space/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, NULL, '', 1619155159, 1619162487, 0, 'normal');
COMMIT;

-- ----------------------------
-- Table structure for sp_banner
-- ----------------------------
DROP TABLE IF EXISTS `sp_banner`;
CREATE TABLE `sp_banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `banner_image` varchar(255) DEFAULT NULL COMMENT 'banner图',
  `title` varchar(20) DEFAULT NULL COMMENT '标题',
  `url` varchar(255) DEFAULT NULL COMMENT '跳转链接',
  `weigh` int(10) DEFAULT NULL COMMENT '权重',
  `crteatime` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Banner';

-- ----------------------------
-- Records of sp_banner
-- ----------------------------
BEGIN;
INSERT INTO `sp_banner` VALUES (2, '/uploads/20210420/073ff384d3d0c51acbdc9ce16ba0e316.png', 'ccc', '111', 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for sp_bill_space
-- ----------------------------
DROP TABLE IF EXISTS `sp_bill_space`;
CREATE TABLE `sp_bill_space` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT NULL COMMENT '用户id',
  `status` enum('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL COMMENT '状态:1=商家提现,2=用户退款,3=子订单退款,4=到场订单,5=预约订单,6=子订单,7=交平台费用',
  `ps` varchar(255) DEFAULT NULL COMMENT '备注',
  `time` int(10) DEFAULT NULL COMMENT '时间',
  `order` varchar(50) DEFAULT NULL COMMENT '订单号',
  `money` decimal(10,2) DEFAULT NULL COMMENT '钱',
  `admin_id` int(10) DEFAULT NULL COMMENT '场馆id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COMMENT='场馆的流水';

-- ----------------------------
-- Records of sp_bill_space
-- ----------------------------
BEGIN;
INSERT INTO `sp_bill_space` VALUES (69, 7, '5', '包场订单', 1620381682, 'Bc72021050718011497564899941598', 0.06, NULL);
INSERT INTO `sp_bill_space` VALUES (70, 6, '6', '包场子订单', 1620381718, 'Son62021050718011497564899941598', 0.02, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sp_bill_tixian
-- ----------------------------
DROP TABLE IF EXISTS `sp_bill_tixian`;
CREATE TABLE `sp_bill_tixian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `space_id` int(10) unsigned DEFAULT NULL COMMENT '场馆id',
  `pay_status` enum('1','2') DEFAULT '1' COMMENT '到账状态:1=待到账,2=已提现',
  `ps` varchar(255) DEFAULT NULL COMMENT '备注',
  `time` int(10) unsigned DEFAULT NULL COMMENT '时间',
  `order` varchar(32) DEFAULT NULL COMMENT '订单号',
  `uid` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `money` decimal(10,2) unsigned DEFAULT NULL COMMENT '钱',
  `status` enum('1','2','3') DEFAULT '1' COMMENT '状态:1=待审核,2=已通过,3=已拒绝',
  `pay_time` int(10) unsigned DEFAULT NULL COMMENT '到账时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='场馆提现记录';

-- ----------------------------
-- Records of sp_bill_tixian
-- ----------------------------
BEGIN;
INSERT INTO `sp_bill_tixian` VALUES (13, 7, '1', NULL, 1619166799, 'Tx2021042316331910252525938842', 3, 0.10, '2', NULL);
INSERT INTO `sp_bill_tixian` VALUES (14, 7, '1', NULL, 1619166862, 'Tx2021042316342210199489983369', 3, 0.10, '2', NULL);
INSERT INTO `sp_bill_tixian` VALUES (15, 7, '1', NULL, 1619166882, 'Tx2021042316344250102505953589', 3, 0.10, '2', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sp_bill_user
-- ----------------------------
DROP TABLE IF EXISTS `sp_bill_user`;
CREATE TABLE `sp_bill_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT NULL COMMENT '用户id',
  `status` enum('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL COMMENT '状态:1=充值,2=退款,3=返佣,4=到场,5=预约,6=子订单',
  `ps` varchar(255) DEFAULT NULL COMMENT '备注',
  `time` int(10) DEFAULT NULL COMMENT '时间',
  `order` varchar(40) DEFAULT NULL COMMENT '订单号',
  `money` int(10) DEFAULT NULL COMMENT '钱',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='用户的流水';

-- ----------------------------
-- Records of sp_bill_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_cate
-- ----------------------------
DROP TABLE IF EXISTS `sp_cate`;
CREATE TABLE `sp_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(20) DEFAULT NULL COMMENT '分类名',
  `weigh` int(10) DEFAULT NULL,
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='场馆分类';

-- ----------------------------
-- Records of sp_cate
-- ----------------------------
BEGIN;
INSERT INTO `sp_cate` VALUES (1, '篮球', 1, NULL);
INSERT INTO `sp_cate` VALUES (2, '足球', 2, NULL);
INSERT INTO `sp_cate` VALUES (3, '羽毛球', 3, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sp_category
-- ----------------------------
DROP TABLE IF EXISTS `sp_category`;
CREATE TABLE `sp_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '栏目类型',
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `flag` set('hot','index','recommend') COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片',
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '关键字',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '描述',
  `diyname` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '自定义名称',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `weigh` (`weigh`,`id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='分类表';

-- ----------------------------
-- Records of sp_category
-- ----------------------------
BEGIN;
INSERT INTO `sp_category` VALUES (1, 0, 'page', '官方新闻', 'news', 'recommend', '/assets/img/qrcode.png', '', '', 'news', 1491635035, 1491635035, 1, 'normal');
INSERT INTO `sp_category` VALUES (2, 0, 'page', '移动应用', 'mobileapp', 'hot', '/assets/img/qrcode.png', '', '', 'mobileapp', 1491635035, 1491635035, 2, 'normal');
INSERT INTO `sp_category` VALUES (3, 2, 'page', '微信公众号', 'wechatpublic', 'index', '/assets/img/qrcode.png', '', '', 'wechatpublic', 1491635035, 1491635035, 3, 'normal');
INSERT INTO `sp_category` VALUES (4, 2, 'page', 'Android开发', 'android', 'recommend', '/assets/img/qrcode.png', '', '', 'android', 1491635035, 1491635035, 4, 'normal');
INSERT INTO `sp_category` VALUES (5, 0, 'page', '软件产品', 'software', 'recommend', '/assets/img/qrcode.png', '', '', 'software', 1491635035, 1491635035, 5, 'normal');
INSERT INTO `sp_category` VALUES (6, 5, 'page', '网站建站', 'website', 'recommend', '/assets/img/qrcode.png', '', '', 'website', 1491635035, 1491635035, 6, 'normal');
INSERT INTO `sp_category` VALUES (7, 5, 'page', '企业管理软件', 'company', 'index', '/assets/img/qrcode.png', '', '', 'company', 1491635035, 1491635035, 7, 'normal');
INSERT INTO `sp_category` VALUES (8, 6, 'page', 'PC端', 'website-pc', 'recommend', '/assets/img/qrcode.png', '', '', 'website-pc', 1491635035, 1491635035, 8, 'normal');
INSERT INTO `sp_category` VALUES (9, 6, 'page', '移动端', 'website-mobile', 'recommend', '/assets/img/qrcode.png', '', '', 'website-mobile', 1491635035, 1491635035, 9, 'normal');
INSERT INTO `sp_category` VALUES (10, 7, 'page', 'CRM系统 ', 'company-crm', 'recommend', '/assets/img/qrcode.png', '', '', 'company-crm', 1491635035, 1491635035, 10, 'normal');
INSERT INTO `sp_category` VALUES (11, 7, 'page', 'SASS平台软件', 'company-sass', 'recommend', '/assets/img/qrcode.png', '', '', 'company-sass', 1491635035, 1491635035, 11, 'normal');
INSERT INTO `sp_category` VALUES (12, 0, 'test', '测试1', 'test1', 'recommend', '/assets/img/qrcode.png', '', '', 'test1', 1491635035, 1491635035, 12, 'normal');
INSERT INTO `sp_category` VALUES (13, 0, 'test', '测试2', 'test2', 'recommend', '/assets/img/qrcode.png', '', '', 'test2', 1491635035, 1491635035, 13, 'normal');
COMMIT;

-- ----------------------------
-- Table structure for sp_command
-- ----------------------------
DROP TABLE IF EXISTS `sp_command`;
CREATE TABLE `sp_command` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '类型',
  `params` varchar(1500) NOT NULL DEFAULT '' COMMENT '参数',
  `command` varchar(1500) NOT NULL DEFAULT '' COMMENT '命令',
  `content` text COMMENT '返回结果',
  `executetime` int(10) unsigned DEFAULT NULL COMMENT '执行时间',
  `createtime` int(10) unsigned DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) unsigned DEFAULT NULL COMMENT '更新时间',
  `status` enum('successed','failured') NOT NULL DEFAULT 'failured' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COMMENT='在线命令表';

-- ----------------------------
-- Records of sp_command
-- ----------------------------
BEGIN;
INSERT INTO `sp_command` VALUES (89, 'api', '[\"--force=1\"]', 'php think api --force=1', 'Build Successed!', 1619147451, 1619147451, 1619147451, 'successed');
INSERT INTO `sp_command` VALUES (90, 'crud', '[\"--force=1\",\"--table=sp_yuyue\",\"--fields=id,admin_id,uid,s_time,e_time,yuyue_status,baochang_status,order,nums,ps,promoter_status,pay_status,random_status,time\",\"--relation=sp_order\",\"--relationmode=belongsto\",\"--relationforeignkey=order\",\"--relationprimarykey=order\",\"--relationfields=price,money,cut_price,pay_money,pay_type,paly_status,baochang_status,pay_status,main_order,main_status,random_status\"]', 'php think crud --force=1 --table=sp_yuyue --fields=id,admin_id,uid,s_time,e_time,yuyue_status,baochang_status,order,nums,ps,promoter_status,pay_status,random_status,time --relation=sp_order --relationmode=belongsto --relationforeignkey=order --relationprimarykey=order --relationfields=price,money,cut_price,pay_money,pay_type,paly_status,baochang_status,pay_status,main_order,main_status,random_status', 'Build Successed', 1619154519, 1619154519, 1619154519, 'successed');
INSERT INTO `sp_command` VALUES (91, 'menu', '[\"--controller=Yuyue\"]', 'php think menu --controller=Yuyue', 'Build Successed!', 1619154526, 1619154526, 1619154526, 'successed');
INSERT INTO `sp_command` VALUES (92, 'crud', '[\"--force=1\",\"--table=sp_order\",\"--relation=sp_user\",\"--relationmode=belongsto\",\"--relationforeignkey=uid\",\"--relationprimarykey=id\",\"--relationfields=nickname,headimage,openid\"]', 'php think crud --force=1 --table=sp_order --relation=sp_user --relationmode=belongsto --relationforeignkey=uid --relationprimarykey=id --relationfields=nickname,headimage,openid', 'Build Successed', 1619154592, 1619154592, 1619154592, 'successed');
INSERT INTO `sp_command` VALUES (93, 'menu', '[\"--controller=Order\"]', 'php think menu --controller=Order', 'Build Successed!', 1619154601, 1619154601, 1619154601, 'successed');
INSERT INTO `sp_command` VALUES (94, 'crud', '[\"--table=sp_bill_space\",\"--relation=sp_space\",\"--relationmode=belongsto\",\"--relationforeignkey=admin_id\",\"--relationprimarykey=id\",\"--relationfields=name\"]', 'php think crud --table=sp_bill_space --relation=sp_space --relationmode=belongsto --relationforeignkey=admin_id --relationprimarykey=id --relationfields=name', 'Build Successed', 1619155021, 1619155021, 1619155021, 'successed');
INSERT INTO `sp_command` VALUES (95, 'menu', '[\"--controller=Space\"]', 'php think menu --controller=Space', 'Build Successed!', 1619155072, 1619155072, 1619155072, 'successed');
INSERT INTO `sp_command` VALUES (96, 'menu', '[\"--controller=BillSpace\"]', 'php think menu --controller=BillSpace', 'Build Successed!', 1619155159, 1619155159, 1619155159, 'successed');
INSERT INTO `sp_command` VALUES (97, 'menu', '[\"--controller=BillSpace\"]', 'php think menu --controller=BillSpace', 'Build Successed!', 1619162487, 1619162487, 1619162487, 'successed');
INSERT INTO `sp_command` VALUES (98, 'api', '[\"--force=1\"]', 'php think api --force=1', 'Build Successed!', 1619162491, 1619162491, 1619162491, 'successed');
INSERT INTO `sp_command` VALUES (99, 'api', '[\"--force=1\"]', 'php think api --force=1', 'Build Successed!', 1619162623, 1619162623, 1619162623, 'successed');
INSERT INTO `sp_command` VALUES (100, 'api', '[\"--force=1\"]', 'php think api --force=1', 'Build Successed!', 1619164053, 1619164053, 1619164053, 'successed');
INSERT INTO `sp_command` VALUES (101, 'api', '[\"--force=1\"]', 'php think api --force=1', NULL, 1619231626, 1619231626, 1619231626, 'failured');
INSERT INTO `sp_command` VALUES (102, 'api', '[\"--force=1\"]', 'php think api --force=1', NULL, 1619231630, 1619231630, 1619231630, 'failured');
INSERT INTO `sp_command` VALUES (103, 'api', '[\"--force=1\"]', 'php think api --force=1', NULL, 1619231651, 1619231651, 1619231651, 'failured');
INSERT INTO `sp_command` VALUES (104, 'api', '[\"--force=1\"]', 'php think api --force=1', 'Build Successed!', 1619231674, 1619231674, 1619231674, 'successed');
INSERT INTO `sp_command` VALUES (105, 'api', '[\"--force=1\"]', 'php think api --force=1', 'Build Successed!', 1620629172, 1620629172, 1620629172, 'successed');
INSERT INTO `sp_command` VALUES (106, 'api', '[\"--force=1\"]', 'php think api --force=1', 'Build Successed!', 1620629253, 1620629253, 1620629253, 'successed');
INSERT INTO `sp_command` VALUES (107, 'api', '[\"--force=1\"]', 'php think api --force=1', NULL, 1621040957, 1621040957, 1621040957, 'failured');
INSERT INTO `sp_command` VALUES (108, 'api', '[\"--force=1\"]', 'php think api --force=1', 'Build Successed!', 1621040985, 1621040985, 1621040985, 'successed');
INSERT INTO `sp_command` VALUES (109, 'api', '[\"--force=1\"]', 'php think api --force=1', 'Build Successed!', 1621041070, 1621041070, 1621041070, 'successed');
COMMIT;

-- ----------------------------
-- Table structure for sp_config
-- ----------------------------
DROP TABLE IF EXISTS `sp_config`;
CREATE TABLE `sp_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '变量名',
  `group` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '分组',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text COLLATE utf8mb4_unicode_ci COMMENT '变量值',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '变量字典数据',
  `rule` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '扩展属性',
  `setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '配置',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统配置';

-- ----------------------------
-- Records of sp_config
-- ----------------------------
BEGIN;
INSERT INTO `sp_config` VALUES (1, 'name', 'basic', 'Site name', '请填写站点名称', 'string', '运动场地预约', '', 'required', '', NULL);
INSERT INTO `sp_config` VALUES (2, 'beian', 'basic', 'Beian', '粤ICP备15000000号-1', 'string', '', '', '', '', NULL);
INSERT INTO `sp_config` VALUES (3, 'cdnurl', 'basic', 'Cdn url', '如果全站静态资源使用第三方云储存请配置该值', 'string', '', '', '', '', NULL);
INSERT INTO `sp_config` VALUES (4, 'version', 'basic', 'Version', '如果静态资源有变动请重新配置该值', 'string', '1.0.1', '', 'required', '', NULL);
INSERT INTO `sp_config` VALUES (5, 'timezone', 'basic', 'Timezone', '', 'string', 'Asia/Shanghai', '', 'required', '', NULL);
INSERT INTO `sp_config` VALUES (6, 'forbiddenip', 'basic', 'Forbidden ip', '一行一条记录', 'text', '', '', '', '', NULL);
INSERT INTO `sp_config` VALUES (7, 'languages', 'basic', 'Languages', '', 'array', '{\"backend\":\"zh-cn\",\"frontend\":\"zh-cn\"}', '', 'required', '', NULL);
INSERT INTO `sp_config` VALUES (8, 'fixedpage', 'basic', 'Fixed page', '请尽量输入左侧菜单栏存在的链接', 'string', 'dashboard', '', 'required', '', NULL);
INSERT INTO `sp_config` VALUES (9, 'categorytype', 'dictionary', 'Category type', '', 'array', '{\"default\":\"Default\",\"page\":\"Page\",\"article\":\"Article\",\"test\":\"Test\"}', '', '', '', NULL);
INSERT INTO `sp_config` VALUES (10, 'configgroup', 'dictionary', 'Config group', '', 'array', '{\"basic\":\"Basic\",\"weixin\":\"Weixin\",\"email\":\"Email\",\"dictionary\":\"Dictionary\",\"user\":\"User\",\"example\":\"Example\"}', '', '', '', NULL);
INSERT INTO `sp_config` VALUES (11, 'mail_type', 'email', 'Mail type', '选择邮件发送方式', 'select', '1', '[\"请选择\",\"SMTP\"]', '', '', '');
INSERT INTO `sp_config` VALUES (12, 'mail_smtp_host', 'email', 'Mail smtp host', '错误的配置发送邮件会导致服务器超时', 'string', 'smtp.qq.com', '', '', '', '');
INSERT INTO `sp_config` VALUES (13, 'mail_smtp_port', 'email', 'Mail smtp port', '(不加密默认25,SSL默认465,TLS默认587)', 'string', '465', '', '', '', '');
INSERT INTO `sp_config` VALUES (14, 'mail_smtp_user', 'email', 'Mail smtp user', '（填写完整用户名）', 'string', '10000', '', '', '', '');
INSERT INTO `sp_config` VALUES (15, 'mail_smtp_pass', 'email', 'Mail smtp password', '（填写您的密码或授权码）', 'string', 'password', '', '', '', '');
INSERT INTO `sp_config` VALUES (16, 'mail_verify_type', 'email', 'Mail vertify type', '（SMTP验证方式[推荐SSL]）', 'select', '2', '[\"无\",\"TLS\",\"SSL\"]', '', '', '');
INSERT INTO `sp_config` VALUES (17, 'mail_from', 'email', 'Mail from', '', 'string', '10000@qq.com', '', '', '', '');
INSERT INTO `sp_config` VALUES (18, 'app_id', 'weixin', '小程序Appid', '小程序Appid', 'string', 'wx38f6f26dc6705c73', '', '', '', NULL);
INSERT INTO `sp_config` VALUES (19, 'secret', 'weixin', '小程序密匙', '小程序密匙secret', 'string', '80c72e48b093e803a89fb6377974eb26', '', '', '', NULL);
INSERT INTO `sp_config` VALUES (20, 'mch_id', 'weixin', '微信商户号', '微信商户号', 'string', '1607387054', '', '', '', NULL);
INSERT INTO `sp_config` VALUES (21, 'key', 'weixin', 'API 密钥', '微信商户号密匙', 'string', '80c72e48b093e803a89fb6377974eb26', '', '', '', NULL);
INSERT INTO `sp_config` VALUES (22, 'wx_app_id', 'weixin', '公众号Appid', '微信公众号Appid', 'string', 'qq496631085', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');
INSERT INTO `sp_config` VALUES (23, 'wx_secret', 'weixin', '公众号密匙', '微信公众号的密匙', 'string', 'qq496631085qq496631085qq496631085', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');
INSERT INTO `sp_config` VALUES (24, 'platform_price', 'basic', '加入平台费用', '平台费用', 'number', '0.01', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');
INSERT INTO `sp_config` VALUES (25, 'apply_space', 'basic', '场地申请不需要审核', '当开关打开，申请场地只需要支付后即可通过，否则都需要审核', 'switch', '1', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');
INSERT INTO `sp_config` VALUES (26, 'disclaimer', 'basic', '免责声明', '免责声明', 'editor', '<p style=\"line-height: 1.8;\"><span style=\"background-color: rgb(255, 156, 0); font-size: 18px;\">测试</span><span style=\"font-size: 18px;\">﻿</span></p>', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');
INSERT INTO `sp_config` VALUES (27, 'agreement', 'basic', '包场协议', '包场协议', 'editor', '包场协议包场协议包场协议包场协议包场协议', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');
INSERT INTO `sp_config` VALUES (28, 'accessToken', 'basic', '灯的accessToken', '请不要修改此项', 'string', '', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');
INSERT INTO `sp_config` VALUES (29, 'free_time', 'basic', '免费时间/分钟', '进场后多少分钟内出场免费', 'number', '10', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');
COMMIT;

-- ----------------------------
-- Table structure for sp_device
-- ----------------------------
DROP TABLE IF EXISTS `sp_device`;
CREATE TABLE `sp_device` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '设备id',
  `status` enum('1','2') DEFAULT NULL COMMENT '设备:1=进,2=出',
  `space_id` int(10) DEFAULT NULL COMMENT '场地id',
  `time` int(10) DEFAULT NULL COMMENT '时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '触发时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='设备';

-- ----------------------------
-- Records of sp_device
-- ----------------------------
BEGIN;
INSERT INTO `sp_device` VALUES (000001, '1', 1, 1, 1619158589);
INSERT INTO `sp_device` VALUES (000002, '2', 1, 1, 1);
INSERT INTO `sp_device` VALUES (000003, '1', 5, 1618886669, NULL);
INSERT INTO `sp_device` VALUES (000004, '2', 5, 1618886669, NULL);
INSERT INTO `sp_device` VALUES (000007, '1', 7, 1618904687, 1620380014);
INSERT INTO `sp_device` VALUES (000008, '2', 7, 1618904687, 1620380023);
COMMIT;

-- ----------------------------
-- Table structure for sp_device_log
-- ----------------------------
DROP TABLE IF EXISTS `sp_device_log`;
CREATE TABLE `sp_device_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ps` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` enum('1','2') DEFAULT NULL COMMENT '设备:1=进,2=出',
  `time` int(10) DEFAULT NULL COMMENT '时间',
  `admin_id` int(10) DEFAULT NULL,
  `order` varchar(50) DEFAULT NULL COMMENT '订单',
  `device_id` int(10) DEFAULT NULL COMMENT '设备id',
  `uid` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COMMENT='驱动日志';

-- ----------------------------
-- Records of sp_device_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_ems
-- ----------------------------
DROP TABLE IF EXISTS `sp_ems`;
CREATE TABLE `sp_ems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '事件',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '邮箱',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证码',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'IP',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='邮箱验证码表';

-- ----------------------------
-- Records of sp_ems
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_msg
-- ----------------------------
DROP TABLE IF EXISTS `sp_msg`;
CREATE TABLE `sp_msg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `msg` varchar(255) DEFAULT NULL,
  `time` int(10) unsigned DEFAULT NULL COMMENT '时间',
  `uid` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `status` enum('1','2') DEFAULT '1' COMMENT '阅读状态:1=未读,2=已读',
  `admin_id` int(10) unsigned DEFAULT NULL COMMENT '场地',
  `read_time` int(1) unsigned DEFAULT NULL COMMENT '阅读时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8mb4 COMMENT='消息';

-- ----------------------------
-- Records of sp_msg
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_order
-- ----------------------------
DROP TABLE IF EXISTS `sp_order`;
CREATE TABLE `sp_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order` varchar(32) DEFAULT NULL COMMENT '订单号',
  `uid` int(10) DEFAULT NULL COMMENT '用户id',
  `admin_id` int(10) DEFAULT NULL COMMENT '场馆id',
  `time` int(1) unsigned DEFAULT NULL COMMENT '下单时间',
  `coupons_id` int(10) unsigned DEFAULT NULL COMMENT '优惠券id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `money` decimal(10,2) DEFAULT NULL COMMENT '实际支付',
  `cut_price` decimal(10,2) DEFAULT NULL COMMENT '优惠的价格',
  `pay_money` int(10) DEFAULT NULL COMMENT '支付价格',
  `pay_type` enum('1','2') DEFAULT '1' COMMENT '支付方式:1=微信,2=钱包',
  `ps` varchar(255) DEFAULT NULL COMMENT '备注',
  `paly_status` enum('1','2','3','4','5') DEFAULT NULL COMMENT '运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进',
  `baochang_status` enum('1','2','3') DEFAULT NULL COMMENT '包场状态:1=个人,2=包场,3=其他',
  `e_time` int(10) DEFAULT NULL COMMENT '结束时间',
  `s_time` int(10) DEFAULT NULL COMMENT '开始时间',
  `pay_status` enum('1','2','3','4','5','6','7','8','9','10') DEFAULT '1' COMMENT '支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款',
  `user_ps` varchar(255) DEFAULT NULL COMMENT '用户备注',
  `money_status` enum('1','2') DEFAULT '1' COMMENT '到账状态:1=待到账,2=已到账',
  `send_msg` varchar(255) DEFAULT NULL COMMENT '发送消息内容',
  `s_door_time` int(10) DEFAULT NULL COMMENT '进门打开时间',
  `pay_time` int(10) DEFAULT NULL COMMENT '支付时间',
  `e_door_time` int(10) DEFAULT NULL COMMENT '出门打开时间',
  `deposit` decimal(10,2) DEFAULT NULL COMMENT '包场押金',
  `overtime_price` decimal(10,2) DEFAULT NULL COMMENT '超时的钱',
  `overtime_status` enum('1','2') DEFAULT '1' COMMENT '超时状态:1=未超时,2=超时',
  `refund_deposit` decimal(10,2) DEFAULT NULL COMMENT '退款押金',
  `deposit_status` enum('0','1','2') DEFAULT '0' COMMENT '押金状态:0=未交押金,1=已支付押金,2=已退还押金',
  `main_order` varchar(32) DEFAULT NULL COMMENT '主订单号',
  `main_status` enum('1','2') DEFAULT '1' COMMENT '主订单:1=主订单,2=子订单',
  `random_status` enum('1','2') DEFAULT '1' COMMENT '随机队员:1=否,2=随机',
  `refund_order` varchar(50) DEFAULT NULL COMMENT '退款订单',
  `refund_ps` varchar(50) DEFAULT NULL COMMENT '退款备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='用户订单';

-- ----------------------------
-- Records of sp_order
-- ----------------------------
BEGIN;
INSERT INTO `sp_order` VALUES (47, 'Bc72021050718011497564899941598', 7, 7, 1620381674, NULL, 0.06, NULL, NULL, 6, '1', NULL, '5', '2', 1620385680, 1620381900, '2', NULL, '1', NULL, NULL, 1620381679, NULL, 0.02, NULL, '1', NULL, '1', NULL, '1', '1', NULL, NULL);
INSERT INTO `sp_order` VALUES (48, 'Son62021050718011497564899941598', 6, 7, 1620381711, NULL, 0.02, NULL, NULL, 2, '1', NULL, '5', '2', 1620385680, 1620381900, '2', NULL, '1', NULL, NULL, 1620381716, NULL, 0.00, NULL, '1', NULL, '0', 'Bc72021050718011497564899941598', '2', '1', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sp_order_platform
-- ----------------------------
DROP TABLE IF EXISTS `sp_order_platform`;
CREATE TABLE `sp_order_platform` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT NULL COMMENT '用户id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `money` decimal(10,2) DEFAULT NULL COMMENT '实际支付金额',
  `time` int(10) DEFAULT NULL COMMENT '创建订单时间',
  `order` varchar(32) DEFAULT NULL COMMENT '订单号',
  `pay_status` enum('0','1','2','3','4','5','6','7','8','9','10','11','12','13') DEFAULT '0' COMMENT '状态:0=待查询,1=待支付,2=已支付,3=已过期,4=已退款',
  `pay_time` int(10) DEFAULT NULL COMMENT '支付时间',
  `ps` varchar(255) DEFAULT NULL COMMENT '备注',
  `query_time` int(10) DEFAULT '0' COMMENT '查询时间',
  `content` text COMMENT '返回内容',
  `space_id` int(10) DEFAULT NULL COMMENT '场馆id',
  `pay_money` int(10) DEFAULT NULL COMMENT '支付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='平台费用';

-- ----------------------------
-- Records of sp_order_platform
-- ----------------------------
BEGIN;
INSERT INTO `sp_order_platform` VALUES (3, 3, 0.01, 0.01, 1618906369, 'Plat2021042016124949100519313272', '2', 1618906431, NULL, 0, NULL, 7, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sp_orderde
-- ----------------------------
DROP TABLE IF EXISTS `sp_orderde`;
CREATE TABLE `sp_orderde` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order` varchar(32) DEFAULT NULL COMMENT '订单号',
  `uid` int(10) DEFAULT NULL COMMENT '用户id',
  `admin_id` int(10) DEFAULT NULL COMMENT '场馆id',
  `time` int(1) unsigned DEFAULT NULL COMMENT '下单时间',
  `coupons_id` int(10) unsigned DEFAULT NULL COMMENT '优惠券id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `money` decimal(10,2) DEFAULT NULL COMMENT '实际支付',
  `cut_price` decimal(10,2) DEFAULT NULL COMMENT '优惠的价格',
  `pay_money` int(10) DEFAULT NULL COMMENT '支付价格',
  `pay_type` enum('1','2') DEFAULT '1' COMMENT '支付方式:1=微信,2=钱包',
  `ps` varchar(255) DEFAULT NULL COMMENT '备注',
  `paly_status` enum('1','2','3','4','5') DEFAULT NULL COMMENT '运动状态:1=待进入,2=进行中,3=时间已到,4=完成,5=超时未进',
  `baochang_status` enum('1','2','3') DEFAULT NULL COMMENT '包场状态:1=个人,2=包场,3=其他',
  `e_time` int(10) DEFAULT NULL COMMENT '结束时间',
  `s_time` int(10) DEFAULT NULL COMMENT '开始时间',
  `pay_status` enum('1','2','3','4','5','6','7','8','9','10') DEFAULT '1' COMMENT '支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款',
  `user_ps` varchar(255) DEFAULT NULL COMMENT '用户备注',
  `money_status` enum('1','2') DEFAULT '1' COMMENT '到账状态:1=待到账,2=已到账',
  `send_msg` varchar(255) DEFAULT NULL COMMENT '发送消息内容',
  `s_door_time` int(10) DEFAULT NULL COMMENT '进门打开时间',
  `pay_time` int(10) DEFAULT NULL COMMENT '支付时间',
  `e_door_time` int(10) DEFAULT NULL COMMENT '出门打开时间',
  `deposit` decimal(10,2) DEFAULT NULL COMMENT '包场押金',
  `overtime_price` decimal(10,2) DEFAULT NULL COMMENT '超时的钱',
  `overtime_status` enum('1','2') DEFAULT '1' COMMENT '超时状态:1=未超时,2=超时',
  `refund_deposit` decimal(10,2) DEFAULT NULL COMMENT '退款押金',
  `deposit_status` enum('0','1','2') DEFAULT '0' COMMENT '押金状态:0=未交押金,1=已支付押金,2=已退还押金',
  `main_order` varchar(32) DEFAULT NULL COMMENT '主订单号',
  `main_status` enum('1','2') DEFAULT '1' COMMENT '主订单:1=主订单,2=子订单',
  `random_status` enum('1','2') DEFAULT '1' COMMENT '随机队员:1=否,2=随机',
  `refund_order` varchar(50) DEFAULT NULL COMMENT '退款订单',
  `refund_ps` varchar(50) DEFAULT NULL COMMENT '退款备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='用户订单';

-- ----------------------------
-- Records of sp_orderde
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_play_time
-- ----------------------------
DROP TABLE IF EXISTS `sp_play_time`;
CREATE TABLE `sp_play_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_time` int(10) DEFAULT NULL COMMENT '开始时间',
  `e_time` int(10) DEFAULT NULL COMMENT '结束时间',
  `uid` int(10) DEFAULT NULL COMMENT '用户ID',
  `sec` int(10) DEFAULT NULL COMMENT '运动秒数',
  `admin_id` int(10) DEFAULT NULL COMMENT '场地id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='运动时间';

-- ----------------------------
-- Records of sp_play_time
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_sms
-- ----------------------------
DROP TABLE IF EXISTS `sp_sms`;
CREATE TABLE `sp_sms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '事件',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '手机号',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证码',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'IP',
  `createtime` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='短信验证码表';

-- ----------------------------
-- Records of sp_sms
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_space
-- ----------------------------
DROP TABLE IF EXISTS `sp_space`;
CREATE TABLE `sp_space` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT NULL COMMENT '用户id',
  `name` varchar(255) DEFAULT NULL COMMENT '场馆名称',
  `time` int(10) DEFAULT NULL COMMENT '申请日期',
  `area` varchar(10) DEFAULT NULL COMMENT '区',
  `city` varchar(10) DEFAULT NULL COMMENT '市',
  `province` varchar(10) DEFAULT NULL COMMENT '省',
  `addr` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `image` varchar(255) DEFAULT NULL COMMENT '场馆主图',
  `blis_image` varchar(255) DEFAULT NULL COMMENT '营业执照图',
  `photo_images` varchar(1000) DEFAULT NULL COMMENT '场馆照片',
  `cate_ids` varchar(255) DEFAULT NULL COMMENT '分类id',
  `tag_str` varchar(255) DEFAULT NULL COMMENT '标签文字',
  `description` varchar(255) DEFAULT NULL COMMENT '简介',
  `play_status` enum('1','2') DEFAULT '1' COMMENT '陪玩状态:1=正常营业,2=停止营业',
  `service` varchar(512) DEFAULT NULL COMMENT '服务说明',
  `specs` varchar(512) DEFAULT NULL COMMENT '规格说明',
  `online_status` enum('1','2','3') DEFAULT '3' COMMENT '状态:1=在线,2=忙碌,3=离线',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '余额',
  `tel` varchar(15) DEFAULT NULL COMMENT '手机号',
  `wechat` varchar(20) DEFAULT NULL COMMENT '微信号',
  `apply_status` enum('1','2','3') DEFAULT '2' COMMENT '申请状态:1=通过,2=申请中,3=已拒绝',
  `lng` varchar(100) DEFAULT NULL COMMENT '经度',
  `lat` varchar(100) DEFAULT NULL COMMENT '纬度',
  `weigh` int(10) DEFAULT NULL,
  `s_time` varchar(10) DEFAULT NULL COMMENT '营业开始时间',
  `e_time` varchar(10) DEFAULT NULL COMMENT '营业结束时间',
  `order` varchar(50) DEFAULT NULL COMMENT '订单号',
  `pay_status` enum('1','2') DEFAULT '1' COMMENT '支付状态:1=待支付,2=已支付',
  `week` varchar(16) DEFAULT NULL COMMENT '星期几营业',
  `baochang_price` decimal(10,2) DEFAULT NULL COMMENT '包场每小时价格',
  `price` decimal(10,2) DEFAULT NULL COMMENT '散客每小时价格',
  `deposit` decimal(10,2) DEFAULT NULL COMMENT '包场押金',
  `overtime_price` decimal(10,2) DEFAULT NULL COMMENT '超时每小时价格',
  `in_device_id` int(6) unsigned zerofill DEFAULT NULL COMMENT '进设备id',
  `out_device_id` int(6) unsigned zerofill DEFAULT NULL COMMENT '出设备id',
  `admin_id` int(10) DEFAULT NULL,
  `hot` int(10) DEFAULT '0' COMMENT '热度',
  `daochang_deposit` decimal(10,2) DEFAULT NULL COMMENT '到场定金',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='场地';

-- ----------------------------
-- Records of sp_space
-- ----------------------------
BEGIN;
INSERT INTO `sp_space` VALUES (7, 3, '诺远球馆', 1618904687, '惠山区', '无锡市', '江苏省', '江苏省无锡市惠山区锦绣路', '/uploads/20210420/2d1dde1511dad2c2fc52ad1552ab01f8.jpg', '/uploads/20210420/22b1db482c0eb9d77588d51a5c51d761.jpg', '/uploads/20210420/a1083a1fe9136dbdc5edb0689dedfb59.jpeg,/uploads/20210420/65be614f5b5a115ecc486eec1b1e0b69.jpg,/uploads/20210420/40a4f02c7d1fcb2cfb49eff0889b2be2.jpeg', '3', '室内,可包场,羽毛球', '测试一下', '1', '设施很好', '场地规格很棒', '1', 1.08, '18205034747', '', '1', '120.29834', '31.68096', 0, '1620349200', '1620388800', '', '1', '1,2,3,4,5,6,7', 0.02, 0.01, 0.02, 0.02, 000007, 000008, 7, 29, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sp_test
-- ----------------------------
DROP TABLE IF EXISTS `sp_test`;
CREATE TABLE `sp_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类ID(单选)',
  `category_ids` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类ID(多选)',
  `week` enum('monday','tuesday','wednesday') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '星期(单选):monday=星期一,tuesday=星期二,wednesday=星期三',
  `flag` set('hot','index','recommend') COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '标志(多选):hot=热门,index=首页,recommend=推荐',
  `genderdata` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male' COMMENT '性别(单选):male=男,female=女',
  `hobbydata` set('music','reading','swimming') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '爱好(多选):music=音乐,reading=读书,swimming=游泳',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '标题',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片',
  `images` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片组',
  `attachfile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '附件',
  `keywords` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '关键字',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '描述',
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '省市',
  `json` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '配置:key=名称,value=值',
  `price` float(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '价格',
  `views` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击',
  `startdate` date DEFAULT NULL COMMENT '开始日期',
  `activitytime` datetime DEFAULT NULL COMMENT '活动时间(datetime)',
  `year` year(4) DEFAULT NULL COMMENT '年',
  `times` time DEFAULT NULL COMMENT '时间',
  `refreshtime` int(10) DEFAULT NULL COMMENT '刷新时间(int)',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `deletetime` int(10) DEFAULT NULL COMMENT '删除时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `switch` tinyint(1) NOT NULL DEFAULT '0' COMMENT '开关',
  `status` enum('normal','hidden') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
  `state` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '状态值:0=禁用,1=正常,2=推荐',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='测试表';

-- ----------------------------
-- Records of sp_test
-- ----------------------------
BEGIN;
INSERT INTO `sp_test` VALUES (1, 0, 12, '12,13', 'monday', 'hot,index', 'male', 'music,reading', '我是一篇测试文章', '<p>我是测试内容</p>', '/assets/img/avatar.png', '/assets/img/avatar.png,/assets/img/qrcode.png', '/assets/img/avatar.png', '关键字', '描述', '广西壮族自治区/百色市/平果县', '{\"a\":\"1\",\"b\":\"2\"}', 0.00, 0, '2017-07-10', '2017-07-10 18:24:45', 2017, '18:24:45', 1491635035, 1491635035, 1491635035, NULL, 0, 1, 'normal', '1');
COMMIT;

-- ----------------------------
-- Table structure for sp_user
-- ----------------------------
DROP TABLE IF EXISTS `sp_user`;
CREATE TABLE `sp_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组别ID',
  `username` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '昵称',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码',
  `salt` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码盐',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '头像',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `bio` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '格言',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '余额',
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '积分',
  `successions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '连续登录天数',
  `maxsuccessions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '最大连续登录天数',
  `prevtime` int(10) DEFAULT NULL COMMENT '上次登录时间',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '登录IP',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `joinip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '加入IP',
  `jointime` int(10) DEFAULT NULL COMMENT '加入时间',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Token',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态',
  `verification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证',
  `unionid` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开放平台id',
  `pid` int(10) DEFAULT NULL COMMENT '上级id',
  `headimage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微信头像',
  `openid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'openid',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `email` (`email`),
  KEY `mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员表';

-- ----------------------------
-- Records of sp_user
-- ----------------------------
BEGIN;
INSERT INTO `sp_user` VALUES (1, 1, 'admin', 'admin', '8c876fa6e2db980b5e1a3895860cab7b', 'fca989', 'admin@163.com', '13888888888', '', 0, 0, '2017-04-08', '', 0.00, 0, 1, 1, 1618896140, 1618897966, '114.224.144.226', 0, '127.0.0.1', 1491635035, 0, 1618897966, '', 'normal', '', NULL, NULL, NULL, NULL);
INSERT INTO `sp_user` VALUES (2, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 2, 2, 1617961908, 1618038845, '114.224.131.31', 0, '114.224.131.31', 1617959527, 1617959527, 1618038845, '', 'normal', '', NULL, NULL, NULL, 'oae6f4seAt1mJIz6g1aUrTxDNTvI');
INSERT INTO `sp_user` VALUES (3, 0, '', 'A小程序、app开发 何维峰', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 2, 6, 1621305025, 1621307223, '114.223.143.173', 0, '114.224.144.226', 1618896567, 1618896567, 1621307223, '', 'normal', '', NULL, NULL, 'https://thirdwx.qlogo.cn/mmopen/vi_32/MahHGwLzRepOozMVCIdRUC1DEyLDqmUBZVXX478Uqh0UrVG1q0MBQcumlib6gqajeBdo3QibxA6zrrWopYib6ictibg/132', 'oRV_J5AkWsR5LHm6Syv2ZfHZ0JQU');
INSERT INTO `sp_user` VALUES (4, 0, '', 'XiaoHe', '8c876fa6e2db980b5e1a3895860cab7b', 'fca989', '', '18784263545', '', 0, 0, NULL, '', 0.00, 0, 2, 4, 1620381099, 1620381162, '114.224.147.96', 0, '114.224.144.226', 1618982813, 1618982813, 1620381162, '', 'normal', '', NULL, NULL, 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo6EmiclwDUV94If1kbhtRbiaI9uYmRaskgR0eabGNqM2iabeVWENg57LiasyMpsOILAeziaQSg2M4edTA/132', 'oRV_J5ABmVXDL6s1HxO1pNR-L6zI');
INSERT INTO `sp_user` VALUES (5, 0, '', '凉介💥', '8c876fa6e2db980b5e1a3895860cab7b', 'fca989', '', '13888888885', '', 0, 0, NULL, '', 0.00, 0, 1, 2, 1620721065, 1621315978, '114.223.143.173', 0, '114.224.144.226', 1619155774, 1619155774, 1621315978, '', 'normal', '', NULL, NULL, 'https://thirdwx.qlogo.cn/mmopen/vi_32/3hZfficKPGCpgatE7bMbgPzs8OnYfoDNatbyTRdlDlAAvqz0HGmLYiaUicdTVtwQWbOnQ6jGHq52TeCZyX90vMlTg/132', 'oRV_J5FkGvSKRxT0F6HViivUwjMI');
INSERT INTO `sp_user` VALUES (6, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 8, 8, 1621394415, 1621394440, '39.144.105.246', 0, '117.136.119.88', 1619169972, 1619169972, 1621394440, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5NR_IXlTxcuLo5smrH9nXpM');
INSERT INTO `sp_user` VALUES (7, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620380448, 1620381649, '223.104.68.67', 0, '219.134.180.173', 1620371707, 1620371707, 1620381649, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5HbGLWh1R7PGovgWZCaksDs');
INSERT INTO `sp_user` VALUES (8, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620372448, 1620375386, '81.68.171.240', 0, '81.68.171.204', 1620372448, 1620372448, 1620375386, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5FhZOHA4JbindvlkoZEkytw');
INSERT INTO `sp_user` VALUES (9, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620372448, 1620373611, '49.234.16.75', 0, '118.89.199.22', 1620372448, 1620372448, 1620373611, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5Gc7Oo6fpGKQlCipZTmPLdc');
INSERT INTO `sp_user` VALUES (10, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620372597, 1620372597, '175.24.9.6', 0, '175.24.9.6', 1620372597, 1620372597, 1620372597, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5ESSSKNihIU_L-0ABV83yPI');
INSERT INTO `sp_user` VALUES (11, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 2, 2, 1620372860, 1620441307, '114.224.147.96', 0, '114.224.147.96', 1620372776, 1620372776, 1620441307, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5AMbxfX60CFxvvwxlh6iyFA');
INSERT INTO `sp_user` VALUES (12, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620373611, 1620373611, '81.68.169.92', 0, '81.68.169.92', 1620373611, 1620373611, 1620373611, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5E_1QcW7zSEA0lJQ3Y7ySRU');
INSERT INTO `sp_user` VALUES (13, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620376163, 1620376172, '175.24.19.224', 0, '123.206.217.42', 1620374256, 1620374256, 1620376172, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5AEEbmeIbLJfuIiK6O62bmI');
INSERT INTO `sp_user` VALUES (14, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620374265, 1620374265, '81.68.171.108', 0, '81.68.171.108', 1620374265, 1620374265, 1620374265, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5Or-rMPSNrQHa0EIca55Ax8');
INSERT INTO `sp_user` VALUES (15, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620374265, 1620374265, '81.68.168.232', 0, '81.68.168.232', 1620374265, 1620374265, 1620374265, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5JP78BRdfcw4ClGCL11vAT0');
INSERT INTO `sp_user` VALUES (16, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620375386, 1620375386, '212.64.82.224', 0, '212.64.82.224', 1620375386, 1620375386, 1620375386, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5OhxR36wX9ftPZRWBwKWscs');
INSERT INTO `sp_user` VALUES (17, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620380585, 1620381720, '117.136.33.147', 0, '117.136.33.147', 1620379920, 1620379920, 1620381720, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5ECrIg7BRcWBXxhSGF7H_PM');
INSERT INTO `sp_user` VALUES (18, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620383945, 1620383992, '223.104.63.29', 0, '223.104.63.29', 1620383945, 1620383945, 1620383992, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5Dh98pmYH_CWEUN9FW8Yisg');
INSERT INTO `sp_user` VALUES (19, 0, '', '李珮心', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620442299, 1620442405, '115.159.50.209', 0, '175.24.68.25', 1620442299, 1620442299, 1620442405, '', 'normal', '', NULL, NULL, 'https://thirdwx.qlogo.cn/mmhead/hxZ0yTicB8p8jgwKt2RVuGriaqX68m17nTdVicolWxT3NA/132', 'oRV_J5ODmlDvD6s-KnVyNc8idhEI');
INSERT INTO `sp_user` VALUES (20, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620554420, 1620554425, '203.190.105.9', 0, '203.190.111.6', 1620554386, 1620554386, 1620554425, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5Ju910FkU9lfN_jsTRpgaI8');
INSERT INTO `sp_user` VALUES (21, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620619137, 1620626078, '101.84.34.31', 0, '101.84.34.31', 1620609753, 1620609753, 1620626078, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5Auyora7FPp18uWgwxLSdy0');
INSERT INTO `sp_user` VALUES (22, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620619990, 1620622719, '125.127.39.62', 0, '36.18.175.50', 1620619968, 1620619968, 1620622719, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5Kli6A1NNr3KUoIWdX9VHHw');
INSERT INTO `sp_user` VALUES (23, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620784846, 1620784988, '116.228.38.30', 0, '116.228.38.30', 1620784771, 1620784771, 1620784988, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5Mmv1naHgaO8BPFzgjSO1s0');
INSERT INTO `sp_user` VALUES (24, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620865220, 1620865220, '114.240.89.196', 0, '114.240.89.196', 1620865220, 1620865220, 1620865220, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5BYBK1AYPAYLALmUi20wAO0');
INSERT INTO `sp_user` VALUES (25, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620956932, 1620956940, '58.246.250.106', 0, '58.246.250.106', 1620956807, 1620956807, 1620956940, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5JndEwJQHF7MG9LNe-R7s3o');
INSERT INTO `sp_user` VALUES (26, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1620984777, 1620984777, '39.149.12.211', 0, '39.149.12.211', 1620984777, 1620984777, 1620984777, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5ARTSXS42uH498jbPywBtg8');
INSERT INTO `sp_user` VALUES (27, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1621049282, 1621049321, '120.204.168.172', 0, '120.204.168.172', 1621049282, 1621049282, 1621049321, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5BC_WIonAfPJ5cQop6cUehc');
INSERT INTO `sp_user` VALUES (28, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1621061408, 1621061408, '49.234.218.83', 0, '49.234.218.83', 1621061408, 1621061408, 1621061408, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5IcEEGQ2OCIxicMsyMY1Ok0');
INSERT INTO `sp_user` VALUES (29, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1621244419, 1621244441, '183.67.62.211', 0, '183.67.62.211', 1621244419, 1621244419, 1621244441, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5Mw9EVg25EZjOz_a5mesImI');
INSERT INTO `sp_user` VALUES (30, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1621305120, 1621305138, '180.162.62.24', 0, '180.162.62.24', 1621305104, 1621305104, 1621305138, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5Ctu1cvmM076yusg-ZPP2Q4');
INSERT INTO `sp_user` VALUES (31, 0, '', 'sunshine', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1621397683, 1621397689, '39.144.40.197', 0, '39.144.40.197', 1621394451, 1621394451, 1621397689, '', 'normal', '', NULL, NULL, 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwazNhgWWsad4HJxznnUqjH7wUgseLyD65GibVgLVfGO7toeibEZia6Q4eeMRy3ZKssT0CXzUeia6Ngg/132', 'oRV_J5EDM65vhWwJKyPf_wu3NwWo');
INSERT INTO `sp_user` VALUES (32, 0, '', '', '', '', '', '', '', 0, 0, NULL, '', 0.00, 0, 1, 1, 1621395097, 1621398530, '114.87.244.29', 0, '180.162.62.24', 1621394459, 1621394459, 1621398530, '', 'normal', '', NULL, NULL, NULL, 'oRV_J5JyOuCijJk87d4mMYscSwj4');
COMMIT;

-- ----------------------------
-- Table structure for sp_user_group
-- ----------------------------
DROP TABLE IF EXISTS `sp_user_group`;
CREATE TABLE `sp_user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '组名',
  `rules` text COLLATE utf8mb4_unicode_ci COMMENT '权限节点',
  `createtime` int(10) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` enum('normal','hidden') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员组表';

-- ----------------------------
-- Records of sp_user_group
-- ----------------------------
BEGIN;
INSERT INTO `sp_user_group` VALUES (1, '默认组', '1,2,3,4,5,6,7,8,9,10,11,12', 1491635035, 1491635035, 'normal');
COMMIT;

-- ----------------------------
-- Table structure for sp_user_money_log
-- ----------------------------
DROP TABLE IF EXISTS `sp_user_money_log`;
CREATE TABLE `sp_user_money_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更余额',
  `before` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更前余额',
  `after` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更后余额',
  `memo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员余额变动表';

-- ----------------------------
-- Records of sp_user_money_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_user_rule
-- ----------------------------
DROP TABLE IF EXISTS `sp_user_rule`;
CREATE TABLE `sp_user_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '标题',
  `remark` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `ismenu` tinyint(1) DEFAULT NULL COMMENT '是否菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) DEFAULT '0' COMMENT '权重',
  `status` enum('normal','hidden') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员规则表';

-- ----------------------------
-- Records of sp_user_rule
-- ----------------------------
BEGIN;
INSERT INTO `sp_user_rule` VALUES (1, 0, 'index', 'Frontend', '', 1, 1491635035, 1491635035, 1, 'normal');
INSERT INTO `sp_user_rule` VALUES (2, 0, 'api', 'API Interface', '', 1, 1491635035, 1491635035, 2, 'normal');
INSERT INTO `sp_user_rule` VALUES (3, 1, 'user', 'User Module', '', 1, 1491635035, 1491635035, 12, 'normal');
INSERT INTO `sp_user_rule` VALUES (4, 2, 'user', 'User Module', '', 1, 1491635035, 1491635035, 11, 'normal');
INSERT INTO `sp_user_rule` VALUES (5, 3, 'index/user/login', 'Login', '', 0, 1491635035, 1491635035, 5, 'normal');
INSERT INTO `sp_user_rule` VALUES (6, 3, 'index/user/register', 'Register', '', 0, 1491635035, 1491635035, 7, 'normal');
INSERT INTO `sp_user_rule` VALUES (7, 3, 'index/user/index', 'User Center', '', 0, 1491635035, 1491635035, 9, 'normal');
INSERT INTO `sp_user_rule` VALUES (8, 3, 'index/user/profile', 'Profile', '', 0, 1491635035, 1491635035, 4, 'normal');
INSERT INTO `sp_user_rule` VALUES (9, 4, 'api/user/login', 'Login', '', 0, 1491635035, 1491635035, 6, 'normal');
INSERT INTO `sp_user_rule` VALUES (10, 4, 'api/user/register', 'Register', '', 0, 1491635035, 1491635035, 8, 'normal');
INSERT INTO `sp_user_rule` VALUES (11, 4, 'api/user/index', 'User Center', '', 0, 1491635035, 1491635035, 10, 'normal');
INSERT INTO `sp_user_rule` VALUES (12, 4, 'api/user/profile', 'Profile', '', 0, 1491635035, 1491635035, 3, 'normal');
COMMIT;

-- ----------------------------
-- Table structure for sp_user_score_log
-- ----------------------------
DROP TABLE IF EXISTS `sp_user_score_log`;
CREATE TABLE `sp_user_score_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '变更积分',
  `before` int(10) NOT NULL DEFAULT '0' COMMENT '变更前积分',
  `after` int(10) NOT NULL DEFAULT '0' COMMENT '变更后积分',
  `memo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员积分变动表';

-- ----------------------------
-- Records of sp_user_score_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sp_user_token`;
CREATE TABLE `sp_user_token` (
  `token` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Token',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `expiretime` int(10) DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员Token表';

-- ----------------------------
-- Records of sp_user_token
-- ----------------------------
BEGIN;
INSERT INTO `sp_user_token` VALUES ('00b1f9cbcbd3306d9b6f5483e062cbc394dcdb5c', 3, 1618911995, 1621503995);
INSERT INTO `sp_user_token` VALUES ('0211e2d3895972bcd919ffa191ecd449005b8d26', 7, 1620372500, 1622964500);
INSERT INTO `sp_user_token` VALUES ('022c7cc776c0a8596b7eb2665062a4423173ddae', 17, 1620380585, 1622972585);
INSERT INTO `sp_user_token` VALUES ('02455ab937449f8f91fa34e072376d6b6d0fb436', 3, 1618994368, 1621586368);
INSERT INTO `sp_user_token` VALUES ('0319e784ba4793a25d1431ae54657467e47570df', 3, 1618971356, 1621563356);
INSERT INTO `sp_user_token` VALUES ('034384e139f602b01e1c9a95b1b4c4696c445da1', 6, 1619170686, 1621762686);
INSERT INTO `sp_user_token` VALUES ('03e8e217b6daf64f201f3332c190f1761cf11549', 6, 1620994498, 1623586498);
INSERT INTO `sp_user_token` VALUES ('043e6dacbdf3fd33f24f009e2f0de1c9efd61f80', 6, 1621252003, 1623844003);
INSERT INTO `sp_user_token` VALUES ('046cc816684614f63be6027c545d6a381d4c8cd7', 6, 1619170720, 1621762720);
INSERT INTO `sp_user_token` VALUES ('04c886d17e8400173101457e6f3cd4cb2e126adb', 3, 1618972732, 1621564732);
INSERT INTO `sp_user_token` VALUES ('04d8b47c18736cd52f482c20601ac5a1dd74267d', 4, 1618986805, 1621578805);
INSERT INTO `sp_user_token` VALUES ('04ffdee0a33c3e4cf0daac047938efd63d037b90', 4, 1618983161, 1621575161);
INSERT INTO `sp_user_token` VALUES ('05b307b22284315b475bf81a6583a8238cd124db', 4, 1619168345, 1621760345);
INSERT INTO `sp_user_token` VALUES ('05bb9f48f5a62528aa88176d7ef63b6cb41bee18', 3, 1618905636, 1621497636);
INSERT INTO `sp_user_token` VALUES ('05ce12abbd26c0fb8da6075e7d936d795b545bb1', 17, 1620381720, 1622973720);
INSERT INTO `sp_user_token` VALUES ('05e0a49be36eca82a1a51391b30239bf7a5f8b89', 3, 1618909137, 1621501137);
INSERT INTO `sp_user_token` VALUES ('06314913a1eff1e12f99647783e477b5676d2ded', 23, 1620784988, 1623376988);
INSERT INTO `sp_user_token` VALUES ('069fc21b71af089374822d0d4610a85673da4e1b', 3, 1620265313, 1622857313);
INSERT INTO `sp_user_token` VALUES ('06eec3d33e6698d9f1173f914efb2df6bc4e6a09', 4, 1620265134, 1622857134);
INSERT INTO `sp_user_token` VALUES ('072c99e1f3918e853ff965b19993d980100d6731', 16, 1620375386, 1622967386);
INSERT INTO `sp_user_token` VALUES ('0754d8e9ecac852a82abee2602526043b9808ab3', 3, 1619166957, 1621758957);
INSERT INTO `sp_user_token` VALUES ('075eefcde1883c19612fb35ad7060487cbc01f2e', 4, 1619149062, 1621741062);
INSERT INTO `sp_user_token` VALUES ('078042a8773509e75733e02883a6e8d859399a84', 3, 1618969242, 1621561242);
INSERT INTO `sp_user_token` VALUES ('078e1953965b9480f8a5f980b5115aa12c3db646', 19, 1620442299, 1623034299);
INSERT INTO `sp_user_token` VALUES ('07cec5962996f04c07ccbd2a78457ddbca7eec41', 3, 1618973254, 1621565254);
INSERT INTO `sp_user_token` VALUES ('0841738fd1460919772596698cb4eb0cf3ef20a2', 3, 1618914391, 1621506391);
INSERT INTO `sp_user_token` VALUES ('084853ebb9f50116a5e30e5291bc3615f4e943dc', 6, 1621092721, 1623684721);
INSERT INTO `sp_user_token` VALUES ('0898db91556dcad96ea6bc22110ae730761cd42f', 3, 1619155492, 1621747492);
INSERT INTO `sp_user_token` VALUES ('08db90c8800db321d97d980be26c6ea261dd4520', 6, 1620370932, 1622962932);
INSERT INTO `sp_user_token` VALUES ('09019ffada4080520601b32716db49f53f497736', 4, 1620265142, 1622857142);
INSERT INTO `sp_user_token` VALUES ('0915c5c283a86ba3e38dfa532b57bb33a93c77a7', 3, 1618901573, 1621493573);
INSERT INTO `sp_user_token` VALUES ('092134e529d8bc01455ccac7c24b973b0b31f8ff', 6, 1619170642, 1621762642);
INSERT INTO `sp_user_token` VALUES ('09579f6b71ed2692471f5089228ed9a4741ddb3b', 3, 1619167268, 1621759268);
INSERT INTO `sp_user_token` VALUES ('0970d4cf291fec08151c61226de8e2025de419c6', 4, 1620372834, 1622964834);
INSERT INTO `sp_user_token` VALUES ('099517be481490cc90c71a0e73c701710984a43a', 21, 1620619137, 1623211137);
INSERT INTO `sp_user_token` VALUES ('0a61752d43b0ab868dd76b97250ed4f61a525271', 7, 1620381649, 1622973649);
INSERT INTO `sp_user_token` VALUES ('0a8f605e87d0b40a5c3f27530c2fdbf1c08a5bcd', 4, 1620380142, 1622972142);
INSERT INTO `sp_user_token` VALUES ('0aed8bc8b6677e61959d0c696d67d93582ab208b', 6, 1621216431, 1623808431);
INSERT INTO `sp_user_token` VALUES ('0b30ff908e33cd6777d8d543f331f403a50c1c01', 4, 1618991889, 1621583889);
INSERT INTO `sp_user_token` VALUES ('0b413e514d264d505eb6de65b3b205c04a4443fa', 19, 1620442405, 1623034405);
INSERT INTO `sp_user_token` VALUES ('0b60ee2fec9a1f6bdf608f0a62b662995ce63111', 3, 1619162682, 1621754682);
INSERT INTO `sp_user_token` VALUES ('0b70fca1161b2dbfe8208786bed25dbda9cbdd8c', 5, 1619681738, 1622273738);
INSERT INTO `sp_user_token` VALUES ('0bbfa6dbbf6420c9220a0e82a0461c5fee258d2c', 7, 1620380438, 1622972438);
INSERT INTO `sp_user_token` VALUES ('0c3743e2fb4ad0449dbdb69772e1d5bc9b54ce61', 3, 1620375350, 1622967350);
INSERT INTO `sp_user_token` VALUES ('0c54dc162f9ebeea95b87b56ed2eacad6187aa21', 3, 1619326774, 1621918774);
INSERT INTO `sp_user_token` VALUES ('0cc3b976567b1de6971f4e38114d943a238b7691', 6, 1620863301, 1623455301);
INSERT INTO `sp_user_token` VALUES ('0ce628ce0a5e195f7835c3c65a82221fcb0fd4e5', 4, 1619145628, 1621737628);
INSERT INTO `sp_user_token` VALUES ('0e4ccd870be3f122d617669272d0ea01d4de629a', 3, 1618972060, 1621564060);
INSERT INTO `sp_user_token` VALUES ('0ea8f405381687c238d60ce7500ab469164b65f5', 3, 1618982686, 1621574686);
INSERT INTO `sp_user_token` VALUES ('0ed5d49d80923042634ce72bb34cff281ed040ad', 3, 1618902696, 1621494696);
INSERT INTO `sp_user_token` VALUES ('0f2ddae9e0d48a94cde6ee83fe73a4a91136d2b7', 17, 1620380558, 1622972558);
INSERT INTO `sp_user_token` VALUES ('0f2e82bd4034395df18bfc915a5323d1939595a9', 3, 1618899483, 1621491483);
INSERT INTO `sp_user_token` VALUES ('0f67ca16ea5b3aa14dd1e23a065f09bc3ddc08da', 5, 1619235755, 1621827755);
INSERT INTO `sp_user_token` VALUES ('0f869efdb000b2a6a22f68c343dc3f99d6632620', 4, 1620372852, 1622964852);
INSERT INTO `sp_user_token` VALUES ('0fddcdf548cbbb45ee728634ca4a642c0b70f589', 4, 1620264266, 1622856266);
INSERT INTO `sp_user_token` VALUES ('1004055180a88f9bda55d4c0e2b6dd07519d83d3', 3, 1620375356, 1622967356);
INSERT INTO `sp_user_token` VALUES ('10403184a295747a0388993672c9829355c1210b', 4, 1620269571, 1622861571);
INSERT INTO `sp_user_token` VALUES ('108b03b4be432c1bdfda744ac9f8f16b641ea34a', 6, 1620994593, 1623586593);
INSERT INTO `sp_user_token` VALUES ('10b36b4315dd7c230b86bb34575ce1845527429d', 32, 1621395060, 1623987060);
INSERT INTO `sp_user_token` VALUES ('116371c26240e56a4c82c52066af8a26146a6d14', 4, 1618985037, 1621577037);
INSERT INTO `sp_user_token` VALUES ('1191eee7f088c2516a88c8cc2585ca670570495c', 21, 1620610181, 1623202181);
INSERT INTO `sp_user_token` VALUES ('11dcd59fabb7ccda80a8f2020825e605124e4bee', 3, 1618902761, 1621494761);
INSERT INTO `sp_user_token` VALUES ('11e8db207be50a01d012a0eb9b5a1a8a0785a66c', 3, 1621305025, 1623897025);
INSERT INTO `sp_user_token` VALUES ('120c92598ba37cb289c5c9fca481c46602b11fdd', 4, 1620373922, 1622965922);
INSERT INTO `sp_user_token` VALUES ('1231ec886e68158a29d5e2eee1266278689bc1db', 4, 1620379884, 1622971884);
INSERT INTO `sp_user_token` VALUES ('13aa0b02229bf6125a2e747d38d2e53943d74b5a', 4, 1618985072, 1621577072);
INSERT INTO `sp_user_token` VALUES ('13d0e69a5d8b4681060050319cabedc8ef17dff6', 25, 1620956807, 1623548807);
INSERT INTO `sp_user_token` VALUES ('13f8a9a42ed06f933ba113c90e88cff1036b50a9', 1, 1618886441, 1621478441);
INSERT INTO `sp_user_token` VALUES ('1426c97d6069b71dcb4b8591c7ee148aceb25ded', 3, 1618970174, 1621562174);
INSERT INTO `sp_user_token` VALUES ('144a1cd6879bc7fbbd1171c9ad5e776d65b3c114', 4, 1619148948, 1621740948);
INSERT INTO `sp_user_token` VALUES ('144f43a3da10695dc3502572f60c6f31fb2be367', 4, 1620271631, 1622863631);
INSERT INTO `sp_user_token` VALUES ('14532efa0483fbd63da42af706a5bf3ff1f8ba64', 6, 1621303875, 1623895875);
INSERT INTO `sp_user_token` VALUES ('14bae1dba2b9b88d08bed902872929dfc58d07c4', 3, 1619094706, 1621686706);
INSERT INTO `sp_user_token` VALUES ('158717b8619a09276d1ad0e6b10221692f75ec24', 6, 1620957843, 1623549843);
INSERT INTO `sp_user_token` VALUES ('1606b28f189275271cd692ab83242a0e83a142b7', 7, 1620376900, 1622968900);
INSERT INTO `sp_user_token` VALUES ('1619e06e7b6ea0a0ac539a1f52a6bce836518ea2', 11, 1620372776, 1622964776);
INSERT INTO `sp_user_token` VALUES ('166992c53bbe55461a3a66bc475b8cc9cdc4f16f', 6, 1620609671, 1623201671);
INSERT INTO `sp_user_token` VALUES ('16d9737c91c67690b67fa2104859014710984ca0', 4, 1620265131, 1622857131);
INSERT INTO `sp_user_token` VALUES ('173e377deab4eea9d4e07c48d860b7641c3e06ab', 4, 1618990621, 1621582621);
INSERT INTO `sp_user_token` VALUES ('177f9ee1663c64089873ef349997c3ad16b08bbb', 17, 1620379920, 1622971920);
INSERT INTO `sp_user_token` VALUES ('183d2ce14efa208328f6591bd4ec5ac7abb861ce', 3, 1618994353, 1621586353);
INSERT INTO `sp_user_token` VALUES ('1893d792504f292b1c37dc6e993228b5c3c7a47f', 4, 1620042151, 1622634151);
INSERT INTO `sp_user_token` VALUES ('18b47aad1b21ac708eb9d31ace92432c12645283', 3, 1618971311, 1621563311);
INSERT INTO `sp_user_token` VALUES ('19492e80a3e297e0a768467f2826d147f8741074', 6, 1620554398, 1623146398);
INSERT INTO `sp_user_token` VALUES ('194aef285b817c0411e79c0b5300d84d7f56f023', 4, 1619578486, 1622170486);
INSERT INTO `sp_user_token` VALUES ('1954e639daf345859fe29acdcf979330a2504089', 4, 1619148308, 1621740308);
INSERT INTO `sp_user_token` VALUES ('1975d841f82e7c60c9f60db48bb6f7329d1ea7b1', 3, 1619094453, 1621686453);
INSERT INTO `sp_user_token` VALUES ('19790a64d6b136c317cb6c9abadef45d24e8a03c', 3, 1618971323, 1621563323);
INSERT INTO `sp_user_token` VALUES ('197e91e4f96bbeab975cc4c12f32c130d28aa212', 3, 1618905427, 1621497427);
INSERT INTO `sp_user_token` VALUES ('19865170b40850d7ef2bf65306abdaf3868f9397', 4, 1619159918, 1621751918);
INSERT INTO `sp_user_token` VALUES ('199bba7d02f065786b5d9d6a491035c7053d758c', 3, 1618904184, 1621496184);
INSERT INTO `sp_user_token` VALUES ('19ce60ba7505fe636b6b10c7e51e846a47b3e5cf', 13, 1620376163, 1622968163);
INSERT INTO `sp_user_token` VALUES ('1af54076dbb05c0fa487b2ea63c32547adde52a7', 4, 1620373685, 1622965685);
INSERT INTO `sp_user_token` VALUES ('1b20c2942232c657b6460d124722e84e85f88cd7', 3, 1618912117, 1621504117);
INSERT INTO `sp_user_token` VALUES ('1b3b43364ed8e032dcbbaa33a3c84600d5497bf2', 4, 1618985101, 1621577101);
INSERT INTO `sp_user_token` VALUES ('1bfd6e7a61d4f358a3f74208b9ed042bbf55d9d3', 3, 1619164826, 1621756826);
INSERT INTO `sp_user_token` VALUES ('1c51c20a5581477316c06d71b7c52aa4718b9389', 6, 1620378948, 1622970948);
INSERT INTO `sp_user_token` VALUES ('1c65ae9e8a37931dc9ce8e7523fd8e813021dadb', 4, 1620269575, 1622861575);
INSERT INTO `sp_user_token` VALUES ('1c93a78af85582665605d40b87ae43541efd3625', 3, 1618906808, 1621498808);
INSERT INTO `sp_user_token` VALUES ('1cda407f59b07a3d347556318bf2dcacb81fe1bb', 4, 1620371425, 1622963425);
INSERT INTO `sp_user_token` VALUES ('1cee75e21cf0ff721982ff3deeff647ed6ffb373', 3, 1619165662, 1621757662);
INSERT INTO `sp_user_token` VALUES ('1d6e0ad99f58f4fbf7e562938c4e613985cee6a8', 4, 1620381099, 1622973099);
INSERT INTO `sp_user_token` VALUES ('1d937eec2cc3bca138360cab7f5820173060beaa', 3, 1618976561, 1621568561);
INSERT INTO `sp_user_token` VALUES ('1dc0abfb323e1b925333e8a8d31f231999868231', 6, 1621394440, 1623986440);
INSERT INTO `sp_user_token` VALUES ('1e4e0e294fe05bfb1c3f80841e7c26e041fb17eb', 3, 1618906659, 1621498659);
INSERT INTO `sp_user_token` VALUES ('1e6a5de323b64d773ec02dae7cd262614d5c8d05', 15, 1620374265, 1622966265);
INSERT INTO `sp_user_token` VALUES ('1ea6875e881226095f2db5025ed53aaa0deede06', 3, 1619166024, 1621758024);
INSERT INTO `sp_user_token` VALUES ('1ee077c5281e967b530a325ab1ce1c63a345d516', 3, 1618972672, 1621564672);
INSERT INTO `sp_user_token` VALUES ('1fe4d8609659310de9ea1b86426b40dfb74cee48', 3, 1618982690, 1621574690);
INSERT INTO `sp_user_token` VALUES ('200ddc011efef49d25a0c248bc6dfeb9cc87ba85', 4, 1619586244, 1622178244);
INSERT INTO `sp_user_token` VALUES ('204932051a903fe3374b38a50ce950c2dc71b47d', 3, 1618972689, 1621564689);
INSERT INTO `sp_user_token` VALUES ('211f4fce6512d809e2c1d80209eb73fec896fb99', 6, 1620910865, 1623502865);
INSERT INTO `sp_user_token` VALUES ('2121c37432ec3b3bf32995087238cc6d6a6e39aa', 3, 1618968436, 1621560436);
INSERT INTO `sp_user_token` VALUES ('219a1d0cb7ff4b177e3ef66bb503f84139c74ebf', 4, 1620042398, 1622634398);
INSERT INTO `sp_user_token` VALUES ('21d64393754e0e8d86b5bcbf19f5e622cbc0bf5b', 6, 1621310739, 1623902739);
INSERT INTO `sp_user_token` VALUES ('21ecf0a905387d6e4916388e38cf16969f0ae271', 3, 1618969400, 1621561400);
INSERT INTO `sp_user_token` VALUES ('22083146c16ebd6b90a582fbf5dd1ae28effa089', 23, 1620784771, 1623376771);
INSERT INTO `sp_user_token` VALUES ('237c6df6c3bd3671681059aa31255cc0cf3570cf', 6, 1620784685, 1623376685);
INSERT INTO `sp_user_token` VALUES ('240b0d800678d321d93238ee56000320ceb1c881', 4, 1620375761, 1622967761);
INSERT INTO `sp_user_token` VALUES ('24899f7818ae940493def8cf14d00017cd2f5c84', 32, 1621395069, 1623987069);
INSERT INTO `sp_user_token` VALUES ('2510571f70990b2bda8452bf21f681b4a7548b47', 6, 1621340032, 1623932032);
INSERT INTO `sp_user_token` VALUES ('25108c859c9726c8d64624bc0ab9f31b96c4104a', 3, 1619165617, 1621757617);
INSERT INTO `sp_user_token` VALUES ('257f304c4a871eaa9182958b87f2f10b58691a1c', 4, 1619160545, 1621752545);
INSERT INTO `sp_user_token` VALUES ('258db87e687f620e9ab2cfa7747e45b2b0a98da5', 4, 1620042732, 1622634732);
INSERT INTO `sp_user_token` VALUES ('263976a40c718fccd47d4010bdac44a92bdf0a0f', 3, 1618902672, 1621494672);
INSERT INTO `sp_user_token` VALUES ('2639dee70708424d354ecd38e832e5d33f394d04', 5, 1619681753, 1622273753);
INSERT INTO `sp_user_token` VALUES ('26449a31228870787b155095265f27700204a08f', 3, 1619166778, 1621758778);
INSERT INTO `sp_user_token` VALUES ('269d91b9d13692e0fd9abb6d1d7209c22485fc63', 3, 1619164546, 1621756546);
INSERT INTO `sp_user_token` VALUES ('26cffc23a007bcd88f31fd174e941fce3788df6a', 3, 1618914199, 1621506199);
INSERT INTO `sp_user_token` VALUES ('27947ed9134e11ed3a44e9eb77bbf8213e1189f8', 5, 1619170025, 1621762025);
INSERT INTO `sp_user_token` VALUES ('283ecdb0ad54f1f01a1331ac4a7770af6b790dbc', 3, 1618897924, 1621489924);
INSERT INTO `sp_user_token` VALUES ('2846418e46f67709b8d3fcf17b945a3b56ad1799', 3, 1618971485, 1621563485);
INSERT INTO `sp_user_token` VALUES ('28956ea66ce69e6b6c57251d48569218d4e8fed4', 4, 1619161490, 1621753490);
INSERT INTO `sp_user_token` VALUES ('28da4a5fcbf52e8b5b7643aaeda89ce6e0f4f802', 4, 1620374964, 1622966964);
INSERT INTO `sp_user_token` VALUES ('2949db36aed372a80d084cedb74df0a86eceb68b', 3, 1618997156, 1621589156);
INSERT INTO `sp_user_token` VALUES ('295f56f8391250193ff5cef8c619aa5c612fc239', 3, 1618909077, 1621501077);
INSERT INTO `sp_user_token` VALUES ('29936d19ba6c95f5efd093775d285b3ddee21efd', 3, 1618912066, 1621504066);
INSERT INTO `sp_user_token` VALUES ('299ceb64cf055d3859889c0a6eafae0f318c96ca', 4, 1620269577, 1622861577);
INSERT INTO `sp_user_token` VALUES ('29aac0ee4cb5288af64c4dc764617ebd7879a958', 3, 1618970352, 1621562352);
INSERT INTO `sp_user_token` VALUES ('2a5ae524e9a153969b74801bba45a89fe63590ca', 3, 1618898629, 1621490629);
INSERT INTO `sp_user_token` VALUES ('2a935e0c2c7a7b96f2c285f816914658ec989de5', 3, 1618967656, 1621559656);
INSERT INTO `sp_user_token` VALUES ('2ba8a3ab6168262d6c299b254aef9a078da5069e', 11, 1620441307, 1623033307);
INSERT INTO `sp_user_token` VALUES ('2bdd2d3d654e4b8ed3d6225388995ee17537e327', 27, 1621049321, 1623641321);
INSERT INTO `sp_user_token` VALUES ('2c045f5f6f55a9b1089d7167fe51323ca06d0fcd', 4, 1619167679, 1621759679);
INSERT INTO `sp_user_token` VALUES ('2c0ae5be4f1a8f9b72c95f8cc0551ba2b84f3755', 3, 1618971033, 1621563033);
INSERT INTO `sp_user_token` VALUES ('2c55ee9468239108a855595e9182d9f281e5dbcd', 4, 1618991799, 1621583799);
INSERT INTO `sp_user_token` VALUES ('2c7a7667514e4a7497528eace723772973948aa8', 22, 1620619990, 1623211990);
INSERT INTO `sp_user_token` VALUES ('2cd7b12eec61c620a7d1e802a203051753530878', 3, 1618994234, 1621586234);
INSERT INTO `sp_user_token` VALUES ('2cf59376c8558d15313bffa247b753687941c7db', 3, 1619183155, 1621775155);
INSERT INTO `sp_user_token` VALUES ('2d472bf5ad9d409e0595e2e72e0353b247e36283', 5, 1619155787, 1621747787);
INSERT INTO `sp_user_token` VALUES ('2dfc78fd7041e31ecfa232eec0131384349e7183', 6, 1620097420, 1622689420);
INSERT INTO `sp_user_token` VALUES ('2e1e9c14ed6a84211da346cc02f59216e396dfe0', 4, 1619144674, 1621736674);
INSERT INTO `sp_user_token` VALUES ('2e9ace980e9dbb56374e14ef6078df142881ee40', 3, 1618996393, 1621588393);
INSERT INTO `sp_user_token` VALUES ('2fba413a72fcaf8448612252b50c930e981f7412', 4, 1618983099, 1621575099);
INSERT INTO `sp_user_token` VALUES ('2fc6496be3da567695b1a63c9a83db7648fc9b6b', 20, 1620554420, 1623146420);
INSERT INTO `sp_user_token` VALUES ('2fd7e23adf60da043d639842c3dd69f149221ef4', 4, 1619159273, 1621751273);
INSERT INTO `sp_user_token` VALUES ('2ffbb8bce12362f6501716cfef9f17aa04baccf2', 4, 1619583179, 1622175179);
INSERT INTO `sp_user_token` VALUES ('303749037e33433bc627afd81719ce631ac6090f', 29, 1621244441, 1623836441);
INSERT INTO `sp_user_token` VALUES ('309d63e5a2378ca60077862f3c4b62c5a033ac80', 3, 1621242333, 1623834333);
INSERT INTO `sp_user_token` VALUES ('30e43e46507485fd8cb1b668bf2bb2b137c1799f', 4, 1619230894, 1621822894);
INSERT INTO `sp_user_token` VALUES ('30fb1f61db7bb2c425a29f61715f4e0808774265', 4, 1618988460, 1621580460);
INSERT INTO `sp_user_token` VALUES ('31851c6f6b1ff220f6ce9f6715754400d90e2c95', 4, 1620265385, 1622857385);
INSERT INTO `sp_user_token` VALUES ('31fdc527f8cd421b3057456ab2261e06c16a003d', 3, 1618913917, 1621505917);
INSERT INTO `sp_user_token` VALUES ('32197a96acbc9dc968629cd90b7b29b27d9de2cd', 3, 1618901193, 1621493193);
INSERT INTO `sp_user_token` VALUES ('32e0fcdbf0e435d82742912f222697f7bd7cbaf2', 4, 1619160189, 1621752189);
INSERT INTO `sp_user_token` VALUES ('331016a388a104809f35fccbd54ef000d40b2a37', 6, 1620379147, 1622971147);
INSERT INTO `sp_user_token` VALUES ('331e154f6f183bea49d4db792fff80b483a0bdfe', 3, 1619165529, 1621757529);
INSERT INTO `sp_user_token` VALUES ('33a8fc29070144e245938bbe77803f38c070e68e', 6, 1620378932, 1622970932);
INSERT INTO `sp_user_token` VALUES ('3404008877a069dfd7a518b9b9c3e973bb8304e6', 6, 1621216576, 1623808576);
INSERT INTO `sp_user_token` VALUES ('34996403dc69478724041f12e3ca554f201b8d85', 4, 1619160210, 1621752210);
INSERT INTO `sp_user_token` VALUES ('34b0f1d9f6cb3b1adc593641affb6296713d63b4', 4, 1620272205, 1622864205);
INSERT INTO `sp_user_token` VALUES ('34b63a8b575ff7aef9bd07b0094687cd666d7e58', 31, 1621394470, 1623986470);
INSERT INTO `sp_user_token` VALUES ('35304d786145f46af69b1211194d97fa5102cb7f', 3, 1618992038, 1621584038);
INSERT INTO `sp_user_token` VALUES ('3627bf13de0f3816e1bed4ae8e371cb47ca97fe7', 6, 1619271113, 1621863113);
INSERT INTO `sp_user_token` VALUES ('3639878f408222e02e88045b05b9eb2166b2f39d', 5, 1620721031, 1623313031);
INSERT INTO `sp_user_token` VALUES ('3656ce8e3186330c7064a11bfce064bf62116ce5', 4, 1619144185, 1621736185);
INSERT INTO `sp_user_token` VALUES ('3673c67ba996647aeadc67b3afe266ef14fb0257', 4, 1618984094, 1621576094);
INSERT INTO `sp_user_token` VALUES ('36b85a5fe5c9f94721b346df006ae14ea7928140', 3, 1618897038, 1621489038);
INSERT INTO `sp_user_token` VALUES ('3744b331567dbae12a1587abb3f7a0f17eb45fd6', 3, 1618899825, 1621491825);
INSERT INTO `sp_user_token` VALUES ('375d694cd8f85747ba4d05d89e2db5df6b67d9d6', 4, 1620266813, 1622858813);
INSERT INTO `sp_user_token` VALUES ('380451b6dda39faecacacd081a95b9410047f60a', 27, 1621049282, 1623641282);
INSERT INTO `sp_user_token` VALUES ('380d58e18dafd45870085f0a82c8854f7885ae41', 5, 1619170031, 1621762031);
INSERT INTO `sp_user_token` VALUES ('388aacd2d38a11adc9a1749fcded20bc43ae30d8', 3, 1618904536, 1621496536);
INSERT INTO `sp_user_token` VALUES ('39437b4e827c2bf3264d28321c88bbdaba2396b1', 3, 1618968593, 1621560593);
INSERT INTO `sp_user_token` VALUES ('39e6f1b1ee799ac8ef8f8c6debc2a9eaa43d60cd', 4, 1620042853, 1622634853);
INSERT INTO `sp_user_token` VALUES ('3a01bc82692d6677c1f8522b93e648eb0ae2537b', 6, 1621157320, 1623749320);
INSERT INTO `sp_user_token` VALUES ('3a04eff51f7ce9b74457dc50853f34422e5e06a7', 31, 1621397689, 1623989689);
INSERT INTO `sp_user_token` VALUES ('3a310f9a3e985b0d2682f360c40f8295ced239ae', 3, 1618904646, 1621496646);
INSERT INTO `sp_user_token` VALUES ('3a6660f191b0edad7df3eb90ba1e80c6daadc42a', 3, 1618904567, 1621496567);
INSERT INTO `sp_user_token` VALUES ('3b1df7bbfacfc087fa8dff9010db905ff3784bc4', 3, 1618914296, 1621506296);
INSERT INTO `sp_user_token` VALUES ('3b92490be9f6cb8e70301cffc023b0e8449c07f1', 3, 1618908557, 1621500557);
INSERT INTO `sp_user_token` VALUES ('3bca0891869ce5fc4a208ef87e6168c90dbc3af1', 4, 1620375385, 1622967385);
INSERT INTO `sp_user_token` VALUES ('3c8a43cf63fa1f086c635d842e36e10fad1e34bc', 5, 1619157448, 1621749448);
INSERT INTO `sp_user_token` VALUES ('3d32932cf96efeba05a1e41efbb7afb1bce0ee49', 3, 1618969434, 1621561434);
INSERT INTO `sp_user_token` VALUES ('3d62ae6befe4f5b281356c6f364821b071596343', 4, 1620042742, 1622634742);
INSERT INTO `sp_user_token` VALUES ('3d67301f7d8d26bc0008f373abfb6ab13962520b', 6, 1621304978, 1623896978);
INSERT INTO `sp_user_token` VALUES ('3e078ac854eb76b9f36ec61065568ab29941919e', 3, 1619143145, 1621735145);
INSERT INTO `sp_user_token` VALUES ('3e0db553bbc8bdabb8c9ab112fc79768dcbef7bf', 4, 1620371401, 1622963401);
INSERT INTO `sp_user_token` VALUES ('3e234f2f7e29c9be980a43d5508ee120885fac69', 3, 1618900816, 1621492816);
INSERT INTO `sp_user_token` VALUES ('3f7a36c39d33f226d5f3f6f03b8ba8840db4fd36', 6, 1619170691, 1621762691);
INSERT INTO `sp_user_token` VALUES ('3fa7579a7d444d856a205e683f9100afa209e8b0', 3, 1620536984, 1623128984);
INSERT INTO `sp_user_token` VALUES ('3fb7297ceac2ef14579a84daa969a1626d3937a7', 3, 1619162384, 1621754384);
INSERT INTO `sp_user_token` VALUES ('3fbd353127471625ef2e6e15c40464162353d2c7', 4, 1620371539, 1622963539);
INSERT INTO `sp_user_token` VALUES ('3fd004723976fd2aff80595a1a4c731846792ccc', 20, 1620554406, 1623146406);
INSERT INTO `sp_user_token` VALUES ('400310a9d0c1e1ef6b67ae61ae4ddf6c40893877', 6, 1620096017, 1622688017);
INSERT INTO `sp_user_token` VALUES ('40524953ef8d5ac3b9aa39af74000943f677ef42', 4, 1620271655, 1622863655);
INSERT INTO `sp_user_token` VALUES ('407bad6fd8d59444fa347e34c5cfcd7d2af3b454', 13, 1620374256, 1622966256);
INSERT INTO `sp_user_token` VALUES ('40bf0c4863e25f7431a089428ca25f5afd127045', 13, 1620374887, 1622966887);
INSERT INTO `sp_user_token` VALUES ('40e0d923a722cd2e3221e9936789f574a907023b', 4, 1619168099, 1621760099);
INSERT INTO `sp_user_token` VALUES ('410029f8e2a05f86231542c10d85b9d4ee89696f', 3, 1619094733, 1621686733);
INSERT INTO `sp_user_token` VALUES ('41829be0480b03715796c4fd4e76f71491009fc1', 3, 1618899127, 1621491127);
INSERT INTO `sp_user_token` VALUES ('4190aa9d54e64aa374e903d8fe9b4278f020424c', 8, 1620375386, 1622967386);
INSERT INTO `sp_user_token` VALUES ('41a49c03874339eafa2f16fec5734bd6d536b738', 3, 1618972187, 1621564187);
INSERT INTO `sp_user_token` VALUES ('41c1ba2dde989e42e3d82b0388cf95e304ea1f09', 29, 1621244419, 1623836419);
INSERT INTO `sp_user_token` VALUES ('41e9f4aed91f45eb1c6009a22931bd311207dca2', 3, 1618898297, 1621490297);
INSERT INTO `sp_user_token` VALUES ('42082f0546b53110553806a14b946efdb009b461', 4, 1619146820, 1621738820);
INSERT INTO `sp_user_token` VALUES ('42350feb97377636f17ef0ccde82c744293c94cd', 3, 1618904223, 1621496223);
INSERT INTO `sp_user_token` VALUES ('423f52479bc8f08dcd862b94cfe7ad0afff981f2', 3, 1619166246, 1621758246);
INSERT INTO `sp_user_token` VALUES ('4397ced2bf7d5410ee126c856119f5586e63ad12', 18, 1620383992, 1622975992);
INSERT INTO `sp_user_token` VALUES ('439c8e854517b513afec774247f7852687cbb47e', 4, 1619146419, 1621738419);
INSERT INTO `sp_user_token` VALUES ('43cfb89650ea90e910b07b9967264b5f8ab5dfe8', 4, 1618991863, 1621583863);
INSERT INTO `sp_user_token` VALUES ('44427fb2a6ea35a051d76739efb221b1dd5b1de5', 4, 1620372672, 1622964672);
INSERT INTO `sp_user_token` VALUES ('44fd958e9d8a41043992d0d26b5d5c0297e1b089', 3, 1618969440, 1621561440);
INSERT INTO `sp_user_token` VALUES ('453bba1bd8094e3a7e5068c8f7276b9a6230c1bf', 24, 1620865220, 1623457220);
INSERT INTO `sp_user_token` VALUES ('4549d84c6c15b1cc707765b5f82a440989bd65a0', 3, 1618981750, 1621573750);
INSERT INTO `sp_user_token` VALUES ('46e3e3b7f33d34b77b03dc6cca817fc7fd4beea6', 4, 1620374931, 1622966931);
INSERT INTO `sp_user_token` VALUES ('4734bb1d19eaf3f64080c9041128fc18c4db4fb9', 3, 1619162830, 1621754830);
INSERT INTO `sp_user_token` VALUES ('4738b9553d08adcac68bf028169a6f4871b3c528', 3, 1618898650, 1621490650);
INSERT INTO `sp_user_token` VALUES ('47befe23269c14dcf51fdf060e517fc5cf2ac022', 6, 1620609941, 1623201941);
INSERT INTO `sp_user_token` VALUES ('47fcf78d65802385dd5554dcf8cb16f26c86b874', 4, 1618991767, 1621583767);
INSERT INTO `sp_user_token` VALUES ('4910735e5603bc29cdc07c4282eeb8d68e59189f', 6, 1620657609, 1623249609);
INSERT INTO `sp_user_token` VALUES ('4919ffb9adc14190cc16965624dbfab269b1ae08', 6, 1621394415, 1623986415);
INSERT INTO `sp_user_token` VALUES ('497e9b2c48fe0488af1473278cdac659ed1e7d13', 3, 1618905971, 1621497971);
INSERT INTO `sp_user_token` VALUES ('4a15556db0b1e4609bb1dce8398ccc7716517e13', 6, 1620955318, 1623547318);
INSERT INTO `sp_user_token` VALUES ('4a1f3730fa1bcfae27f475c2cc67311c8251dd69', 6, 1621242764, 1623834764);
INSERT INTO `sp_user_token` VALUES ('4a5ea1dd306d94df35c80243391b46cc9c1be91b', 6, 1619271147, 1621863147);
INSERT INTO `sp_user_token` VALUES ('4aee988c4bfba35964507197f63af05292c52536', 6, 1620777297, 1623369297);
INSERT INTO `sp_user_token` VALUES ('4b21143442fd7fa8294a4c806bac7055756050b7', 32, 1621395097, 1623987097);
INSERT INTO `sp_user_token` VALUES ('4b33825a00d17bc05b6c436d52520ae2fc2ee2f6', 5, 1620720937, 1623312937);
INSERT INTO `sp_user_token` VALUES ('4b3a852e4cc2662e128995cc0f507a0fa72bc922', 10, 1620372597, 1622964597);
INSERT INTO `sp_user_token` VALUES ('4bd5663850e94a3003db267d8769858254a91d1c', 4, 1619143830, 1621735830);
INSERT INTO `sp_user_token` VALUES ('4be70be9a748d719c9ca14d32b51b2ab825a3b78', 3, 1620373455, 1622965455);
INSERT INTO `sp_user_token` VALUES ('4c88ab3e594640fca812031979a86451fb57d11c', 5, 1621315978, 1623907978);
INSERT INTO `sp_user_token` VALUES ('4d4405b92f825244e7bbae8dabe2c369fbb6fca0', 3, 1618986414, 1621578414);
INSERT INTO `sp_user_token` VALUES ('4d8e546c3551926bea9c141b2a9ee9f3a1c33a6c', 4, 1618983634, 1621575634);
INSERT INTO `sp_user_token` VALUES ('4da6059ddd41d1d645c823d989225a111804276d', 3, 1619094683, 1621686683);
INSERT INTO `sp_user_token` VALUES ('4e02891463df94ade14d30c83e523c02ac02169a', 4, 1618991676, 1621583676);
INSERT INTO `sp_user_token` VALUES ('4e93723dfbc283ce2ac2e3f06055f28c8ea8bec2', 3, 1619586136, 1622178136);
INSERT INTO `sp_user_token` VALUES ('4ea0b371b8ccf667513d1991d5779543cef6a6bb', 3, 1618898364, 1621490364);
INSERT INTO `sp_user_token` VALUES ('4efaecaf2a552e25e6c7b8762f587ef6069929bc', 4, 1620380611, 1622972611);
INSERT INTO `sp_user_token` VALUES ('4f56c1fc2dae97005b5a322ffa35859083f04075', 3, 1621304971, 1623896971);
INSERT INTO `sp_user_token` VALUES ('4f70bb349481027d2959efbd422856e242cb74aa', 6, 1619169972, 1621761972);
INSERT INTO `sp_user_token` VALUES ('4fe764cc09dcc25b0f1c7553283998dd49bef60e', 4, 1619168374, 1621760374);
INSERT INTO `sp_user_token` VALUES ('4ff06de3078080b2c8cf8f1dd11a59f213540ac2', 6, 1621251996, 1623843996);
INSERT INTO `sp_user_token` VALUES ('4ffd43fb02cb6a669af688b2492be15759e54622', 3, 1619162931, 1621754931);
INSERT INTO `sp_user_token` VALUES ('50109805e852419421c9edb59d4731af43989d34', 3, 1618907347, 1621499347);
INSERT INTO `sp_user_token` VALUES ('507237751201494ca43e7b5304e346a9238c9278', 3, 1618992060, 1621584060);
INSERT INTO `sp_user_token` VALUES ('51b5f46452be89542846a6d9b04d0de1d344f678', 4, 1619168371, 1621760371);
INSERT INTO `sp_user_token` VALUES ('524ba79429954271abe0349f304ae74bb00348a3', 5, 1619167945, 1621759945);
INSERT INTO `sp_user_token` VALUES ('528948fd8937081806dc7de5c96d08e233983d23', 4, 1620271645, 1622863645);
INSERT INTO `sp_user_token` VALUES ('5319844afc0590887c0594e48d1378087c3df085', 3, 1618993780, 1621585780);
INSERT INTO `sp_user_token` VALUES ('533bb3e5d1620cf3d66be042c1b462403311dae4', 4, 1619057403, 1621649403);
INSERT INTO `sp_user_token` VALUES ('53593d40e0b96894091927fb9f84663def3d318d', 4, 1619160603, 1621752603);
INSERT INTO `sp_user_token` VALUES ('5386317be0bcb000646da692f5280eed3561fb35', 4, 1620377047, 1622969047);
INSERT INTO `sp_user_token` VALUES ('5410aac5fe17757f6a28a2af999606740d0b2b75', 3, 1619155501, 1621747501);
INSERT INTO `sp_user_token` VALUES ('5474c736eaa1d7c9d085d8c0f12eddbd0a8c77ed', 6, 1620371731, 1622963731);
INSERT INTO `sp_user_token` VALUES ('5527fa54f4e7a84b93049baf8a03c713b974ad91', 3, 1618898682, 1621490682);
INSERT INTO `sp_user_token` VALUES ('55377267b018b8552a5dd8a57c643ea65543afb0', 3, 1618901250, 1621493250);
INSERT INTO `sp_user_token` VALUES ('558d0504f84a9d0729c90103f4fab15083577bad', 3, 1618969233, 1621561233);
INSERT INTO `sp_user_token` VALUES ('55db27a4acd14bc0c4acf94291590596c9fe7246', 4, 1619149056, 1621741056);
INSERT INTO `sp_user_token` VALUES ('55f12deeccd4a406e2dc5d0a2239f72517490af1', 3, 1618915924, 1621507924);
INSERT INTO `sp_user_token` VALUES ('562d7df809b3ce7f566d661b5968fd55af29a920', 3, 1619165218, 1621757218);
INSERT INTO `sp_user_token` VALUES ('56369709ef6085ff446e27411d4a69dfaefe80be', 3, 1618972255, 1621564255);
INSERT INTO `sp_user_token` VALUES ('5656ab5e02460be2c363c0e2a63214755a029c15', 22, 1620622719, 1623214719);
INSERT INTO `sp_user_token` VALUES ('56fe1d813ac650ce5643dd361b08b31e39b5d1aa', 3, 1618973526, 1621565526);
INSERT INTO `sp_user_token` VALUES ('5740c1a558b5cbcfc576f0ce07c43cec92892485', 3, 1619162827, 1621754827);
INSERT INTO `sp_user_token` VALUES ('57949131050e00307cee02b6c35e1d4477aadeaf', 3, 1618898511, 1621490511);
INSERT INTO `sp_user_token` VALUES ('57aaa94c1024267eb9955338ef4e6c38c5c6c1d2', 4, 1620375587, 1622967587);
INSERT INTO `sp_user_token` VALUES ('57f71d2fa7cbc629d4d5b3fdcc9001a202da2dde', 3, 1619166158, 1621758158);
INSERT INTO `sp_user_token` VALUES ('582792bd200dc71b6cb288e96b8412c4b71bafe5', 3, 1618897555, 1621489555);
INSERT INTO `sp_user_token` VALUES ('5903ee8aad3ff7a41975417a7b56b5684692a145', 4, 1618982813, 1621574813);
INSERT INTO `sp_user_token` VALUES ('598b3d7b16bee383ae290ad38dc2a5444f001d9f', 3, 1619162797, 1621754797);
INSERT INTO `sp_user_token` VALUES ('5a37d7c2810afa9bcbdceb179b51255605ec8c22', 3, 1619166911, 1621758911);
INSERT INTO `sp_user_token` VALUES ('5aa1b48175139910c2f957489ee3d50a9eb0bc47', 3, 1618903434, 1621495434);
INSERT INTO `sp_user_token` VALUES ('5b66e94f51d0934a7324e50c49d5e015b6fa5eea', 4, 1618990362, 1621582362);
INSERT INTO `sp_user_token` VALUES ('5b68e181712527747c0797945d5f45e65b8f4811', 3, 1618977034, 1621569034);
INSERT INTO `sp_user_token` VALUES ('5c518ea092393a2dbc4e76c9e298c6b7077d6f34', 4, 1618990335, 1621582335);
INSERT INTO `sp_user_token` VALUES ('5ca3f9b59b7336d456e28aa5f85b0bd8ba37ecb0', 4, 1619158427, 1621750427);
INSERT INTO `sp_user_token` VALUES ('5cc90da9fd291aee3441ef1b612cf73599c0cf37', 6, 1620660106, 1623252106);
INSERT INTO `sp_user_token` VALUES ('5cf3a454e58355d043705b771a7719bf1adc9b6f', 3, 1619094716, 1621686716);
INSERT INTO `sp_user_token` VALUES ('5cf9a6204e6fcfed8c27c45dc21023d0cb0350bd', 3, 1618972409, 1621564409);
INSERT INTO `sp_user_token` VALUES ('5d22535dc913d7d344c779373e601b3fe04297aa', 4, 1619161425, 1621753425);
INSERT INTO `sp_user_token` VALUES ('5d5e557b18e9268a35d0657be2c882d8aba93dac', 4, 1619587264, 1622179264);
INSERT INTO `sp_user_token` VALUES ('5de08002e8da07a09e5c369389702c11fffa08cd', 4, 1620381162, 1622973162);
INSERT INTO `sp_user_token` VALUES ('5e3c1727a739957c996f06a901b183424fb00171', 6, 1621242757, 1623834757);
INSERT INTO `sp_user_token` VALUES ('5f71687193a3e793786eae31e1b5d6bba1245c6b', 3, 1619164939, 1621756939);
INSERT INTO `sp_user_token` VALUES ('61a1d0a3ac4132ad4e7410bf7ee23cd5ca5eefee', 3, 1618906366, 1621498366);
INSERT INTO `sp_user_token` VALUES ('61cdbf0d88e6b550d33c9296bf7befad924f9433', 3, 1618976564, 1621568564);
INSERT INTO `sp_user_token` VALUES ('6285b3b3abe7ee2954ffdd55f03df9ca5df490b0', 6, 1620910198, 1623502198);
INSERT INTO `sp_user_token` VALUES ('62893c101f8bc27c7c2b31a148eaff27456c83db', 4, 1620375668, 1622967668);
INSERT INTO `sp_user_token` VALUES ('628f0ee5e75977e35487d6c8fa3c408fb9c17531', 3, 1618899543, 1621491543);
INSERT INTO `sp_user_token` VALUES ('62df4f7586eda5f9fb1fe5631df5c1f3ecf79227', 3, 1618969602, 1621561602);
INSERT INTO `sp_user_token` VALUES ('6320db164225e3e22623bade88a45bd75cc7b2b2', 4, 1620375179, 1622967179);
INSERT INTO `sp_user_token` VALUES ('6403ec454c4411df112e93547c3acbe4f518dba3', 6, 1619395776, 1621987776);
INSERT INTO `sp_user_token` VALUES ('6475707ec7a7485e8d82d56ba015ba12b5bbf672', 1, 1618896140, 1621488140);
INSERT INTO `sp_user_token` VALUES ('64bd0101a7cf4d42bf998c52f12d67305d9daa2c', 3, 1618905380, 1621497380);
INSERT INTO `sp_user_token` VALUES ('64c0b161995ebfa8ed74eb1026476ba7695e6f3f', 6, 1620376903, 1622968903);
INSERT INTO `sp_user_token` VALUES ('6598f2ed1848ee60948545c3573f3b5cef817255', 3, 1618903590, 1621495590);
INSERT INTO `sp_user_token` VALUES ('65b060641d56bb9c14d76af73f0e948dc30e1d93', 4, 1619587283, 1622179283);
INSERT INTO `sp_user_token` VALUES ('65e60f64589d9dda4bb45059868e370e9ddd9de0', 30, 1621305138, 1623897138);
INSERT INTO `sp_user_token` VALUES ('660aef426bedad7013c6ecaa679a719721277b31', 6, 1621049267, 1623641267);
INSERT INTO `sp_user_token` VALUES ('662dd63039ed5069cb2544e42f05a85e199c930b', 3, 1618904811, 1621496811);
INSERT INTO `sp_user_token` VALUES ('66838e59e32dce54d209eba44a716544e3718888', 3, 1618993278, 1621585278);
INSERT INTO `sp_user_token` VALUES ('6695f0b21affea8a54c90b5b4edd1f9c7b626281', 4, 1620042714, 1622634714);
INSERT INTO `sp_user_token` VALUES ('66e34bf33fdf91b67484b81436af1c3d7a4cd337', 3, 1618902823, 1621494823);
INSERT INTO `sp_user_token` VALUES ('6743b96a45445a791c89117ced3a2acd1f66a6a1', 4, 1619154865, 1621746865);
INSERT INTO `sp_user_token` VALUES ('6745071a33a434a1e0293af152ffe75983a6d862', 3, 1618974006, 1621566006);
INSERT INTO `sp_user_token` VALUES ('678a586153fa3f714ab03a75ad674159e0065e17', 6, 1620609676, 1623201676);
INSERT INTO `sp_user_token` VALUES ('679639d1427d469fac2e259d4cb3b5865a71bd35', 6, 1621251980, 1623843980);
INSERT INTO `sp_user_token` VALUES ('67d2a1c3ab49ad2bbcd3fadb37a49788626b050a', 3, 1619164797, 1621756797);
INSERT INTO `sp_user_token` VALUES ('67e66d0bb16fa3a6eca162b0f2b7087a5eae60d5', 4, 1618987642, 1621579642);
INSERT INTO `sp_user_token` VALUES ('68b6f84a6b6988b5a6675d1f74ebefb57f7b4efc', 3, 1618911642, 1621503642);
INSERT INTO `sp_user_token` VALUES ('68d14cb2f6eca0acf04f27d6c9cea4f532f1ec12', 3, 1619164651, 1621756651);
INSERT INTO `sp_user_token` VALUES ('690625c012c9f0d72c07756f42bf465aad9ecc81', 5, 1619681804, 1622273804);
INSERT INTO `sp_user_token` VALUES ('690cd1638aa72d163bcd9f8cf1ea2401d9bc707e', 3, 1618912184, 1621504184);
INSERT INTO `sp_user_token` VALUES ('696f8ddde76d76cc35a0481f7f4593fdd5e1c322', 4, 1620375393, 1622967393);
INSERT INTO `sp_user_token` VALUES ('6a1bf9ba4e96124d69d1354aa8237af4759c4097', 4, 1618990176, 1621582176);
INSERT INTO `sp_user_token` VALUES ('6b54bddd0c9677626ff2392546b8014386d31491', 3, 1618974640, 1621566640);
INSERT INTO `sp_user_token` VALUES ('6b8ad398668440275271574d1de4dc3a9dabc1e8', 3, 1618997246, 1621589246);
INSERT INTO `sp_user_token` VALUES ('6c4b9d9e4c14fb5702e7bf317ca2a7716756887b', 4, 1620042853, 1622634853);
INSERT INTO `sp_user_token` VALUES ('6c70481e50949beff95774a5bb75b83f64e226fe', 6, 1620785088, 1623377088);
INSERT INTO `sp_user_token` VALUES ('6c8cf6adbb15584995fa8d5eb5063682bbaafe74', 4, 1620272211, 1622864211);
INSERT INTO `sp_user_token` VALUES ('6d58408f36d80ca88d44a468535f89d9f340b4d4', 6, 1620554379, 1623146379);
INSERT INTO `sp_user_token` VALUES ('6dec62c288e2b46284bccfddd9343bffa8dfa834', 11, 1620372860, 1622964860);
INSERT INTO `sp_user_token` VALUES ('6e33bcf673fc5105bb01d677cda7d139d0db33d8', 18, 1620383945, 1622975945);
INSERT INTO `sp_user_token` VALUES ('6e5811c5ae07a348adc99475de4b79cef1acc9bf', 6, 1620994507, 1623586507);
INSERT INTO `sp_user_token` VALUES ('6e6ab6f8b4c5aa9eeeceff64fc038d8e827b3592', 3, 1621307223, 1623899223);
INSERT INTO `sp_user_token` VALUES ('6e8c1057d7fa3d6bfb829c3795efb11ce66b37d2', 3, 1620265457, 1622857457);
INSERT INTO `sp_user_token` VALUES ('6eb35a37e39dc014eaddfd3f5bbd52019acfba01', 3, 1618909093, 1621501093);
INSERT INTO `sp_user_token` VALUES ('6ed6375710c9a24ad1e5fffc18fdb95ad734d706', 4, 1620380208, 1622972208);
INSERT INTO `sp_user_token` VALUES ('6ee8dc08da4c8c55a5d7702c1f70f60d821d0290', 4, 1618982934, 1621574934);
INSERT INTO `sp_user_token` VALUES ('6f46a290023c746d057d1e6ab80afd56dfde00a3', 3, 1619162749, 1621754749);
INSERT INTO `sp_user_token` VALUES ('6fb58eccbe6d58bbc9d34ccc279aa2cb025c6467', 3, 1618899424, 1621491424);
INSERT INTO `sp_user_token` VALUES ('6ffd10d34585cdd62d1d5a7fc2706f29cad492a6', 4, 1618988302, 1621580302);
INSERT INTO `sp_user_token` VALUES ('705a31eb50e9a583674c2c67f62e40bbd008bb11', 3, 1618897602, 1621489602);
INSERT INTO `sp_user_token` VALUES ('7078214f37e57e2035d19abbba74e5bf7e3066a7', 6, 1620609683, 1623201683);
INSERT INTO `sp_user_token` VALUES ('7080edf41f78a8bbc50cd866a374064c2d9480b8', 5, 1619155774, 1621747774);
INSERT INTO `sp_user_token` VALUES ('7093dc5b0585f4d45db73ed0fce17a3b8aea40d0', 4, 1619155021, 1621747021);
INSERT INTO `sp_user_token` VALUES ('7224ebec9b168313d2f508e60933cdca65b1bf6b', 3, 1618912022, 1621504022);
INSERT INTO `sp_user_token` VALUES ('72457dd059e6c8d3a1f5c914c1009ca960f57e17', 4, 1618983197, 1621575197);
INSERT INTO `sp_user_token` VALUES ('72ef1ba993ff9fdbe01b10db94266ee3e92777ca', 3, 1618903563, 1621495563);
INSERT INTO `sp_user_token` VALUES ('731d6859a90aa67845fe2304668a171616bfc64e', 4, 1618990911, 1621582911);
INSERT INTO `sp_user_token` VALUES ('73379abbfc64fda382286686397c075eaff22789', 3, 1620374209, 1622966209);
INSERT INTO `sp_user_token` VALUES ('735082139ab078d17ac4c58791d5201a017b17b2', 4, 1620380589, 1622972589);
INSERT INTO `sp_user_token` VALUES ('73b4fb13f7fcf31a01d23bf2607767c130131dc2', 6, 1620378936, 1622970936);
INSERT INTO `sp_user_token` VALUES ('74613754c411195a6b0b629e5ea868cae5ae3ff1', 7, 1620371715, 1622963715);
INSERT INTO `sp_user_token` VALUES ('74620221b6652864fa6d3edecf5d2098acd35827', 9, 1620373611, 1622965611);
INSERT INTO `sp_user_token` VALUES ('749b1d0491b2184a47afd3377f1795f86300bb82', 3, 1618991970, 1621583970);
INSERT INTO `sp_user_token` VALUES ('74ae81526f3e8c17a7753e01dbad4287460f506b', 4, 1620372682, 1622964682);
INSERT INTO `sp_user_token` VALUES ('74e317b0fe451a8adadd9d6bce511b88a2a00fc3', 3, 1619163917, 1621755917);
INSERT INTO `sp_user_token` VALUES ('754e279cebec9bcf6731eecf5d3499d40d8001a3', 4, 1619158945, 1621750945);
INSERT INTO `sp_user_token` VALUES ('756361791296efa2267f79c0ebac28c388e45b72', 3, 1619166934, 1621758934);
INSERT INTO `sp_user_token` VALUES ('75bd5be2823b57f33a12fd6083282145828dd737', 3, 1619165705, 1621757705);
INSERT INTO `sp_user_token` VALUES ('75fc44e99977a9ea121aef3bbca2be67efbc17ba', 3, 1619167192, 1621759192);
INSERT INTO `sp_user_token` VALUES ('76246343df37029d5237a54bd1dda790d364e6f9', 4, 1619145610, 1621737610);
INSERT INTO `sp_user_token` VALUES ('762682414a9be1e81ed0fcc7e0dc32cffbef8874', 3, 1618905912, 1621497912);
INSERT INTO `sp_user_token` VALUES ('76719d7c280c248810d3cddfe7a5c56a3731c2ea', 6, 1621340020, 1623932020);
INSERT INTO `sp_user_token` VALUES ('76868cc734b59eae3fc902e72ff9d7dcf76d40db', 32, 1621398530, 1623990530);
INSERT INTO `sp_user_token` VALUES ('768a146999bb5667d0f8e01cfcba8d09593c6032', 7, 1620371755, 1622963755);
INSERT INTO `sp_user_token` VALUES ('76abddb1b67a6a9b46052424ecababe03998fce7', 3, 1618971786, 1621563786);
INSERT INTO `sp_user_token` VALUES ('76c2d24e1499699d01227fac13dc560e3bdf7f08', 3, 1618972824, 1621564824);
INSERT INTO `sp_user_token` VALUES ('76f3066ffd30d6bedfd5c1c702262c01db8f99e1', 3, 1618970375, 1621562375);
INSERT INTO `sp_user_token` VALUES ('76f5f4b3048cce48e04c21a1cf302bb40e0a29c1', 13, 1620376172, 1622968172);
INSERT INTO `sp_user_token` VALUES ('7795658d08ebcb4c643e80702d30b8e1982a3c4e', 21, 1620619123, 1623211123);
INSERT INTO `sp_user_token` VALUES ('78142b67204f7adba174602b1ce601d21ecd0fb0', 3, 1618994305, 1621586305);
INSERT INTO `sp_user_token` VALUES ('788be7a0292640f5369ca01986bed4e5d2cc0951', 3, 1619586169, 1622178169);
INSERT INTO `sp_user_token` VALUES ('78fdcebf8b7442f08053c3abbe23ebb60c485e52', 4, 1619145658, 1621737658);
INSERT INTO `sp_user_token` VALUES ('792cacc46c45f6857962e07bf4d0eac9a3509ab4', 4, 1618991545, 1621583545);
INSERT INTO `sp_user_token` VALUES ('794353463b5420d5405e92882f8167c65d01145d', 6, 1620168614, 1622760614);
INSERT INTO `sp_user_token` VALUES ('795d82fcdad6ab8505e7c5253160cd7cfa73044e', 3, 1618911480, 1621503480);
INSERT INTO `sp_user_token` VALUES ('79613b89a8e376a4c565d8f59803396ce02ae8c8', 5, 1619170049, 1621762049);
INSERT INTO `sp_user_token` VALUES ('79dcc304c2f83e713fa7f3464fc1eba12f318ef6', 4, 1619578454, 1622170454);
INSERT INTO `sp_user_token` VALUES ('79f3575d569c4aae38e1c62d02a62900004cb15e', 3, 1618896601, 1621488601);
INSERT INTO `sp_user_token` VALUES ('7a0b196b368534025661eba3d27f34587ce6f7a3', 30, 1621305120, 1623897120);
INSERT INTO `sp_user_token` VALUES ('7a1d31703b0d77bfc023e99ed5e03342785e8c4a', 4, 1619233032, 1621825032);
INSERT INTO `sp_user_token` VALUES ('7a48940f910710d466d63279ba7ddcf622415c5f', 3, 1618971694, 1621563694);
INSERT INTO `sp_user_token` VALUES ('7a93768875afa621037964e183eca163594eb9b8', 3, 1619094778, 1621686778);
INSERT INTO `sp_user_token` VALUES ('7b4b79cfaa71d79648f0a61c5a8ad4a2151d49b9', 3, 1619165223, 1621757223);
INSERT INTO `sp_user_token` VALUES ('7bc54acd1f30a062e70d97c4e85784e8edc3fa83', 4, 1618983246, 1621575246);
INSERT INTO `sp_user_token` VALUES ('7c6649ef50844d0fb7e29b9c1df9fdb86194e653', 4, 1618990647, 1621582647);
INSERT INTO `sp_user_token` VALUES ('7d25dfdb14bf4ca555f57c8652c51cf26acfbf37', 20, 1620554425, 1623146425);
INSERT INTO `sp_user_token` VALUES ('7d37d7246800eea2d7db8d245c4fe00ce6a0a3f0', 5, 1619168055, 1621760055);
INSERT INTO `sp_user_token` VALUES ('7dc338d06be3a2afe007c6c7876ddc0abbd26a8c', 4, 1619145682, 1621737682);
INSERT INTO `sp_user_token` VALUES ('7e11721a615e984a7bb918628578f9beb8eb9720', 31, 1621394483, 1623986483);
INSERT INTO `sp_user_token` VALUES ('7ed05b79ac54b325e686afb9111ff0516983a30f', 9, 1620372448, 1622964448);
INSERT INTO `sp_user_token` VALUES ('7ed1121ddbea87e362a7082201505ff039907870', 6, 1620370923, 1622962923);
INSERT INTO `sp_user_token` VALUES ('7fda3156037744bb9b572498a70109415e406879', 3, 1619163711, 1621755711);
INSERT INTO `sp_user_token` VALUES ('800e7801a52eea75d821580333116e9572b31441', 3, 1618968722, 1621560722);
INSERT INTO `sp_user_token` VALUES ('80682d56cd4c3c422ed81f2fe7ae918afcfa0786', 3, 1620375315, 1622967315);
INSERT INTO `sp_user_token` VALUES ('8071a733ab19e2749aade800eb3e3521f3fe2b6d', 4, 1620264267, 1622856267);
INSERT INTO `sp_user_token` VALUES ('809264f271663ed3d1b9e8ce3397630d5e18d883', 3, 1619057358, 1621649358);
INSERT INTO `sp_user_token` VALUES ('80975aead0aff77ad519c01abf5a6274588afeaa', 3, 1618912031, 1621504031);
INSERT INTO `sp_user_token` VALUES ('80c532c2832a166c9a1bf462bc21c58437f6a234', 7, 1620379904, 1622971904);
INSERT INTO `sp_user_token` VALUES ('80fd91fc5cb00d179e056a3d1aa3b8c367df9487', 6, 1620657613, 1623249613);
INSERT INTO `sp_user_token` VALUES ('80febc3a11098274f60ff8c7d8e4f7cf06a54f56', 3, 1619162769, 1621754769);
INSERT INTO `sp_user_token` VALUES ('81b34c10919335c2af1e4ff1b5b25481cf64d8db', 3, 1619166195, 1621758195);
INSERT INTO `sp_user_token` VALUES ('81c5145f65d7c9189a330269da9ce4968d930f19', 4, 1619144151, 1621736151);
INSERT INTO `sp_user_token` VALUES ('81c734025713669bc1464024ee69636847d24e5b', 4, 1619167505, 1621759505);
INSERT INTO `sp_user_token` VALUES ('8234c10bcb280a11c96d2a4f1d664d4cb7c31354', 4, 1620372847, 1622964847);
INSERT INTO `sp_user_token` VALUES ('82642d0b61f62fa6e9d6b5ba690eae66bb77c5b3', 3, 1618912165, 1621504165);
INSERT INTO `sp_user_token` VALUES ('83741e673bc68ee86a5dae129aa7ab39d4a60e12', 3, 1619142932, 1621734932);
INSERT INTO `sp_user_token` VALUES ('8429bd02324841d7d30ce60ca49c0fce0959e1fd', 4, 1618990141, 1621582141);
INSERT INTO `sp_user_token` VALUES ('84443e82dda0d5b9e0f5b3bf83cc519da5bcd4a4', 4, 1619591053, 1622183053);
INSERT INTO `sp_user_token` VALUES ('8454beb7f4e8bce5b9c7535d0c19ebd211bf43a6', 4, 1620272143, 1622864143);
INSERT INTO `sp_user_token` VALUES ('84abfa6c7651975f2abf15590e9c574b16b129c5', 3, 1618897022, 1621489022);
INSERT INTO `sp_user_token` VALUES ('84c107783cb518ab4f2baba77f3db6584735c9ca', 4, 1620375259, 1622967259);
INSERT INTO `sp_user_token` VALUES ('84d22bd0643364f88195cd19c802891dce51f87f', 6, 1621304914, 1623896914);
INSERT INTO `sp_user_token` VALUES ('84dafc34ddbcabf9d8f83b3d50c61fd907347800', 3, 1618913257, 1621505257);
INSERT INTO `sp_user_token` VALUES ('8521648a2e086f44909990cf8fffe8b1e2b77875', 3, 1618908458, 1621500458);
INSERT INTO `sp_user_token` VALUES ('853ae07ed3fdbb952097e0a56b137ea41666e310', 5, 1620721065, 1623313065);
INSERT INTO `sp_user_token` VALUES ('86050ed0a4d59b8c9d159d3ca3bf6b8ab19ebf21', 4, 1620042871, 1622634871);
INSERT INTO `sp_user_token` VALUES ('862ad0f6af6a30cbfafc902257a91ece23d19586', 3, 1618914231, 1621506231);
INSERT INTO `sp_user_token` VALUES ('863435708723628e47b3d272bbca484d4e156083', 4, 1620371415, 1622963415);
INSERT INTO `sp_user_token` VALUES ('86d5d7f76bb1158040f0e7714c0325f023523636', 3, 1618898560, 1621490560);
INSERT INTO `sp_user_token` VALUES ('8713177eecc7de266d58e51d467b0c96030b5635', 4, 1618987112, 1621579112);
INSERT INTO `sp_user_token` VALUES ('8714d679738566a893f9673940a15093b50ecd31', 3, 1618972793, 1621564793);
INSERT INTO `sp_user_token` VALUES ('87e00c756d1a109dda2c78494e64b78c8bfeca52', 3, 1618974572, 1621566572);
INSERT INTO `sp_user_token` VALUES ('880e514a92dcc89a379e07878effc590a032e1cf', 4, 1618985689, 1621577689);
INSERT INTO `sp_user_token` VALUES ('8850f4170dc6f69d63dd0a4bd6646120db0ffe39', 3, 1618968527, 1621560527);
INSERT INTO `sp_user_token` VALUES ('886c84c0a29f1f91412f0b446ddbe429d65b3e30', 3, 1618907455, 1621499455);
INSERT INTO `sp_user_token` VALUES ('887b5f5051f5179a4907f51cd8b47a11a2826332', 3, 1618969154, 1621561154);
INSERT INTO `sp_user_token` VALUES ('88d44499a3ec2f89ca6a5d5437384e92af251b49', 21, 1620609753, 1623201753);
INSERT INTO `sp_user_token` VALUES ('88e619368cb8082aa0c40884b16050b00b30d437', 3, 1618900279, 1621492279);
INSERT INTO `sp_user_token` VALUES ('88ebcb7e6e66ad72070b3c5dce745d45df62d024', 3, 1619163129, 1621755129);
INSERT INTO `sp_user_token` VALUES ('89a0d1fb72c63abdd9925ae3d57381ef94bae0a0', 4, 1619148318, 1621740318);
INSERT INTO `sp_user_token` VALUES ('89aae05882274b576ac361823aeb1d0f0659e6f9', 4, 1619148731, 1621740731);
INSERT INTO `sp_user_token` VALUES ('89b6d8adf52835cdb4be3276c02fec0db6061f50', 6, 1620863289, 1623455289);
INSERT INTO `sp_user_token` VALUES ('89ceae87cb97b235b84e42d30c51fd4f09523bef', 6, 1621049254, 1623641254);
INSERT INTO `sp_user_token` VALUES ('8b25c61fceadc9d51f427fa5280147a52b0b30ee', 4, 1620042213, 1622634213);
INSERT INTO `sp_user_token` VALUES ('8b66ffac25bb81bb8084a837c96844461d2cb14a', 4, 1619587250, 1622179250);
INSERT INTO `sp_user_token` VALUES ('8b991b2d094b5fb7b025368f4ecff83c13cbe80e', 5, 1619185577, 1621777577);
INSERT INTO `sp_user_token` VALUES ('8c5fce700faa7501c8cccb4179c83ab198c2af7a', 4, 1619159958, 1621751958);
INSERT INTO `sp_user_token` VALUES ('8cb06291c83340ed53b5edc0a28c890e9e89bee9', 3, 1618977002, 1621569002);
INSERT INTO `sp_user_token` VALUES ('8d8dbe3415d6211902cf4db4ca3c3500c8af7dbf', 3, 1618898398, 1621490398);
INSERT INTO `sp_user_token` VALUES ('8df8b2b8399ce4eae6713ddcdb67cdc00cd6513d', 3, 1618982473, 1621574473);
INSERT INTO `sp_user_token` VALUES ('8e35651d0338efec0b4a5dd60b756b068d7efd6c', 3, 1619166275, 1621758275);
INSERT INTO `sp_user_token` VALUES ('8e46a93ead17a1e7e8b6033e85fcc6d97fc89444', 3, 1619164519, 1621756519);
INSERT INTO `sp_user_token` VALUES ('8f52c19097f9102f8bd292515ea34d75ee586d91', 25, 1620956861, 1623548861);
INSERT INTO `sp_user_token` VALUES ('8fd96e8fe48fcecce7d826b81418535ed743a939', 6, 1621092772, 1623684772);
INSERT INTO `sp_user_token` VALUES ('91197860c4c3ed80b4f8d0b2f66d8ab6f88ff192', 31, 1621394451, 1623986451);
INSERT INTO `sp_user_token` VALUES ('912be77716cda90139b3d5de8ecb636d87ddea9f', 4, 1620375190, 1622967190);
INSERT INTO `sp_user_token` VALUES ('91866224b6776949bbb471c2e4f9160f90224ad1', 3, 1618972360, 1621564360);
INSERT INTO `sp_user_token` VALUES ('91a65e99b30c4d5e8cb3c21b162f2dd9cfc2b447', 3, 1618994781, 1621586781);
INSERT INTO `sp_user_token` VALUES ('921b4efe971a581f84165f1d2b7c7ef1221d452c', 4, 1618983387, 1621575387);
INSERT INTO `sp_user_token` VALUES ('92565b9011492d8e37dfb52bca7e195fd5a37735', 26, 1620984777, 1623576777);
INSERT INTO `sp_user_token` VALUES ('92a0b1800a8e605228a3e5d50c035c496e000582', 5, 1619185617, 1621777617);
INSERT INTO `sp_user_token` VALUES ('92ddcbd0a6e6961df9da114a63ff3dedc2865f1d', 4, 1620272446, 1622864446);
INSERT INTO `sp_user_token` VALUES ('931072b6f0d88618f0bd7a0b7e912cba8b49db86', 4, 1619592272, 1622184272);
INSERT INTO `sp_user_token` VALUES ('935eae26b33284f316f810eb33548f0a04b9ebbc', 6, 1620449804, 1623041804);
INSERT INTO `sp_user_token` VALUES ('939b4450bed8b94fe80b35106f181fceb9e98192', 4, 1620270718, 1622862718);
INSERT INTO `sp_user_token` VALUES ('95458e42db2fd61f77e398ac08be15592a192e3c', 4, 1619148872, 1621740872);
INSERT INTO `sp_user_token` VALUES ('9585d2015f53aaec0361e10a58ed269e11df3e58', 4, 1619167588, 1621759588);
INSERT INTO `sp_user_token` VALUES ('95cdf9a298b621e5265eea752c2c41c191249f0e', 4, 1619145440, 1621737440);
INSERT INTO `sp_user_token` VALUES ('963efb212217745eb5620939fe2b327ebd77020e', 3, 1619166261, 1621758261);
INSERT INTO `sp_user_token` VALUES ('96822eaa1cee448f858edf7a270d59cd00127ea9', 4, 1620374928, 1622966928);
INSERT INTO `sp_user_token` VALUES ('96823af28a2470813c66a52fb72bdca77850b761', 4, 1619144206, 1621736206);
INSERT INTO `sp_user_token` VALUES ('96c430e225e8ec7f008a76ea51bd9df891bc6b79', 4, 1618983736, 1621575736);
INSERT INTO `sp_user_token` VALUES ('96cb7590a081000ac91ea4c972f575a042d1b169', 3, 1618996515, 1621588515);
INSERT INTO `sp_user_token` VALUES ('976f36a2eb2fde64f0c07f57c3a83da9b56e585e', 6, 1619308107, 1621900107);
INSERT INTO `sp_user_token` VALUES ('97c4b8c7ccb0209051c20a572c00c928d7b0252c', 3, 1618973818, 1621565818);
INSERT INTO `sp_user_token` VALUES ('98250d3ab21842fbf32b6c2da8066a7fbe73a9f5', 4, 1619161279, 1621753279);
INSERT INTO `sp_user_token` VALUES ('98e1b22d87d224cffbaa2fd680d7fd36ce50fc39', 4, 1618985613, 1621577613);
INSERT INTO `sp_user_token` VALUES ('99079a1580010257991278ba39eb4c7284b4248c', 4, 1618982913, 1621574913);
INSERT INTO `sp_user_token` VALUES ('99365d240e074f495a8e256285dc5761eb81a60b', 3, 1619167533, 1621759533);
INSERT INTO `sp_user_token` VALUES ('993df833ac9c7c50f68e649d65b5684e29a0a644', 4, 1619168495, 1621760495);
INSERT INTO `sp_user_token` VALUES ('99afa0ea070f1d2c88679b2f8db5668b92a0cc3f', 1, 1618897966, 1621489966);
INSERT INTO `sp_user_token` VALUES ('9a0bd190317fe7134d57d23a0e53360be6101b52', 7, 1620371707, 1622963707);
INSERT INTO `sp_user_token` VALUES ('9a25569ed73ac22b62f623769ab28934fdd01bc0', 6, 1620380583, 1622972583);
INSERT INTO `sp_user_token` VALUES ('9c25fd9cdb8099f0a466438c724223f5b626c99f', 3, 1619094321, 1621686321);
INSERT INTO `sp_user_token` VALUES ('9c70695f7db62871ba1f69315e5a039494492b5e', 4, 1619148723, 1621740723);
INSERT INTO `sp_user_token` VALUES ('9c7568483d7d65b40d86e4f3cda5b305e7adf31f', 4, 1618989831, 1621581831);
INSERT INTO `sp_user_token` VALUES ('9cb7d0ec2d0cb9bf71755a58fbf33306ec822666', 4, 1619161427, 1621753427);
INSERT INTO `sp_user_token` VALUES ('9d1654d55261375a9d378cac78bf7df5719e3f7a', 3, 1618982743, 1621574743);
INSERT INTO `sp_user_token` VALUES ('9d6596a305368bd3220f0c9a8c5e4ce96f027ed3', 4, 1620375927, 1622967927);
INSERT INTO `sp_user_token` VALUES ('9d804012f48086d93b33612e5130ad3882ab143d', 3, 1618970590, 1621562590);
INSERT INTO `sp_user_token` VALUES ('9d8e36093c3e3769a6728bdc0d9a1200b497d8bd', 6, 1619271076, 1621863076);
INSERT INTO `sp_user_token` VALUES ('9ddf55de9d1b20139e6db5ea8053d04356c6c809', 6, 1620657597, 1623249597);
INSERT INTO `sp_user_token` VALUES ('9e2e9db49e2ce8d5806d99a1fcc63e1a605e6b59', 6, 1620784657, 1623376657);
INSERT INTO `sp_user_token` VALUES ('9ea4aac289425b10411750d7e3303ba7a6e0b2d2', 3, 1618993850, 1621585850);
INSERT INTO `sp_user_token` VALUES ('9f5bd107f13f1d97c2e029201dcad993037d58e8', 8, 1620372448, 1622964448);
INSERT INTO `sp_user_token` VALUES ('9f717c6b083b51179fb53babfff670c20d3da391', 3, 1619162729, 1621754729);
INSERT INTO `sp_user_token` VALUES ('9f7f00fd34444e2354b4b960864b8778ad6f51d1', 6, 1619170641, 1621762641);
INSERT INTO `sp_user_token` VALUES ('a022cc1e1f9d781cce491434488f8e20dd627e10', 3, 1618994232, 1621586232);
INSERT INTO `sp_user_token` VALUES ('a04625498296f73a32dc6ba7d9a1dd73e9e05ff4', 4, 1620372707, 1622964707);
INSERT INTO `sp_user_token` VALUES ('a0838d35bdd9db6404ba1d848a980a63264fbb7b', 3, 1618898282, 1621490282);
INSERT INTO `sp_user_token` VALUES ('a0a65d9339432ae20bf4336cac41dc4f092e90ab', 4, 1620372652, 1622964652);
INSERT INTO `sp_user_token` VALUES ('a11ffea79c1b00a4ddf591f42b3f4b202c3c5d36', 28, 1621061408, 1623653408);
INSERT INTO `sp_user_token` VALUES ('a23c89cd1ca8e0202d2c0b5d04c60b9ccc272049', 3, 1619165470, 1621757470);
INSERT INTO `sp_user_token` VALUES ('a2b4c7824ea13e7663acd051a48b785e5ddf623a', 3, 1619094435, 1621686435);
INSERT INTO `sp_user_token` VALUES ('a486c973c5d3844c5bedd7b6bec095fca789d01a', 4, 1620372853, 1622964853);
INSERT INTO `sp_user_token` VALUES ('a490adffe39b3c052d84f35b67fb451bf011567f', 3, 1618898597, 1621490597);
INSERT INTO `sp_user_token` VALUES ('a51d97f03c04add645e2e9274aa688a96794e93b', 3, 1620265264, 1622857264);
INSERT INTO `sp_user_token` VALUES ('a61a44d6ad37eb4ddba1686d2b9c1c029428ceda', 3, 1618997909, 1621589909);
INSERT INTO `sp_user_token` VALUES ('a63968ad3bc20bcbd616c68151ed020d25ada537', 4, 1620272140, 1622864140);
INSERT INTO `sp_user_token` VALUES ('a644c60762816f5a705ae15b1f68b9d9942a5692', 4, 1620374013, 1622966013);
INSERT INTO `sp_user_token` VALUES ('a69d550b7cce0ebd63d37e9c92eccd745a150aac', 4, 1619167542, 1621759542);
INSERT INTO `sp_user_token` VALUES ('a76e37ab03a4b3067a59a61eb6c050f03d5cdae1', 3, 1618968642, 1621560642);
INSERT INTO `sp_user_token` VALUES ('a79586660adc880c6819d5846d0fc3072ab40b8a', 4, 1619144837, 1621736837);
INSERT INTO `sp_user_token` VALUES ('a7a880123d53bad47055e44d40b183cc71a5a963', 3, 1618906225, 1621498225);
INSERT INTO `sp_user_token` VALUES ('a7ae69088acdfced5dd41ade645cbd658c07d826', 3, 1620374230, 1622966230);
INSERT INTO `sp_user_token` VALUES ('a828011a543d123217410f758d8e7c9311d23680', 3, 1618981729, 1621573729);
INSERT INTO `sp_user_token` VALUES ('a8490edc896b33be9b14717ee0bc1771884c0621', 14, 1620374265, 1622966265);
INSERT INTO `sp_user_token` VALUES ('a8c76240b8abcece2413c7359f98e7f8ddebcaf8', 3, 1619143140, 1621735140);
INSERT INTO `sp_user_token` VALUES ('a91096da9cd58478b5d52f4a09fe44c31401a083', 4, 1619168465, 1621760465);
INSERT INTO `sp_user_token` VALUES ('a97a4a4fe14378d612aaef016b3b045008512a29', 20, 1620554386, 1623146386);
INSERT INTO `sp_user_token` VALUES ('a9e7b9ba25f3d634d8df0742dcb0ed44902aa543', 3, 1619164263, 1621756263);
INSERT INTO `sp_user_token` VALUES ('a9ef07a405ce203c07eb63bfed3e62192a17ffd5', 4, 1620375358, 1622967358);
INSERT INTO `sp_user_token` VALUES ('aa0e748d083a5881af1c2b020a6079d255573e5e', 6, 1620034517, 1622626517);
INSERT INTO `sp_user_token` VALUES ('aa572414fb0bf52f7da5f53aa6ce85c61fc9306d', 3, 1618900425, 1621492425);
INSERT INTO `sp_user_token` VALUES ('aa76b86ff761ad8ebf41223c5f5bebbc86a7f158', 4, 1618987599, 1621579599);
INSERT INTO `sp_user_token` VALUES ('ab5fabfd2d4918789377e0e7a2d160d7a263d41a', 4, 1619159013, 1621751013);
INSERT INTO `sp_user_token` VALUES ('ab7a55e2fd20161dd837183a374d53dccddc698e', 3, 1618910091, 1621502091);
INSERT INTO `sp_user_token` VALUES ('abfb563c2f29a16fcbc22c1bb7d2dd0e337ba424', 3, 1619162877, 1621754877);
INSERT INTO `sp_user_token` VALUES ('ac07ddc5de62dc0d1d58b98f9dd8e8cefac81fa8', 4, 1618988087, 1621580087);
INSERT INTO `sp_user_token` VALUES ('ac29940c2a52d72ba7d14ea6fd1079153844aace', 3, 1618906133, 1621498133);
INSERT INTO `sp_user_token` VALUES ('ac33d4b1512adb63c0fc654511c4d18cc6fdf91a', 3, 1619167422, 1621759422);
INSERT INTO `sp_user_token` VALUES ('aca38907ea0b8a8d5619f8808315a65f261f7968', 4, 1620375183, 1622967183);
INSERT INTO `sp_user_token` VALUES ('ad1cb25f7138352c0799bee2b7849e382b650974', 6, 1620371944, 1622963944);
INSERT INTO `sp_user_token` VALUES ('ad308fd13137957844622e54ef732406a76d9f61', 3, 1618973931, 1621565931);
INSERT INTO `sp_user_token` VALUES ('ad32ea915df00c3e412a26d69e70492983724c89', 25, 1620956940, 1623548940);
INSERT INTO `sp_user_token` VALUES ('ae03ac8f031d55a82c12adb82a136bebc7393696', 6, 1620955312, 1623547312);
INSERT INTO `sp_user_token` VALUES ('ae6477d1cb6744740e6f095e1f0c278a7faa1f49', 4, 1618990860, 1621582860);
INSERT INTO `sp_user_token` VALUES ('af62d7b178878c378825ca2ea52e686732317bc0', 3, 1620265111, 1622857111);
INSERT INTO `sp_user_token` VALUES ('af663e7259a6c5a270298bad252c1244c15ee23a', 3, 1618971024, 1621563024);
INSERT INTO `sp_user_token` VALUES ('af82e75fb49cec0f712385b653b152c86a0525ae', 6, 1621304974, 1623896974);
INSERT INTO `sp_user_token` VALUES ('b01bdbd9b2d34fba28f188cbaf48aef4b9215997', 4, 1619160045, 1621752045);
INSERT INTO `sp_user_token` VALUES ('b0403f2e5e8e5bba65c61853471de4def05e9413', 3, 1619162898, 1621754898);
INSERT INTO `sp_user_token` VALUES ('b0bc0bc4eb3ca19672c7500690487f933036d0f1', 3, 1620265053, 1622857053);
INSERT INTO `sp_user_token` VALUES ('b1e635247b872f0cac717dd343667ba6d6ac14fc', 3, 1618974524, 1621566524);
INSERT INTO `sp_user_token` VALUES ('b200113505ee5436db39325d97bba902a285f2b7', 3, 1618970340, 1621562340);
INSERT INTO `sp_user_token` VALUES ('b259fad51433377682ff7f27d73a166bcfb7ee7e', 21, 1620619112, 1623211112);
INSERT INTO `sp_user_token` VALUES ('b29c79c93ed0155a813937da439d4a45cba322ed', 6, 1620957561, 1623549561);
INSERT INTO `sp_user_token` VALUES ('b2fc69de5aafc1088b86a93ba1224276f1c4baaa', 3, 1618971337, 1621563337);
INSERT INTO `sp_user_token` VALUES ('b30908ce9458d8b75eccfc837225b149d4482b23', 3, 1618994275, 1621586275);
INSERT INTO `sp_user_token` VALUES ('b3653e0953e0fa291aed79a4f628f25f7269352e', 6, 1621310732, 1623902732);
INSERT INTO `sp_user_token` VALUES ('b3758067da42a931a24f8927ab30285dd9053684', 4, 1619161725, 1621753725);
INSERT INTO `sp_user_token` VALUES ('b3a270dd8abd50e39713fa1ea46b18993bf8d1e1', 3, 1618901212, 1621493212);
INSERT INTO `sp_user_token` VALUES ('b3a4bc098aebc4088fe3c71e3ef0a5adef03ccf3', 4, 1619146838, 1621738838);
INSERT INTO `sp_user_token` VALUES ('b3d3226dbd47b57b6c1b1d9b6861f0f8477cec52', 3, 1618904667, 1621496667);
INSERT INTO `sp_user_token` VALUES ('b40782a82e9d01119998cf3d74460d9f97b77d9d', 6, 1621050947, 1623642947);
INSERT INTO `sp_user_token` VALUES ('b449a7812e6fc68710c81f5a38dc78498b29169e', 3, 1618991971, 1621583971);
INSERT INTO `sp_user_token` VALUES ('b50c810f7036c57a64eb6cc21aeeb0d1e7da5f02', 6, 1620884121, 1623476121);
INSERT INTO `sp_user_token` VALUES ('b54837f3abf9ed572cc79b6a9088923e344f0525', 4, 1619145739, 1621737739);
INSERT INTO `sp_user_token` VALUES ('b5748863ffc433cda22584d1e79fb11e936db034', 23, 1620784835, 1623376835);
INSERT INTO `sp_user_token` VALUES ('b5baa7c228c844f36f6b7e441ee47fe4d6c5bfe5', 3, 1619166134, 1621758134);
INSERT INTO `sp_user_token` VALUES ('b619e1490781510ed927feb783aede3138bc5d0c', 3, 1618907389, 1621499389);
INSERT INTO `sp_user_token` VALUES ('b67abe0d35a38e1f95cb0740df2344736106954f', 3, 1618901072, 1621493072);
INSERT INTO `sp_user_token` VALUES ('b726f9957287629542093436780a43eb186d432b', 3, 1620373549, 1622965549);
INSERT INTO `sp_user_token` VALUES ('b78aa4634b3b8b807252e0c459e48cad5a5b2823', 3, 1619060259, 1621652259);
INSERT INTO `sp_user_token` VALUES ('b7b0d082ce2d867ada9254d8345cd16df225095d', 3, 1618973199, 1621565199);
INSERT INTO `sp_user_token` VALUES ('b7f6bc8d88d640876aea3da7563e69ab79710082', 3, 1618994792, 1621586792);
INSERT INTO `sp_user_token` VALUES ('b82aeacd63a0b80036bca1800178a1d42c282628', 6, 1619271968, 1621863968);
INSERT INTO `sp_user_token` VALUES ('b879119727147a6a2c1523440fbe582d5a0e15ba', 25, 1620956932, 1623548932);
INSERT INTO `sp_user_token` VALUES ('b8ef9df96e25fba48f5dbf93027bc807e7b971c4', 3, 1618896883, 1621488883);
INSERT INTO `sp_user_token` VALUES ('b8efb96b9889742fe95aaaf24b12514c353d1943', 4, 1620377186, 1622969186);
INSERT INTO `sp_user_token` VALUES ('b9467ffd115cc8fa62cbe08de259ab9379c41711', 6, 1619662485, 1622254485);
INSERT INTO `sp_user_token` VALUES ('b95b553ce8937393e100e60952bbc287e43f1628', 4, 1620042316, 1622634316);
INSERT INTO `sp_user_token` VALUES ('b9721078d1d87356af2066be3f3f42ce0486c77c', 3, 1618911623, 1621503623);
INSERT INTO `sp_user_token` VALUES ('ba05999e4dac6c9bf299fe7cb1b4e61a8422d4ed', 4, 1620380658, 1622972658);
INSERT INTO `sp_user_token` VALUES ('ba5bdbffddb89b99fce44787d46464e4a63f7e5a', 3, 1618908848, 1621500848);
INSERT INTO `sp_user_token` VALUES ('ba6f426cb757a971c09317d35bb4334298ed60cc', 4, 1618985411, 1621577411);
INSERT INTO `sp_user_token` VALUES ('baddba322431feeb1feccce39c0417c8e105204b', 4, 1620375754, 1622967754);
INSERT INTO `sp_user_token` VALUES ('bb9871b9fbf3c587bd5823663dd43cdfc2eb5ecd', 3, 1618973749, 1621565749);
INSERT INTO `sp_user_token` VALUES ('bc9c4c76653c06fa1fb5418ddcddf728b339ac23', 3, 1618913955, 1621505955);
INSERT INTO `sp_user_token` VALUES ('bca28329ac29c9975eb7ad02a5422b9d366bc8d3', 4, 1620267762, 1622859762);
INSERT INTO `sp_user_token` VALUES ('bcbd629e460dda3635bf9dbcd666076b51816781', 3, 1618973169, 1621565169);
INSERT INTO `sp_user_token` VALUES ('bd0cee87d7630012d7e96fc718424e0773fe5e5a', 22, 1620619968, 1623211968);
INSERT INTO `sp_user_token` VALUES ('bd82900f7c956ec6abd2f899ec9daf342da1489c', 4, 1619145712, 1621737712);
INSERT INTO `sp_user_token` VALUES ('bdc97a44b49844c8b040d16a40511c6ccaf58104', 32, 1621394459, 1623986459);
INSERT INTO `sp_user_token` VALUES ('beb2731eccb0eb8d3dbaf5cc4056fd1b94ca45a2', 4, 1618986469, 1621578469);
INSERT INTO `sp_user_token` VALUES ('bfa415993eeffc847882fd0328d037875ed8a249', 4, 1620264107, 1622856107);
INSERT INTO `sp_user_token` VALUES ('bfd70372529087949a2f1b7b265335b2e75d6708', 4, 1620271638, 1622863638);
INSERT INTO `sp_user_token` VALUES ('bff33e5ccc8751432169514b7e3c0124dab41f11', 3, 1618903671, 1621495671);
INSERT INTO `sp_user_token` VALUES ('c088b9931e452bcabc564d89d2bff7f012c665d7', 3, 1619094719, 1621686719);
INSERT INTO `sp_user_token` VALUES ('c0d0d0d5882861ed548d596648f42b7513abde85', 4, 1618990635, 1621582635);
INSERT INTO `sp_user_token` VALUES ('c19fcbe7f8f635b4ff0e5d49a5bb52127bd4fb6d', 6, 1620554447, 1623146447);
INSERT INTO `sp_user_token` VALUES ('c1cdffdc43104fc66b17c2304279460eadc349f2', 3, 1618973215, 1621565215);
INSERT INTO `sp_user_token` VALUES ('c20e56b6708ea1b6f2a2971ab005ff08b5eceaa6', 3, 1618992978, 1621584978);
INSERT INTO `sp_user_token` VALUES ('c382fbde404bfa2a9e719fe6799754cd8dc8f7b2', 4, 1620272449, 1622864449);
INSERT INTO `sp_user_token` VALUES ('c4b0a9d634e5b74f3bb130fceb3189e44cead938', 4, 1619168460, 1621760460);
INSERT INTO `sp_user_token` VALUES ('c5b4db77941d5a5df26590fb73b796b772a97bad', 3, 1618904478, 1621496478);
INSERT INTO `sp_user_token` VALUES ('c5cb24f935c4d2ed250500701ef20050a3fe9a43', 3, 1621261560, 1623853560);
INSERT INTO `sp_user_token` VALUES ('c633389e6b8802062b5e3152ce694f38b70e6728', 31, 1621397683, 1623989683);
INSERT INTO `sp_user_token` VALUES ('c6ad05f2b0f31f11ca6c73122064bac63d6463eb', 6, 1620379608, 1622971608);
INSERT INTO `sp_user_token` VALUES ('c7369493abb8c2515d7ddba772495486cd9735ff', 6, 1621296355, 1623888355);
INSERT INTO `sp_user_token` VALUES ('c7a9f717c35b06876e9922184ecc1fd49912bd37', 3, 1618974818, 1621566818);
INSERT INTO `sp_user_token` VALUES ('c7b0cdcab74ec1e97c4ea9d51189144e41b756a9', 3, 1620265335, 1622857335);
INSERT INTO `sp_user_token` VALUES ('c801139e73510c1fbea00fad91346f46fc0357d8', 3, 1618972317, 1621564317);
INSERT INTO `sp_user_token` VALUES ('c836ee8f692575e619c6c4482b87ba6d6d76b017', 3, 1618899861, 1621491861);
INSERT INTO `sp_user_token` VALUES ('c891e0193bdf7076404203ad895e2df3eeca11a4', 3, 1618899152, 1621491152);
INSERT INTO `sp_user_token` VALUES ('ca5958e9a2d8dc32fbfb58d571d86ad6fc99c7a2', 3, 1619167412, 1621759412);
INSERT INTO `sp_user_token` VALUES ('cac54b57afc3769fb58d70f84cabca6facbf83c8', 3, 1619165548, 1621757548);
INSERT INTO `sp_user_token` VALUES ('cb0869b1316ea36b02f5b36a4b113cf335c79071', 3, 1620265267, 1622857267);
INSERT INTO `sp_user_token` VALUES ('cb45a6b0e9db729add5c531cfdbb7ebaff17ae18', 4, 1620375986, 1622967986);
INSERT INTO `sp_user_token` VALUES ('cbda9b1b66c4567386cc3c96915367e55f292c0e', 4, 1618988693, 1621580693);
INSERT INTO `sp_user_token` VALUES ('cc09afa83c30e20fe76e9db2091eb40357a8de6f', 3, 1618992248, 1621584248);
INSERT INTO `sp_user_token` VALUES ('cd97fa8de15c9ae4f66ecf472fdf96a09cd99de0', 3, 1618905793, 1621497793);
INSERT INTO `sp_user_token` VALUES ('cda00e8403086b72cded894dde17d24971e99d12', 5, 1619156208, 1621748208);
INSERT INTO `sp_user_token` VALUES ('cda1685ddcf988a602f43c8ea5df63ff16917589', 6, 1620371781, 1622963781);
INSERT INTO `sp_user_token` VALUES ('cdd13296679f05502a3a56483994cd0e38d0e839', 3, 1620373450, 1622965450);
INSERT INTO `sp_user_token` VALUES ('cde78600c8f7c6d462f9ef8c7c041a5c95e7abdd', 4, 1619592203, 1622184203);
INSERT INTO `sp_user_token` VALUES ('ce5cd24172edafcf5cd15ee3589c12aabde34ef5', 6, 1620381707, 1622973707);
INSERT INTO `sp_user_token` VALUES ('cff4ca5df953c09ddeff459d6adcb86838677109', 3, 1618972939, 1621564939);
INSERT INTO `sp_user_token` VALUES ('d039c981bae653c5645e67b6092fe933647f51f0', 4, 1619154743, 1621746743);
INSERT INTO `sp_user_token` VALUES ('d03cc1b056c0a15d9d65c2aeeb9d71ca76b8624d', 3, 1620373580, 1622965580);
INSERT INTO `sp_user_token` VALUES ('d071e36a61ded979b813e4cf3c4a581bf38e4ae0', 3, 1618971638, 1621563638);
INSERT INTO `sp_user_token` VALUES ('d0916593dfef6b239b4ee0760360f07900a7880b', 4, 1619149091, 1621741091);
INSERT INTO `sp_user_token` VALUES ('d161b6dc07b51b89f99858b98a7edf2c51cb70da', 3, 1618994361, 1621586361);
INSERT INTO `sp_user_token` VALUES ('d1957dc198cd46b9b899a443acf5c901cc71420d', 3, 1618904395, 1621496395);
INSERT INTO `sp_user_token` VALUES ('d226d699a930d7649444c7445b644cff85ee605b', 30, 1621305104, 1623897104);
INSERT INTO `sp_user_token` VALUES ('d24d541de144c335346ba55a1c25c642673937b0', 3, 1620373466, 1622965466);
INSERT INTO `sp_user_token` VALUES ('d315d899194b359b6156647b5e30855eaaa57418', 6, 1619271277, 1621863277);
INSERT INTO `sp_user_token` VALUES ('d32707541f46d7caf8aceee3b726b35f3c106066', 3, 1621304951, 1623896951);
INSERT INTO `sp_user_token` VALUES ('d34efbeab791ec1a9208cca2a089c4019de99121', 3, 1618969371, 1621561371);
INSERT INTO `sp_user_token` VALUES ('d41ab5708499e5c419fc302815f1f9a037eae254', 3, 1618912079, 1621504079);
INSERT INTO `sp_user_token` VALUES ('d429f01ab6643b60f4429c027e673bb497dbe6b5', 21, 1620626078, 1623218078);
INSERT INTO `sp_user_token` VALUES ('d46ab6dbd1f300b6faf40b713330aead6bf884a9', 3, 1619162764, 1621754764);
INSERT INTO `sp_user_token` VALUES ('d4744797b1d12d62b8df25027da53d283cebaf64', 4, 1620263188, 1622855188);
INSERT INTO `sp_user_token` VALUES ('d47f9b8b2ce547d3252565663f02b75663d1a720', 3, 1618914081, 1621506081);
INSERT INTO `sp_user_token` VALUES ('d4accbeb71d0992b02433644f86770f5233d9c2b', 4, 1619161689, 1621753689);
INSERT INTO `sp_user_token` VALUES ('d4ccfbacebcc2e631ec0ff37fcce6aa58ecde4bd', 3, 1618908909, 1621500909);
INSERT INTO `sp_user_token` VALUES ('d4d3be4d5bb3d6ed93ce4cbf3d4790abbc7d1ed0', 6, 1621296285, 1623888285);
INSERT INTO `sp_user_token` VALUES ('d4deace369163fdfc574aac0a793ffeb90b0738a', 6, 1620609659, 1623201659);
INSERT INTO `sp_user_token` VALUES ('d57bab33fe78df91a4349fcac3fdad72592bfb55', 3, 1618970419, 1621562419);
INSERT INTO `sp_user_token` VALUES ('d5f914eff9efadb296fb1b722d3834117fb82ce8', 3, 1618976953, 1621568953);
INSERT INTO `sp_user_token` VALUES ('d60e7f3a7efdf1226d45bdd2c91620e8997f69d2', 31, 1621394494, 1623986494);
INSERT INTO `sp_user_token` VALUES ('d61b21f600585141a573ae41ed8f13e6f17c80bf', 3, 1618898105, 1621490105);
INSERT INTO `sp_user_token` VALUES ('d661b5ded06748898bfb61fe59829bfb06c7bf84', 4, 1619167927, 1621759927);
INSERT INTO `sp_user_token` VALUES ('d6defe898b70b878a4904d308db5d48a91b65cc4', 3, 1619164874, 1621756874);
INSERT INTO `sp_user_token` VALUES ('d7aafffe75c6cc16874c9bf8bf44430dce1e5dc3', 3, 1618976609, 1621568609);
INSERT INTO `sp_user_token` VALUES ('d7deb905d42a02606bdc2280a00959ca9678fcb3', 3, 1618903946, 1621495946);
INSERT INTO `sp_user_token` VALUES ('d834a076ac0f1d75626ce1e5612abd20cab3c2e7', 3, 1618900259, 1621492259);
INSERT INTO `sp_user_token` VALUES ('d8751842e7afeb1fcab4d96c1ad2af3b72cd0e71', 3, 1618896567, 1621488567);
INSERT INTO `sp_user_token` VALUES ('d8e3c24595cad4ce4f3c37d66d7ed166a5dfa929', 3, 1621299385, 1623891385);
INSERT INTO `sp_user_token` VALUES ('d903bddc76bdb05b7c52d08e04c5e89b905450fe', 7, 1620372185, 1622964185);
INSERT INTO `sp_user_token` VALUES ('d9110d253193b4f47059dee0a2b324b28ee93ddf', 4, 1619161117, 1621753117);
INSERT INTO `sp_user_token` VALUES ('d961b2ff30f1c13ae586db139e39ff1c25815d02', 4, 1620373699, 1622965699);
INSERT INTO `sp_user_token` VALUES ('d97f23fc87327bc89ee0549f904ce9e14c1d33d5', 4, 1618983009, 1621575009);
INSERT INTO `sp_user_token` VALUES ('d9c4a43ea67e36d384e3407114cef52a6b7b2f23', 3, 1618974038, 1621566038);
INSERT INTO `sp_user_token` VALUES ('dacd995ebd3ab363d8989646470f277f95ce6a0a', 4, 1618987685, 1621579685);
INSERT INTO `sp_user_token` VALUES ('daf9ee2e19b0c7840c10e7e626f363a92c8b9d14', 4, 1620377017, 1622969017);
INSERT INTO `sp_user_token` VALUES ('db71c4dd8f23005c53c4431435d41d1af8896814', 3, 1618901515, 1621493515);
INSERT INTO `sp_user_token` VALUES ('db781fbf39a1b1437f4d69c6b6e4d93e3e20489c', 3, 1619094785, 1621686785);
INSERT INTO `sp_user_token` VALUES ('dbcd097b34039c6bc7ec2d74bc354392a92f28a0', 4, 1620373689, 1622965689);
INSERT INTO `sp_user_token` VALUES ('dc6d125e848df90c422ac0c046e42183fa9bf305', 4, 1618991788, 1621583788);
INSERT INTO `sp_user_token` VALUES ('dc6d38d5ff016056e145bebb295a56c84e26bb81', 3, 1618971239, 1621563239);
INSERT INTO `sp_user_token` VALUES ('dc7ddd35f3cc5fa7cd81bc84fc8040c5a12ad828', 3, 1618898309, 1621490309);
INSERT INTO `sp_user_token` VALUES ('dcde028471893cd09b4b0c243192435d9e562c2f', 3, 1618997845, 1621589845);
INSERT INTO `sp_user_token` VALUES ('dd0c853fdaf15c304996fd0c81ae337444f876e8', 4, 1619168326, 1621760326);
INSERT INTO `sp_user_token` VALUES ('dd674b3d6d45b281ff120bde5e508355d8172690', 3, 1618906413, 1621498413);
INSERT INTO `sp_user_token` VALUES ('dd786c8b18869d66f43f251043d4d0bd9cebd8f0', 6, 1621241909, 1623833909);
INSERT INTO `sp_user_token` VALUES ('de258ce730e7cb1aaad42653da9459b5a81512b0', 6, 1621241888, 1623833888);
INSERT INTO `sp_user_token` VALUES ('de2dcc354dba11ab35a62e1f9c0a5f9e2c2c7cb8', 4, 1619145499, 1621737499);
INSERT INTO `sp_user_token` VALUES ('de63d25e6c7fb6abfdfe586f95160df0ca373c3d', 6, 1620569867, 1623161867);
INSERT INTO `sp_user_token` VALUES ('de6b01d4bffbe3fbaf490b9ac9a2db98e82ee616', 4, 1619057400, 1621649400);
INSERT INTO `sp_user_token` VALUES ('de7fc0d81af015cbb7fb20b5429449f3700e383d', 7, 1620371745, 1622963745);
INSERT INTO `sp_user_token` VALUES ('df0f26b3df01fd89bf1aeca9bc9498ea283bdc91', 3, 1618974644, 1621566644);
INSERT INTO `sp_user_token` VALUES ('df49deb36e8a0ec53194cbd1151f3d26c1956cff', 4, 1619148922, 1621740922);
INSERT INTO `sp_user_token` VALUES ('df751f1ea26d9a39c68afab3f5a988e69907dd84', 12, 1620373611, 1622965611);
INSERT INTO `sp_user_token` VALUES ('dfd7a2fcfda2a585e4a8360d6e954ebee5fe4b16', 3, 1619167525, 1621759525);
INSERT INTO `sp_user_token` VALUES ('e014b93b847c97c0d7c3727debc330cec183a64d', 3, 1618901279, 1621493279);
INSERT INTO `sp_user_token` VALUES ('e0374663c8277379beefca16c87cdb7c928bc260', 4, 1620371766, 1622963766);
INSERT INTO `sp_user_token` VALUES ('e0539888a5ed04f1526cd55a71dcda7c42b5dcec', 4, 1618991941, 1621583941);
INSERT INTO `sp_user_token` VALUES ('e064678ca7b322e7abf5c1b7fb030308414a51f4', 3, 1618972715, 1621564715);
INSERT INTO `sp_user_token` VALUES ('e1098f3b3d2e48582308de12760d5b103b027c2b', 6, 1620168652, 1622760652);
INSERT INTO `sp_user_token` VALUES ('e14f43d901426c3631c1e897b186119846c42687', 3, 1619167427, 1621759427);
INSERT INTO `sp_user_token` VALUES ('e1adf331c29fa8ba356b62b5b4f96a7c0c8bb716', 3, 1618900384, 1621492384);
INSERT INTO `sp_user_token` VALUES ('e1b068219b6b857476efd612d71c84611f2c79a2', 3, 1618971842, 1621563842);
INSERT INTO `sp_user_token` VALUES ('e20ca63b0857927b215e72b237b4de1a27aef8dc', 3, 1618993264, 1621585264);
INSERT INTO `sp_user_token` VALUES ('e23aadca42114016869ca3d6f525d7ab8db66005', 3, 1618975381, 1621567381);
INSERT INTO `sp_user_token` VALUES ('e2466be8213361fda882cd9ff5cc1ee71b85c050', 6, 1620097300, 1622689300);
INSERT INTO `sp_user_token` VALUES ('e299f967ec98d62b5ef42de13f4a20a3f9104859', 3, 1618897883, 1621489883);
INSERT INTO `sp_user_token` VALUES ('e39d260acb444203db07bdee8b738b36c064a3f1', 7, 1620380448, 1622972448);
INSERT INTO `sp_user_token` VALUES ('e39e412d557a6cbe349f3031fab432ee3c72e810', 6, 1620371745, 1622963745);
INSERT INTO `sp_user_token` VALUES ('e3f455806b0598953f0b6c7a7914499b1a598685', 4, 1620272129, 1622864129);
INSERT INTO `sp_user_token` VALUES ('e40fa31e064217dbc0cd87ec4e8bdfe2fe9dab7f', 3, 1618994238, 1621586238);
INSERT INTO `sp_user_token` VALUES ('e4104803e401e898fe586942fcb7f4579db5d896', 6, 1620379569, 1622971569);
INSERT INTO `sp_user_token` VALUES ('e4ae7cc718e756a8ed4446af307a4157ec850602', 4, 1619146475, 1621738475);
INSERT INTO `sp_user_token` VALUES ('e4d09e796de28c4f9dad78af4649f39f227f1795', 6, 1620609696, 1623201696);
INSERT INTO `sp_user_token` VALUES ('e52295b592f715d3a2734d93b331787ceadd38dc', 4, 1620375222, 1622967222);
INSERT INTO `sp_user_token` VALUES ('e5e35065c3e84b48b3a1f58766af3274cdd91f64', 6, 1619271322, 1621863322);
INSERT INTO `sp_user_token` VALUES ('e601b7c02cfa70230727c8b2f04db4b65fc0ff55', 3, 1618968507, 1621560507);
INSERT INTO `sp_user_token` VALUES ('e6a1b01c3c855a59475fa7bf49dd0d4199e160aa', 3, 1619229885, 1621821885);
INSERT INTO `sp_user_token` VALUES ('e79f0518d879468b3d4181880d696c6694405539', 4, 1619167535, 1621759535);
INSERT INTO `sp_user_token` VALUES ('e81ecc6f4ccd52d2eda90b86d984c455927dac32', 5, 1619157453, 1621749453);
INSERT INTO `sp_user_token` VALUES ('e945e9c14fb2a1ca277a1c148302eef54d1154f6', 3, 1618899526, 1621491526);
INSERT INTO `sp_user_token` VALUES ('e996feda64d40050185dea72de798a7986e7ceea', 3, 1618974812, 1621566812);
INSERT INTO `sp_user_token` VALUES ('e9cb3d4ac69945a3eb28530b562f8a1fb83be144', 3, 1620379669, 1622971669);
INSERT INTO `sp_user_token` VALUES ('e9e90e061b79855d114daeac76a04597f49d05f7', 4, 1619167545, 1621759545);
INSERT INTO `sp_user_token` VALUES ('ea972f32f5a0ee23bd355b211cabaf7dce18bf74', 3, 1620265376, 1622857376);
INSERT INTO `sp_user_token` VALUES ('eb1a0921620463f3839b22d5b7df3b969a883d1b', 5, 1619157130, 1621749130);
INSERT INTO `sp_user_token` VALUES ('ec2254b4c1e7890099bf2dc39cc2015114d788b4', 3, 1618901423, 1621493423);
INSERT INTO `sp_user_token` VALUES ('ecf334c7395420e07e178322bce10de20e13746f', 4, 1619160672, 1621752672);
INSERT INTO `sp_user_token` VALUES ('ed3b9931e4bf12e4c76107a814cddeb3e65fd845', 3, 1619143150, 1621735150);
INSERT INTO `sp_user_token` VALUES ('ed55b3cba1add2fe856790ad240cb469195a522e', 3, 1618897677, 1621489677);
INSERT INTO `sp_user_token` VALUES ('ed7a22139558b75a2a3df1271d483d2710a94a7f', 4, 1618982814, 1621574814);
INSERT INTO `sp_user_token` VALUES ('ed7da2e970fe6bdedea3d77116ab108a3ff501d8', 3, 1618972303, 1621564303);
INSERT INTO `sp_user_token` VALUES ('ee29beac43245b81c5cba44c9ace1de9504701df', 3, 1618973265, 1621565265);
INSERT INTO `sp_user_token` VALUES ('eeb2cd7162cff4f6572d5282b954b410ac5bdac0', 3, 1618969381, 1621561381);
INSERT INTO `sp_user_token` VALUES ('eebbe371bc0388110c0db2df99891b49ab62d49a', 3, 1618905663, 1621497663);
INSERT INTO `sp_user_token` VALUES ('eebd2cac7f9d2c1aac6f98bb475bdd9605747c97', 6, 1621304872, 1623896872);
INSERT INTO `sp_user_token` VALUES ('eecf1258ef374528497bb8569eea1616b2381324', 6, 1621251991, 1623843991);
INSERT INTO `sp_user_token` VALUES ('ef557f0dc9e6f7a441125fcf822d90a02e18edf8', 3, 1618906047, 1621498047);
INSERT INTO `sp_user_token` VALUES ('eff29ce570749a0249b45c2f72f24d6ffe4de0ba', 4, 1620372821, 1622964821);
INSERT INTO `sp_user_token` VALUES ('f1783356bfe24d431a06836a5d716c5508048679', 6, 1620619007, 1623211007);
INSERT INTO `sp_user_token` VALUES ('f17a2c377dfb5714d585c3c836fcaf81d32fe794', 4, 1620265145, 1622857145);
INSERT INTO `sp_user_token` VALUES ('f186590913e8d895e445c6f4182c3ed460b6f907', 4, 1618987047, 1621579047);
INSERT INTO `sp_user_token` VALUES ('f28b6b0523cde7baf1dfc17abd2b69e5ae5cfa11', 4, 1619143897, 1621735897);
INSERT INTO `sp_user_token` VALUES ('f31e81fddcf8a7116a1b6cdeee9c258a9c252634', 3, 1618975370, 1621567370);
INSERT INTO `sp_user_token` VALUES ('f32cdc7ddbafe6400c49eae6a30941966a6b374b', 4, 1620375273, 1622967273);
INSERT INTO `sp_user_token` VALUES ('f3d8081edcdcd332d5d2ce68fa7575aaddb38fde', 4, 1618983072, 1621575072);
INSERT INTO `sp_user_token` VALUES ('f47d77fc055bf6b8ba868f40d3bed738039d39cd', 3, 1619167055, 1621759055);
INSERT INTO `sp_user_token` VALUES ('f48cd0f2c28ac513677e5bdd598778e8ce500834', 3, 1618899168, 1621491168);
INSERT INTO `sp_user_token` VALUES ('f52b948148257087a4d71062d5959ceef2a8c12f', 6, 1619271313, 1621863313);
INSERT INTO `sp_user_token` VALUES ('f5c2b19dfc6feb3739101a9ee72d8991c3c73f27', 3, 1618970292, 1621562292);
INSERT INTO `sp_user_token` VALUES ('f6524d4779f445aeead2e0b19eb51581f6b883dd', 4, 1620380577, 1622972577);
INSERT INTO `sp_user_token` VALUES ('f67045062f0d3f31b8a8b3395215f68cc01540f7', 3, 1618903419, 1621495419);
INSERT INTO `sp_user_token` VALUES ('f72179e81ae4f50df417bd350ca04fe6b27fe471', 6, 1620554368, 1623146368);
INSERT INTO `sp_user_token` VALUES ('f7326c7aab4174cd9b9d193900de91bf9dc70da8', 3, 1618973191, 1621565191);
INSERT INTO `sp_user_token` VALUES ('f747a4dc67c1912f19e397359a2877a1c21a14ea', 5, 1619156719, 1621748719);
INSERT INTO `sp_user_token` VALUES ('f8be8cd4571ef05042f72da28ac0fe1e50d8312f', 4, 1620270700, 1622862700);
INSERT INTO `sp_user_token` VALUES ('f908e96d8a55db827e8b781c46fe7b6b7bb84535', 4, 1620042126, 1622634126);
INSERT INTO `sp_user_token` VALUES ('f91c9bdb036534a625d3fb99bae9596874fd5915', 3, 1618982759, 1621574759);
INSERT INTO `sp_user_token` VALUES ('f961a12dd16cc81b80799ca85f4932a426f424ef', 4, 1618990766, 1621582766);
INSERT INTO `sp_user_token` VALUES ('f98621f4da51d9aad9d1e074f48577548468bdd8', 23, 1620784846, 1623376846);
INSERT INTO `sp_user_token` VALUES ('f9c04c104869fe6c605987076147b49fedd529cf', 7, 1620377300, 1622969300);
INSERT INTO `sp_user_token` VALUES ('f9c84b16201d2430f46cc9ad6f3e1e232390f7f8', 4, 1618983997, 1621575997);
INSERT INTO `sp_user_token` VALUES ('f9f440c9ad706185f33812555930f90e3df1a2bd', 3, 1618904292, 1621496292);
INSERT INTO `sp_user_token` VALUES ('fab6c17a96d6edeb84ef70dcb3b84aec779d20f4', 3, 1618897629, 1621489629);
INSERT INTO `sp_user_token` VALUES ('fb23afb5b0114cf631df4ae1e26fd8dcf7b5e900', 4, 1619148952, 1621740952);
INSERT INTO `sp_user_token` VALUES ('fb2ce43c477a7cf4552bf7d25cdf76ad54fe5e2e', 4, 1620272148, 1622864148);
INSERT INTO `sp_user_token` VALUES ('fb597e3bacb666e37014e286c5245127f311fe0d', 3, 1619163883, 1621755883);
INSERT INTO `sp_user_token` VALUES ('fb65794119341c9fb9d6f43574cfed3bb9faa1fe', 4, 1620372057, 1622964057);
INSERT INTO `sp_user_token` VALUES ('fba498a2aff12ba15f4e42ea9139619aca4ba033', 4, 1619240049, 1621832049);
INSERT INTO `sp_user_token` VALUES ('fbca686c7979a271a898421fb888df1fd85bf897', 4, 1619146438, 1621738438);
INSERT INTO `sp_user_token` VALUES ('fbfb8c6685b3c0ded7a014ff40a078e43c6185e4', 3, 1619142931, 1621734931);
INSERT INTO `sp_user_token` VALUES ('fc0f39f135a02f0a6bf928f361730b8e0f628739', 4, 1619158818, 1621750818);
INSERT INTO `sp_user_token` VALUES ('fc418fd1f3e177f7f25b4b6859cc6e5763921c78', 25, 1620956845, 1623548845);
INSERT INTO `sp_user_token` VALUES ('fcea911608c98c0a50e3c06c3aec071c275e8e2f', 3, 1618992297, 1621584297);
INSERT INTO `sp_user_token` VALUES ('fd1a6889c2a28574052113c1f0a275e98f9b0a4d', 3, 1618994236, 1621586236);
INSERT INTO `sp_user_token` VALUES ('fd5847fef28da6e24b3fdd16bae4707cd502f095', 4, 1618988367, 1621580367);
INSERT INTO `sp_user_token` VALUES ('fdce7265cbbd0ac60c766d8eef545f395fda187c', 4, 1620375403, 1622967403);
INSERT INTO `sp_user_token` VALUES ('fe09f447da60f3beec886500662e1711f52c7fa6', 3, 1619162854, 1621754854);
INSERT INTO `sp_user_token` VALUES ('fe32895572965d3c46ffdaa03aef59f2a94aa35b', 4, 1620375485, 1622967485);
INSERT INTO `sp_user_token` VALUES ('fe87074f43065f5c4713fa1a4680c6c01dc3be10', 4, 1619231224, 1621823224);
INSERT INTO `sp_user_token` VALUES ('feb0c2d3ecd0997a7fae429852de6cf5113fc133', 6, 1621304970, 1623896970);
INSERT INTO `sp_user_token` VALUES ('feec5decfccb9ad856742378cb596f2c6e332a65', 13, 1620374890, 1622966890);
INSERT INTO `sp_user_token` VALUES ('ffb9f7e04362312c48661a133c8b72228f44e969', 3, 1618911757, 1621503757);
COMMIT;

-- ----------------------------
-- Table structure for sp_version
-- ----------------------------
DROP TABLE IF EXISTS `sp_version`;
CREATE TABLE `sp_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `oldversion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '旧版本号',
  `newversion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '新版本号',
  `packagesize` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '包大小',
  `content` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '升级内容',
  `downloadurl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '下载地址',
  `enforce` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '强制更新',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='版本表';

-- ----------------------------
-- Records of sp_version
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sp_yuyue
-- ----------------------------
DROP TABLE IF EXISTS `sp_yuyue`;
CREATE TABLE `sp_yuyue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(10) DEFAULT NULL COMMENT '场馆id',
  `uid` int(10) NOT NULL COMMENT '用户id',
  `s_time` int(10) NOT NULL COMMENT '开始时间',
  `e_time` int(10) DEFAULT NULL COMMENT '结束时间',
  `yuyue_status` enum('1','2') DEFAULT '1' COMMENT '预约状态:1=已预约,2=到场',
  `baochang_status` enum('1','2','3') DEFAULT '1' COMMENT '包场状态:1=个人,2=包场,3=其他',
  `order` varchar(35) DEFAULT '0' COMMENT '订单号',
  `nums` int(4) DEFAULT '1' COMMENT '人数',
  `ps` varchar(255) DEFAULT NULL COMMENT '备注',
  `promoter_status` enum('1','2') DEFAULT '1' COMMENT '是否是发起人:1=是,2=否',
  `pay_status` enum('1','2','3','4','5','6','7','8','9','10') DEFAULT '1' COMMENT '支付状态:1=待支付,2=已支付,8=支付超时,9=申请退款中,10=已退款',
  `random_status` enum('1','2') DEFAULT '1' COMMENT '随机队员:1=否,2=随机',
  `time` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=587 DEFAULT CHARSET=utf8mb4 COMMENT='预约场馆';

-- ----------------------------
-- Records of sp_yuyue
-- ----------------------------
BEGIN;
INSERT INTO `sp_yuyue` VALUES (585, 7, 7, 1620381900, 1620385680, '1', '2', 'Bc72021050718011497564899941598', 2, NULL, '1', '2', '1', 1620381674);
INSERT INTO `sp_yuyue` VALUES (586, 7, 6, 1620381900, 1620385680, '1', '2', 'Son62021050718011497564899941598', 1, NULL, '2', '2', '1', 1620381711);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
